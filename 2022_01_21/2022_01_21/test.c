#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	return 0;
//}

// strtok
//1:strtok函数找第一个标记的时候，函数的第一个参数不是NULL
//2:strtok函数找非第一个标记的时候，函数的第一个参数是NULL
//
//int main()
//{
//	const char* p = "@.";
//	char arr[] = "lixingyu@yeah.com";
//	char buf[50] = { 0 };
//	strcpy(buf, arr);
//	char* str = NULL;
//	for (str = strtok(buf, p); str != NULL;str = strtok(NULL,p))
//	{
//		printf("%s\n", str);
//	}
//	//char* str = strtok(buf, p);//lixingyu
//	//printf("%s\n", str);
//	//str = strtok(NULL,p);//yeah
//	//printf("%s\n", str);
//	//str = strtok(NULL, p);//com
//	//printf("%s\n", str);
//	return 0;
//}

//streror
//返回错误码所对应的错误信息
//C语言中规定了一些错误信息
// 0 - NO Error
// 
//strerror 可以把翻译码翻译成错误信息

//int main()
//{
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d -->%s\n",i,strerror(i));
//	}
//	return 0;
//}
//C语言可以操作文件
//打开文件 - fopen
//
// 当库函数使用的时候，发生错误会把error这个全局的错误变量设置为本次执行库函数产生的错误码
// error是C语言提供的一个全局变量，可以直接使用，放在error.h文件中的

#include <errno.h>
//int main()
//{
//	FILE* pf = fopen("test.txt", "r");
//	if (NULL == pf)
//	{
//		//出错误的原因是什么
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	//读文件
//	//...
//
//
//	// 关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}


// isspace -- 空白字符 
// 是返回 正数
// 不是返回 0
//
#include <ctype.h>
//int main()
//{
//	//printf("%d\n", isspace(' '));
//	char ch = 'w';
//	if (isspace(ch))
//	{
//		//空白字符
//	}
//	return 0;
//}

//int main()
//{
//	char ch = '0';
//	if (ch >= '0' && ch <= '9')
//	{
//
//	}
//	if (isdigit(ch))
//	{
//
//	}
//
//	return 0;
//}

//islower -- 判断小写字母

//字符转换
//int tolower(int c)

//int main()
//{
//	char ch = 0;
//	ch = getchar();
//	if (islower(ch))
//	{
//		ch = toupper(ch);
//	}
//	else
//	{
//		ch = tolower(ch);
//	}
//
//	return 0;
//}

//内存函数
 
//memcpy
//memmove
//memset
//
//

//memcpy
#include <assert.h>

void* my_memcpy(void* dest, const void* src, size_t num)
{
	void* ret = dest;
	assert(dest && src);
	while(num--)
	{
		*(char*)dest = *(char*)src;
		dest = (char*)dest + 1;
		src = (char*)src + 1;
	}
	return ret;
}
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[20] = { 0 };
//	strcpy(arr2, arr1);
//
//	int arr3[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr4[5] = { 0 };
//	//my_memcpy(arr4, arr3,20);
//	my_memmove(arr3+2,arr3,12);
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr3[i]);
//	}
//	return 0;
//}

//memmove
//内存拷贝时可以重叠

// C语言只要求
//memcpy能拷贝不重叠的内存空间就可以了
//memmove去处理那些重叠内存拷贝
//
my_memmove(void* dest, const void* src, size_t num)
{
	void* ret = dest;
	assert(dest && src);
	if (dest < src)
	{
		//正着拷
		while (num--)
		{
			*(char*)dest = *(char*)src;
			dest = (char*)dest + 1;
			src = (char*)src + 1;
		}
	}
	else
	{
		//倒着拷
		while (num--)
		{
			*((char*)dest+num) = *((char*)src + num);
		}
		return ret;
	}
}

//memcmp -- 内存比较
//从ptr1,ptr2开始之后num个字节
//相等 -- 0
//

//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };
//	int arr2[] = { 1,2,3,4,5 };
//	int ret = memcmp(arr1, arr2, 8);
//	printf("%d\n", ret);
//	return 0;
//}

//memset
//以字节为单位来修改
//int main()
//{
//	/*char arr[20] = { 0 };
//	memset(arr, 'x', 10);*/
//
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	memset(arr, 'x', 10);
//	return 0;
//}
