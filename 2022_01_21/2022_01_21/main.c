#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <float.h>
#include <math.h>

//int main()
//{
//	//浮点数是不能 == 比较的
//
//
//	//浮点数精度值
//	//1.自定义
//	//2.系统自定义
//	//double x = 1.0;
//	//double y = 0.1;
//	//printf("%.50f\n", x-1.0);
//	//printf("%.50f\n", y);
//
//	//printf("%.50f\n", x - 0.9);
//	//printf("%.50f\n", x - 0.9-y);
//
//	//if ( fabs(x - 0.9 - y) < DBL_EPSILON)
//	//{
//	//	printf("You can see me!\n");
//	//}
//	//else
//	//{
//	//	printf("oops!\n");
//	//}
//
//
//	//与零值的比较
//	//double a = 0.0000000000000000001;
//	//if (fabs(a) < DBL_EPSILON)
//	//{
//	//	//说明 a == 0;
//	//	printf("You Can See Me!");
//
//	//}
//	//else
//	//{
//	//	printf("oops!");
//	//}
//	
//	//要不要相等
//	//double x = 0.00000000000000000000001;
//	//if (x >= DBL_EPSILON && x <= DBL_EPSILON) //error
//	//{
//	//	printf("x == 0.0!\n");
//	//}
//	//答案是不要
//
//	return 0;
//}

//指针和0比较
//
//int main()
//{
//	//类型是不同的
//	printf("%d\n", 0);
//	printf("%d\n", '\0');
//	printf("%d\n", NULL);
//
//	int a = 0;
//	char* p = NULL;
//
//	//NULL = '(void* )0'  把0强制类型转换成(void* )
//
//	//如何理解强制类型转换
//	// "123456" --> int:123456 (真实的转化)
//	//真实的转化改变内存中的值
//
//	//什么叫做强制类型转化
//	//
//	//强制类型转化不改变内存中的数据，只改变对应的类型
//
//	return 0;
//}

//int main()
//{
//	int x = 0;
//	int y = 1;
//	//else 匹配采用就近原则
//	if (10 == x)
//	{
//		if (11 == y)
//		{
//			printf("hello!\n");
//		}
//		else
//		{
//			printf("hello 世界！\n");
//		}
//	}
//	/*int* p = NULL;
//	if (NULL == p)
//	{
//
//	}*/
//
//	return 0;
//}

int main()
{
	//if:判断，分支
	int day = 0;
	scanf("%d",&day);
	// case:  判断功能
	// break: 分支
	switch (day)
	{
	case 1:
		printf("星期一\n");
		// break: 分支功能
		break;
	case 2:	
		printf("星期二\n");
		break;
	case 3:
		printf("星期三\n");
		break;
	case 4:
		printf("星期四\n");
		break;
	case 5:
		printf("星期五\n");
		break;
	case 6:
		printf("星期六\n");
		break;
	case 7:
		printf("星期天\n");
		break;

	}

	return 0;
}