#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//int main()
//{        // 逗号表达式   值是最后一个值
//	int arr[] = { 1,2,(3,4),5 };
//}
//int reverse(int* arr, int sz)
//{
//	int i = 0;
//	int tmp = 0;
//	int left = 0;
//	int right = sz -1 ;
//	while (left <= right)
//	{
//		tmp = arr[left];
//		arr[left] = arr[right];
//		arr[right] = tmp;
//		left++;
//		right--;
//	}
//	 for (i = 0; i <= sz-1; i++)
//	 {
//		printf("%d ", arr[i]);
//	 }
//}

//void init(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		arr[i] = 0;
//	}
//}
//
//void print(int arr[],int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//void reverse(int arr[], int sz)
//{
//	int left = 0;
//	int right = sz - 1;
//	while (left < right)
//	{
//		int tmp = 0;
//		tmp = arr[left];
//		arr[left] = arr[right];
//		arr[right] = tmp;
//		left++;
//		right--; 
//	}
//}
//int main()
//{
//	//int arr[10] = { 0 };
//
//	int arr[5] = { 1,2,3,4,5 };
//	//init
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	//init(arr,sz);
//	print(arr, sz);
//
//	reverse(arr, sz);
//	
//	print(arr,sz);
//	
//	return 0;
//}




//int main()
//{
//	int arr1[] = { 1,3,5,7,9 };
//	int arr2[] = { 2,4,6,8,10 };
//
//
//	//交换
//	int tmp =0;
//	int i = 0;
//	int sz = sizeof(arr1) / sizeof(arr1[0]);
//	for (i = 0; i < sz; i++)
//	{
//		tmp = arr1[i];
//		arr1[i] = arr2[i];
//		arr2[i] = tmp;
//
//	}
//	for (i = 0; i < sz; i++) {
//		printf("%2d ", arr1[i]);
//	}
//	printf("\n");
//	for (i = 0; i < sz; i++) {
//		printf("%2d ", arr2[i]);
//	}
//	return 0;
//}

// ********操作符详解*********

// /       % --取模操作符两端必须是整数
//int main()
//{
//	int r = -11 % 3;
//	printf("%d", r);
//
//
//	return 0;
//}

// 移位操作符
//整数的3中二进制的表示形式：原码  反码   补码
// 正数 的 原码 反码 补码相同

// 负数 的 反码+1 就是补码

// 整数在内存中存储的是补码


// 16进制  0 1 2 3 4 5 6 7 8 9 a b c d e f
//
//int main()
//{
//	int a = 5;
//	int a2 = -5;
//	int b = a << 2; // 左移移动一位 *2
//	int c = a >> 2;// 右移移动一位 /2 
//	int b2 = a2 << 1; // 左移移动一位 *2
//	int c2 = a2 >> 1;// 右移移动一位 /2 
//	printf("%d\n", b);
//	printf("%d\n", c);
//	printf("%d\n", b2);
//	printf("%d\n", c2);
//
//	return 0;
//}

// 按位与
//int main()
//{
//	int a = 3;
//	int b = -5;
//	int c = a & b;
//	// a 和 b 存在内存中的二进制的补码计算的
//	printf("%d\n",c);
//	return 0;
//}

// 按位或
//int main()
//{
//	int a = 3;
//	int b = -5;
//	int c = a | b;
//	printf("%d ", c);
//	return 0;
//}

//int main()
//{
//	int a = 3;
//	int b = 5;
//
//	int c = 0;
//	//printf("a=%d,b=%d\n", a, b);
//	//c = a;
//	//a = b;
//	//b = c;
//	//printf("a=%d,b=%d\n", a, b);
////2
//	//a = a + b;
//	//b = a - b;
//	//a = a - b;
//	//printf("a=%d,b=%d\n", a, b);
//// 3 异或
//	a = a ^ b;
//	b = a ^ b;
//	a = a ^ b;
//	printf("a=%d,b=%d\n", a, b);
//
//	return 0;
//}


//int main()
//{
//	//左值：是可以放在等号左边的，一般是一块空间
//	//右值：是可以放在等号右边的，一般是一个值，或者一块空间的内存
//
//}

//int main()
//{
//	int a = 0;
//	//按（内存中补码的2进制）位取反
//	// 00000000000000000000000000000000 - 补码
//	// 11111111111111111111111111111111 - 反码
//	// 10000000000000000000000000000001 - 原码
//	printf("%d\n",~a);// -1
//
//	return 0;
//}
int main()
{

	int a = 10;
	a |= (1 << 2);
	a &= ~(1 << 2);
	printf("%d ", a);

	return 0;
}
