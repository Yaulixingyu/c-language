#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//
//
//int main() {
//	printf("helloworld");
//}
// 
 
//define include 预处理指令   不是关键字
//******常见关键字********
	//c语言提供的关键字
	// 1.c语言提供的，不能自己创建关键字
	// 2.关键字不能做变量名

// auto 是自动的-每个局部变量都是 auto 修饰的
//int main() {
//    auto int a = 10;//自动创建自动销毁的-自动变量
//	                // auto 一般省略掉了
//                    // auto在新的C语言中有别的用
//	printf("%d",a);
//	return 0;
//}
 
 

//emun 枚举
//extern 是用来申明外部符号的
//register 寄存器关键字
//signed  有符号的 10  -20 
//unsigned 无符号的
// static 静态的
//union   联合体
//void  无 空的
// volatile 高级的C语言  暂时理解不了

 
//int main() {
//
//	register int num = 100;//建议num的值存放在寄存器中
//	                 //更小   即使mb   8-16G    500G    2T                
//	// 数据可以存在： 寄存器 高速缓存   内存    硬盘   网盘
//	printf("%d",num);
//	return 0;
//}

//************0915常见关键字2**************88
//typedef 类型定义  类型重定义
//typedef unsigned int u_int;
////张三 - 小三 别名 
//int main() {
//
//	unsigned int num = 100;
//	u_int num2 = 100;
//
//	return 0;
//}


//static 静态的
//1: static   修饰局部变量
//            改变了局部变量的生命周期(本质上改变了变量的存储类型)
//  栈区      局部变量 函数的参数
//  堆区      动态内存分配的
//  静态区    存全局变量  和  static修饰的静态变量
//2: static 修饰全局变量
//3: static 修饰函数
// 
//void test() {
//	int a = 1;//a是局部变量
//	a++;
//	printf("%d", a);//执行结果：2222222222
//}
// 
 
//  从栈区变到了静态区   程序不结束 静态区不停止 改变生命周期

//void test() {
//	static int a = 1;//a是局部变量
//	a++;
//	printf("%d", a);//执行结果 234567891011
//}
//int main() {
//	int i = 0;
//	while (i <10) {
//		test();
//		i++;
//	}
//  return 0 ;
//}



//声明
//extern  声明外部符号
//extern int g_val;
//int main() {
//s
//	printf("%d\n", g_val);//执行结果：2022
//	return 0;
//}
//全局变量在工程中都可以使用
//全局变量在其他源文件内部可以被使用，是因为全局变量具有外部链接属性
//但是被static修饰之后，就变成了内容链接属性，其他源文件就不能链接到这个静态的全局变量了！

//被static 修饰全局变量之后 使得这个全局变量只能在自己所在的源文件(.c)内部可以使用
//其他源文件不能使用！ 即使声明也没用 

//声明函数
//extern int Add(int x，inty);
//int main() {
//	int a = 10;
//	int b = 20;
//	int sum = Add(a, b);
//	printf("sum = %d\n", sum);
//	return 0;
//}
//static 修饰函数，使得函数只能在自己所在的源文件内部使用，不能在其他源文件内部使用
//本质上：static 是讲函数的外部链接属性变成了内部链接属性！(和static修饰全局变量一样！)

//**********define定义常量和宏*********
// define 是一个预处理指令
//1. define 定义符号

//#define MAX 1000
//int main() {
//
//	printf("%d\n", MAX);
//	return 0;
//}
//2. define 定义宏
//   宏的参数是替换的
//#define ADD(X,Y) X+Y
//int main() {
//	//4*2+3
//	printf("%d\n", 4 * ADD(2, 3));//执行结果：11
//	return 0;
//}
//#define ADD(X,Y) ((X)+(Y))
//int main() {
//	               //4*2+3
//	printf("%d\n", 4*ADD(2, 3));//执行结果：11
//	return 0;
//}