#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>


//写一个函数打印arr数组的内容，不使用数组下标，使用指针。
//arr是一个整形一维数组。

//void print_arr(int* p, int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p+i));
//		
//	}
//
//}

//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	//arr -->表示首元素的地址
//	print_arr(arr,sz);
//
//	return 0;
//
//}





//#include <string.h>
////写一个函数，可以逆序一个字符串的内容。
//int main()
//{
//	char arr[1000] = { 0 };
//	//输入
//	gets(arr);
//
//	//逆序
//	int len = strlen(arr);
//	char* left = arr;
//	char* right = arr + len - 1;
//
//	while (left < right)
//	{
//		char tmp = *left;
//		*left = *right;
//		*right = tmp;
//
//		left++;
//		right--;
//
//	}
//
//	//打印
//	printf("%s\n", arr);
//
//	return 0;
//}




//求Sn = a + aa + aaa + aaaa + aaaaa的前5项之和，其中a是一个数字，
//例如：2 + 22 + 222 + 2222 + 22222

//int main()
//{
//	int a = 0;
//	int n = 0;
//	scanf("%d %d", &n, &a); // n是前n项 ，a是数字
//	int sum = 0;
//	int ret = 0;
//	int i = 0;
//	for (i = 0; i < n; i++)
//	{
//		ret = ret * 10 + a;
//		sum += ret;
//
//	}
//	printf("%d\n", sum);
//	return 0;
//}

//求出0～100000之间的所有“水仙花数”并输出。
//“水仙花数”是指一个n位数，其各位数字的n次方之和确好等于该数本身，如:153＝1 ^ 3＋5 ^ 3＋3 ^ 3，则153是一个“水仙花数”
//#include <math.h>
//int main()
//{
//	int i = 0;
//	for (i = 0; i <= 100000; i++)
//	{
//		//判断i是不是水仙花数
//		//i是n位数
//		int n = 1;
//		int sum = 0;
//		int tmp = i;
//		while (tmp /= 10)
//		{
//			n++;
//		}
//		//每一位的n次方之和
//		tmp = i;
//		while (tmp)
//		{
//			sum += (int)pow(tmp % 10, n);
//			tmp /= 10;
//		}
//		if (sum == i)
//		{
//			printf("%d ", i);
//		}
//	}
//
//	return 0;
//}



////打印菱形
//int main()
//{
//	int n, i, m, j, k;
//	scanf("%d", &n);
//	m = (n + 1) / 2;
//	for (i = 1; i <= n; i++)  //一行一行的循环打印
//	{
//		if (i <= m)           //分两种情况，上半部分和下半部分
//		{
//			for (j = m - i; j > 0; j--)
//				printf(" ");
//			for (k = i; k > 0; k--)
//				printf("* ");
//		}
//		else
//		{
//			for (j = i - m; j > 0; j--)
//				printf(" ");
//			for (k = 2 * m - i; k > 0; k--)
//				printf("* ");
//		}
//		printf("\n");        //注意换行
//	}
//}


//输入一个整数数组，实现一个函数，
//来调整该数组中数字的顺序使得数组中所有的奇数位于数组的前半部分，
//所有偶数位于数组的后半部分。

//void print(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//
//void move(int arr[],int sz)
//{
//	int* left = arr;
//	int* right = arr + sz - 1;
//	while (left < right)
//	{
//		//从左向右找一个偶数，停下来
//		while (left<right && (*left) % 2 == 1)
//		{
//			left++;
//		}
//		//从右向左找一个奇数，停下来
//		while (left < right && (*right) % 2 == 0)
//		{
//			right--;
//		}
//		//奇数偶数交换
//		if (left < right)
//		{
//			int tmp = *left;
//			*left = *right;
//			*right = tmp;
//		}
//	}
//
//}
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,0 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	move(arr, sz);
//	printf(arr, sz);
//	
//	return 0;
// }


//杨辉三角

//int main()
//{
//	int arr[10][10] = { 0 };
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < 10; i++)
//	{
//		for (j = 0; j <= i; j++)
//		{
//			if (j == 0)
//				arr[i][j] = 1;
//			if (i == j)
//				arr[i][j] = 1;
//			if (i >= 2 && j >= 1)
//				arr[i][j] = arr[i - 1][j] + arr[i - 1][j - 1];
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//		return 0;
//}



//日本某地发生了一件谋杀案，警察通过排查确定杀人凶手必为4个嫌疑犯的一个。
//以下为4个嫌疑犯的供词:
//A说：不是我。
//B说：是C。
//C说：是D。
//D说：C在胡说
//已知3个人说了真话，1个人说的是假话。
//现在请根据这些信息，写一个程序来确定到底谁是凶手。

//int main()
//{
//	int killer = 0;
//	for (killer = 'a'; killer <= 'd'; killer++)
//	{
//		//          1/0             1/0                 1/0            1/0
//		if ((killer != 'a') + (killer == 'c') + (killer == 'd') + (killer != 'd') == 3)
//		{
//			printf("%c\n", killer);
//		}
//	}
//	return 0;
//}




//5位运动员参加了10米台跳水比赛，有人让他们预测比赛结果：
//A选手说：B第二，我第三；
//B选手说：我第二，E第四；
//C选手说：我第一，D第二；
//D选手说：C最后，我第三；
//E选手说：我第四，A第一；
//比赛结束后，每位选手都说对了一半，请编程确定比赛的名次。
int main()
{
	int a = 0;
	int b = 0;
	int c = 0;
	int d = 0;
	int e = 0;
	for (a = 1; a <= 5; a++)
	{
		for (b = 1; b <= 5; b++)
		{
			for (c = 1; c <= 5; c++)
			{
				for (d = 1; d <= 5; d++)
				{
					for (e = 1; e <= 5; e++)
					{
						if (((b == 2) + (a == 3) == 1)
							&& ((b == 2) + (e == 4) == 1)
							&& ((c == 1) + (d == 2) == 1)
							&& ((c == 5) + (d == 3) == 1)
							&& ((e == 4) + (a == 1) == 1)
							)
						{
							if (a * b * c * d * e == 120)
								printf("a = %d b = %d  c = %d  d = %d e = %d ", a, b, c, d, e);
						}
					}
				}
			}
		}

	}

	return 0;
}