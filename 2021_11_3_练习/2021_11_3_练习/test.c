#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>


//int DigitSum(n) {
//	if (n > 9) {
//		int m = n % 10;
//		return m + DigitSum(n / 10);
//	}
//	else return n;
//}
//
//int main() {
//	int num = 0;
//	scanf("%d",&num);
//	int ret = DigitSum(num);
//	printf("%d",ret);
//	return 0;
//}

//编写一个函数实现n的k次方，使用递归实现。
//int Sqrt(m, k) {
//	if (k == 1) {
//		return m;
//	}
//	else {
//		return m * Sqrt(m, k - 1);
//	}
//}
//
//int main() {
//	int m = 0;
//	int k = 0;
//	scanf("%d %d", &m,&k);
//	int sum = Sqrt(m,k);
//	printf("%d", sum);
//	return 0;
//}

// fib数：后一位数是前两位数字之和
// 规定：前两位是 1 1 
// fib: 1 1 2 3 5 8 13 ...........

//int Fib(m) {
//	if (m <= 2) {
//		return 1;
//	}
//	else {
//		return Fib(m - 1) + Fib(m - 2);
//	}
//}
//编写一个函数 reverse_string(char * string)（递归实现）

//实现：将参数字符串中的字符反向排列，不是逆序打印。

//要求：不能使用C函数库中的字符串操作函数。
//char reverse_string(char* s) {
//	if (s == '\0') {
//		return 0;
//	}
//	else {
//		return reverse_string(s + 1);
//	}
//}
//int main() {
//
//	char arr[]= "abcd";
//	char string = reverse_string(arr);
//	printf("%s", string);
//	return 0;
//}

void reverse_string(char* arr)
{
	int len = strlen(arr);
	char tmp = *arr;
	*arr = *(arr + len - 1);

	*(arr + len - 1) = '\0';
	if (strlen(arr + 1) >= 2)
		reverse_string(arr + 1);

	*(arr + len - 1) = tmp;
}
int main() {
	char arr[] = "abcdef";
//	 = reverse_string(arr);


	return 0;
}