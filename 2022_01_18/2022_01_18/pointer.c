#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//指针进阶4
//int main()
//{
//	int a = 10;
//	scanf("%d", &a);
//	return 0;
//}

//数组名
//数组名是数组首元素地址
// 有两个例外：
// 1：sizeof(数组名)，这里的数组名表示整个数组，计算的是整个数组的大小，单位是字节
// 2：&数组名，这里的数组名表示整个数组，取出的是数组的地址
// 除上面两种外，所有的数组名表示数组首元素的地址
//
//int main()
//{
//	int a[] = { 1,2,3,4 };
//	printf("%d\n", sizeof(a)); // 16 数组名单独放在sizeof内部。计算整个数组的大小，单位是字节
//	printf("%d\n", sizeof(a+0)); // 4 a表示首元素的地址，a+0还是首元素的地址 
//	printf("%d\n", sizeof(*a)); // 4 a表示首元素的地址，*a是对首元素的解引用，就是首元素，单位是字节 
//	printf("%d\n", sizeof(a + 1));//a表示首元素地址，a+1是第二个元素的地址，是地址，大小就是4/8个字节
//	printf("%d\n", sizeof(a[1]));//a[1]是第二个字节
//	printf("%d\n", sizeof(&a));//4/8 &a表示的是数组的地址，地址就是4/8个字节
//	printf("%d\n", sizeof(*&a));// 16 &和*抵消，相当于a,sizeof(a)是16，
//	// &a --> int(*)[4]
//	//&a是数组的地址，他的类型是int(*)[4]数组指针，如果解引用，访问的是4个int的数组，大小是16个字节
//	printf("%d\n", sizeof(&a+1));//&a是数组的地址，&a+1是跳过整个数组后的地址，是地址就是4/8个字节
//	printf("%d\n", sizeof(&a[0]));//&a[0]取出数组第一个元素的地址
//	printf("%d\n", sizeof(&a[0]+1));//&a[0]+1取出数组第二个元素的地址
//	//&a[0] - int*
//
//
//	return 0;
//}

//sizeof只关注占用看见的大小，单位是字节
//sizeof不关注类型
//sizeof是操作符

//strlen关注的字符串'\0'为止，计算的是\0之前出现了多少个字符
//strlen只针对字符串
//strlen是库函数
#include <string.h>
//int main()
//{
//	//字符数组
//	char arr[] = { 'a','b','c','d','e','f' };
//	printf("%d\n", strlen(arr));//随机值  arr是首元素的地址，但是arr数组中没有\0，计算的时候就不知道什么时候停止，结果是随机值
//	printf("%d\n", strlen(arr+0));//随机值 arr是首元素的地址，arr+0还是首元素的地址，结果是：随机值
//	//printf("%d\n", strlen(*arr));//error 97——>向后访问 不是合法地址 非法访问内存
//	//strlen需要的是一个地址，从这个地址开始向后找字符，直到\0，统计字符的个数
//	//但是*arr是数组的首元素，也是'a'，这是传给strlen的就是'a'的ASCII码值，strlen函数会把97当做起始地址
//	//统计字符串，会形成访问错误
//	//printf("%d\n", strlen(arr[1]));//和上一个一样，是第二个元素，访问冲突
//	printf("%d\n", strlen(&arr));//&arr是arr数组的地址，虽然类型和strlen的参数类型有所差异，但是传参过去
//	//还是从第一个字符的位置向后数字符，结果还是随机值
//	printf("%d\n", strlen(&arr+1));// 也是随机值
//	printf("%d\n", strlen(&arr[0]));//
//
//
//	return 0;
//}


//int main()
//{
//	char arr[] = "abcedf";
//	printf("%d\n", sizeof(arr));//7
//	printf("%d\n", sizeof(arr + 0));//4/8
//	printf("%d\n",  sizeof(*arr));//1
//	printf("%d\n",  sizeof(arr[1]));//1
//	printf("%d\n", sizeof(&arr));//4/8
//	printf("%d\n", sizeof(&arr + 1));//4/8
//	printf("%d\n", sizeof(&arr[0] + 1));//4/8
//
//	return 0;
//}

//int main()
//{
//	char arr[] = "abcedf";
//	printf("%d\n", strlen(arr));//6
//	printf("%d\n", strlen(arr+0));//6
//	//printf("%d\n", strlen(*arr));//error
//	//printf("%d\n", strlen(arr[1]));//error
//	printf("%d\n", strlen(&arr));//6
//	printf("%d\n", strlen(&arr+1));//随机值
//	printf("%d\n", strlen(&arr[0]+1));//5
//
//	return 0;
//}

//int main()
//{
//	char* p = "abcdef";
//	//printf("%d\n", sizeof(p)); // p是一个指针变量，sizeof(p)计算的是指针变量的大小，4/8个字节
//	//printf("%d\n", sizeof(p+1));//p是指针变量，是存放地址的，p+1还是地址，地址大小是4/8个字节
//	//printf("%d\n", sizeof(*p));//p是char*的指针，*p解引用访问1个字节
//	//printf("%d\n", sizeof(p[0]));//p[0] --> *(p+0) --> *p  1个字节
//	//printf("%d\n", sizeof(&p)); // &p也是地址，是地址就是4/8字节，&p是二级指针
//	//printf("%d\n", sizeof(&p+1)); //&p是地址，+1后还是地址。是地址就是4/8个字节
//	////&p  p[0]就是a &p[0]就是a的地址 &p[0]+1就是b的地址
//	//printf("%d\n", sizeof(&p[0]+1));
//
//
//	printf("%d\n", strlen(p)); // p中存放的a的地址，strlen(p)就是从'a'的位置向后 就是6
//	printf("%d\n", strlen(p+1));// p+1是b的地址。从b的位置向后找字符串，就是 5
//	//printf("%d\n", strlen(*p));// err
//	//printf("%d\n", strlen(p[0]));//err
//	printf("%d\n", strlen(&p));//随机值
//	printf("%d\n", strlen(&p+1));//随机值
//	printf("%d\n", strlen(&p[0]+1));//5
//	//p[0] ->
//
//
//	return 0;
//}

//int main()
//{
//	//二维数组
//	int a[3][4] = { 0 };
//	printf("%d\n", sizeof(a));//数组名单独放在sizeof内部，计算的是整个数组的大小
//	printf("%d\n", sizeof(a[0][0]));
//	printf("%d\n", sizeof(a[0]));//a[0]表示第一行的数组名，a[0]作为数组名单独放在sizeof内部，计算的是整行的大小
//	printf("%d\n", sizeof(a[0]+1));//a[0]作为第一行的数组名，没有&,没有单独放在sizeof内部
//	//所以a[0]表示的是首元素的地址，即a[0][0]的地址，a[0]+1表示第一行第二个元素的地址，是地址就是4/8个字节
//	printf("%d\n", sizeof(*(a[0] + 1)));
//	printf("%d\n", sizeof(a + 1));
//	//a是二维数组的数组名，没有&,没有单独放在sizeof内部，a表示sizeof内部，a表示首元素的地址
//	//即第一行的地址，a+1即第二行的地址，是地址就是4/8
//	printf("%d\n", sizeof(*(a + 1)));//*(a+1)相当于第二行数组名，*(a+1)->a[1],sizeof计算的是第二行的大小
//	printf("%d\n", sizeof(&a[0] + 1));//a[0]是第一行的地址，
//	printf("%d\n", sizeof(*(&a[0] + 1)));//第二行
//	printf("%d\n", sizeof(*a));
//	printf("%d\n", sizeof(a[3]));//感觉a[3]越界但是没关系 int[4]
//
//	return 0;
//}

//int main()
//{
//	int a[5] = { 1,2,3,4,5 };
//	int* ptr = (int*)(&a + 1);
//	printf("%d %d", *(a + 1), *(ptr - 1));
//	return 0;
//}
//int main()
//{
//	int a[4] = { 1,2,3,4 };
//	int* ptr1 = (int*)(&a + 1);
//	int* ptr2 = (int*)((int)a + 1);
//	printf("%x %x", ptr1[-1], *ptr2);
//}

//int main()
//{
//	int a[3][2] = { (0,1),(2,3),(4,5) };
//	int* p;
//	p = a[0];
//	printf("%d", p[0]);
//	return 0;
//
//}