#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
//斐波那契数 x 最小步骤
//题目：https://www.nowcoder.com/questionTerminal/18ecd0ecf5ef4fe9ba3f17f8d00d2d66
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int a = 0;
//	int b = 1;
//	int c = a+b;
//	while (1)
//	{
//		if (n == b)
//		{
//			printf("%d\n", 0);
//			break;
//		}
//		else if(n<b)
//		{
//			if (abs(a-n) < abs(b-n))
//			{
//				printf("%d\n", abs(a-n));
//				break;
//			}
//			else
//			{
//				printf("%d\n", abs(b - n));
//				break;
//			}
//		}
//		a = b;
//		b = c;
//		c = a + b;
//	}
//	return 0;
//}

//题目：https://www.nowcoder.com/questionTerminal/4060ac7e3e404ad1a894ef3e17650423
//转换空格
//接口型
void replaceSpace(char* str, int length)
{
	int spacecnt = 0;
	char* cur = str;
	//求空格的长度
	while (*cur)
	{
		if (*cur == ' ')
		{
			spacecnt++;
		}
		cur++;
	}
	int newlen = length + spacecnt * 2;
	int end1 = length - 1;
	int end2 = newlen - 1;
	while (end1 != end2)
	{
		if (str[end1] != ' ')
		{
			str[end2--] = str[end1--];
		}
		else
		{
			str[end2--] = '0';
			str[end2--] = '2';
			str[end2--] = '%';
			end1--;
		}
	}
}