#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//int main() {
//	int a = 10;
//	a++;
//	printf("%d\n", a);
//    return 0;
//}

//******分支语句和循环语句******
//顺序结构  选择结构   循环结构
// 语句：;隔开就是一个语句
//*******if 语句********
//语法结构
// 
//int main() {
//	int age = 20;
//	if (age >= 18) {
//		printf("成年\n");
//	}
//	else {
//		printf("未成年\n");
//		printf("不能谈恋爱\n");
//	}
//
//	return 0;
//}
////**********多分支************
//int main() {
//	int age = 170;
//	if (age < 18)
//		printf("少年\n");
//	else if (age>=18 && age < 26)
//		printf("青年\n");
//	else if (age>=26 && age < 40)
//		printf("中年\n");
//	else if (age >= 40 && age < 60)
//		printf("壮年\n");
//	else if (age >= 60 && age < 100)
//		printf("老年\n");
//	else
//		printf("老不死\n");
//	return 0;
//}
// 
// else 悬空
//else 和最近的if 匹配
//int main() {
//	int a = 0;
//	int b = 1;
//	if (a == 1) {
//		if (b == 1) {
//			printf("hehe");
//		}
//		else{
//			printf("haha");
//		}
//	}
//	return 0;
//}
// 代码风格 - 
//int test() {
//
//	if (0)
//		return 0;
//	printf("hehe");
//	return 1;
//}
//int main() {
//	test();
//
//	return 0;
//}
//int main() {
//	int num = 3;
//	if (5 == num)// 常量放在左边
//		printf("hehe\n");
//
//	return 0;
//}

//int main() {
//    int num = 12;
//	if (num % 2 == 0)
//		printf("num是偶数");
//	else
//		printf("num是奇数");
//
//
//	return 0;
//}
//输出1-100的奇数 
//int main() {
//	int i;
//	for (i = 1;i <=100; i++)
//		if (i % 2 == 1)
//			printf("%d  ", i);
//
//		return 0;
//}

// switch语句
//int main() {
//	int day = 0;
//	scanf("%d", &day);
//	switch (day) {
//	case 1:
//		printf("星期一");
//		break;
//	case 2:
//		printf("星期二");
//		break;
//	case 3:
//		printf("星期三");
//		break;
//	case 4:
//		printf("星期四"); 
//		break;
//	case 5:
//		printf("星期五");
//		break;
//	case 6:
//		printf("星期六"); 
//		break;
//	case 7:
//		printf("星期日");
//		break;
//	default:
//		printf("错误：请输入1-7的数字！");
//		break;
//	}
//	return 0;
//}

