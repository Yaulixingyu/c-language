#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//int main()
//{
//	printf("hehe");
//
//	return 0;
//}

//int Print(unsigned int n)
//{
//	int i = 0;//计数器
//	while (i)
//	{
//		if (1 == n % 2)
//		{
//			i++;
//		}
//		n = n / 2;
//	}
//	printf("%d",i);
//}
//
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	//判断二进制中1的个数
//	Print(n);
//	return 0;
//}

//void print(int m)
//{
//	//打印奇数位
//	int i = 0;
//	for (i = 30; i >= 0; i -= 2)
//	{
//		printf("%d ",(m >> i) & 1);
//	}
//	printf("\n");//换行
//	//打印偶数位
//	for (i = 31; i >= 1; i -= 2)
//	{
//		printf("%d ", (m >> i) & 1);
//	}
//}
//
//int main()
//{
//	int m = 0;
//	scanf("%d",&m);
//	print(m);
//	return 0;
//}

//********初始结构体*********
// 结构是一些值的集合，这些值成为成员变量。结构的每个成员可以是不用类型的变量
//结构体就是用来描述复杂对象的
//

//声明一个人的结构体类型
//struct S 
//{
//	int a;
//	char c;
//	double d;
//};
//struct Stu
//{
//	struct S ss;//结构体套一个结构体
//	//成员列表
//	char name[20];  //姓名
//	int age;       //年龄
//	float score;  //成绩
//}; s1, s2;//s1,s2是两个结构体变量，全局的
//int main()
//{
//	int a = 0;
//	int b = 0; 
//
//	struct Stu s = { {100,'w',3.14},"zhangsan",20,95.5 };
//	
//	//struct Stu s = { "zhangsan",20,95.5f }; //定义一个结构体变量。局部的
//	printf("%d %c %lf %s %d %f\n", s.ss.a,s.ss.c,s.ss.d,s.name, s.age, s.score);
//	return 0;
//}

// 结构体传参
//struct S
//{
//	int arr[1000];
//	float f;
//	char ch[100];
//};
//
//void print1(struct S tmp)
//{
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", tmp.arr[i]);
//	}
//	printf("\n");
//	printf("%f\n", tmp.f);
//	printf("%s\n", tmp.ch);
//}
//
//void print2(struct S* ps)
//{
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", ps->arr[i]);
//	}
//	printf("\n");
//	printf("%f\n", ps->f);
//	printf("%s\n", ps->ch);
//}
//
//int main()
//{
//	struct S s = { {1,2,3,4,5,6,7,8,9,10},5.5f,"hello bit" };
//	//print1(s);
//	print2(&s);
//	return 0;
//}

int Add(int x, int y)
{
	int  z = 0;
	z = x + y;
	return z;
}

int main()
{
	int a = 10;
	int b = 20;
	int c = Add(a, b);
	printf("%d\n", c);
	return 0;
}