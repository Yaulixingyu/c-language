#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//getchar
//getchar 读取失败的时候

//int main() {
//	// scanf()
//	int ch = getchar();// 'a' - 97  
//	//printf("%c\n", ch);
//	// 
//	//  putchar ---输出一个字符
//	putchar(ch);
//
//	return 0;
//}

//int main() {
//
//	int ch = 0;
//	while ((ch = getchar())!= EOF) {
//		putchar(ch);
//	}
//
//	return 0;
//}

//int main() {
//	char input[20] = { 0 };
//	printf("请输入密码：>");
//	scanf("%s", input); // abcdef
//	
//	getchar(); // 拿走 \n
//
//
//	printf("请输入密码（Y/N）:>");
//	
//	int ch = getchar();
//	if (ch == 'Y') {
//		printf("确认成功\n");
//	}
//	else {
//		printf("确认失败\n");
//
//	}
//	return 0;
//}

//int main() {
//	char input[20] = { 0 };
//	printf("请输入密码：>");
//	scanf("%s", input); // abcdef hehe
//	 
//	//清理缓冲区
//	int tmp = 0;
//	while ((tmp = getchar()) != '\n') {
//		;  //空语句
//	}
//
//
//	printf("请输入密码（Y/N）:>");
//
//	int ch = getchar();
//	if (ch == 'Y') {
//		printf("确认成功\n");
//	}
//	else {
//		printf("确认失败\n");
//
//	}
//	return 0;
//}

//int main() {
//
//	int ch = 0;
//	while ((ch = getchar()) != EOF) {
//		if (ch < '0' || ch >'9')
//			continue;
//		putchar(ch);
//	}
//
//	return 0;
//}


// ****for循环****
// 打印 1 - 10 


//int main() {
//
//	int i = 0; // 初始化
//	while (i <= 10) {// 判断
//
//		printf("%d ", i);
//
//
//
//		i++;       //调整部分
//	}
//	return 0;
//}

// 用for 循环写1-10
//int main() {
//	int i = 0;
//
//	for (i = 1; i <= 10; i++){
//		if (5 == i) {
//			//break; // 1234
//			//continue; // 1234678910
//		}
//		printf("%d ", i);
//
//	}
//	return 0;
//}

//int main() {
//	int i = 0;
//	for (i = 0; i < 10; i++) {// 建议写成前闭后开，10表示打印10次 提高可读性
//		printf("hehe\n");
//	}
//
//	return 0;
// }

//int main() {
//
//	// 1. for  的舒适化，判断，调整三个部分都可以省略
//	// 2. 中间的判断部分如果省略，意味着判断恒为真，就构成了死循环
//	// 3. 如果条件允许，不建议省略for循环的3个表达式
//	//for (;;) {
//
//	//	printf("hehe\n");// 死循环打印hehe
//	//}
//
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < 3; i++){
//		for (j = 0; j < 3; j++) {
//			printf("hehe\n");
//		}
//	}
//	return 0;
//}

//int main() {
//	int i = 0;
//	int j = 0;
//	for (; i < 3; i++) {
//		//  j = 3 没再初始化了
//		for (; j < 3; j++) {
//			printf("hehe\n"); // 打印3个hehe
//		}
//	}
//	return 0;
//}

// 一道笔试题
// 下列只循环1次
//int main() {
//	int i = 0;
//	int k = 0; 
//	         // k = 0  赋值0  结果是0  表示假 不进入循环
//	for (i = 0, k = 0; k = 0; k++) {
//		k++;
//	}
//	return 0;
//}
//int main() {
//	int i = 1;
//	do {
//		if (i == 5)
//			//break;//打印 1 2 3 4
//		    continue;// 1 2 3 4死循环
//		printf("%d ", i);
//		i++;
//	} while (i <= 10);
//
//	return 0;
//}

//      练习
//  1.计算n的阶乘

//int main() {
//
//	int n = 0;
//	scanf("%d", &n);// 5
//	int i = 0;
//	int ret = 1;
//	for (i = 1; i <= n; i++) {
//		ret *= i;
//	}
//	printf("%d\n",ret);
//	return 0;
//}

//  2.计算 1!+2!+3!+4！+.....n!
//int main() {
//	int n = 0;//共n个数字
//	scanf("%d", &n);
//	int i = 0;// j  内部的第 i 个数字
//	int j = 0;// n  内部的第 j 个数字
//	int sum = 0;
//
//	for (j = 1; j <= n; j++) {// 
//		int ret = 1;
//		for (i = 1; i <= j; i++) {
//			ret *= i;
//		}
//		sum += ret;
//	}
//	printf("%d\n", sum);
//	return 0;
//}

//优化算法
//int main() {
//	int n = 0;
//	int i = 0;
//	int ret = 1;
//	int sum = 0;
//	for (n = 1; n <= 10; n++) {
//		ret = ret * n;
//		sum += ret;
//	}
//	printf("%d\n", sum);
//	return 0;
//}

//  在一个有序数组中查找具体的某个数字n(二分查找)
//int main() {
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int k = 7;
//	int i = 0;
//	for (i = 0; i < 10; i++) {
//		if (arr[i] == k) {
//			printf("找到了，下标是%d\n", i);
//			break;
//		}
//		else if (i == 10) {
//			printf("找不到\n");
//		}
//	}
//	return 0;
//}
// 二分法 -- 折半查找
//int main() {
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	 //         40       /    4  = 10
//	int k = 7;
//	int left = 0;
//	int right = sz - 1;
//
//
//	while (left <= right) {
//		int mid = (left + right) / 2;
//		if (arr[mid] < k) {
//			left = mid + 1;
//		}
//		else if (arr[mid] > k) {
//			right = mid - 1;
//		}
//		else {
//			printf("找到了，下标是%d \n", mid);
//			break;
//		}
//	}
//	if (left > right) {
//		printf("找不到\n");
//	}
//	return 0;
//}

#include <string.h>
#include <windows.h>
int main() {
	char arr1[] = "Hello World!!!!!!!";
	char arr2[] = "##################";

	int left = 0;
	int right = strlen(arr1)-1;
	while (left <= right) {
		arr2[left] = arr1[left];
		arr2[right] = arr1[right];
		printf("%s\n", arr2);
		Sleep(1000); // 睡眠函数 - 单位是毫秒
		system("cls"); // 执行系统命令的
		left++;
		right--;
	}
	printf("%s\n", arr2);

	return 0;
}
