
#include <stdio.h>

//进阶指针

//1：字符指针
//2:指针数组
//3:数组指针



//1：字符指针

//int main()
//{
//	/*char ch = 'q';
//	char* pc = &ch;*/
//
//	//不仅仅可以指向一个字符，也可以指向一个字符串
//	char* ps = "hello 2022";
//	char arr[] = "hello 2022";
//
//	//本质上把字符串"hello 2022"首元素'h'的地址给ps
//	//printf("%c\n", *ps);// h
//
//	printf("%s\n", ps);
//	printf("%s\n", arr);
//
//	return 0;
//}

//练习题：
//int main()
//{
//    char str1[] = "hello 2022.";
//    char str2[] = "hello 2022.";
//    const char* str3 = "hello 2022.";
//    const char* str4 = "hello 2022.";
//    if (str1 == str2)
//        printf("str1 and str2 are same\n");
//    else
//        printf("str1 and str2 are not same\n");
//
//    if (str3 == str4)
//        printf("str3 and str4 are same\n");
//    else
//        printf("str3 and str4 are not same\n");
//
//    return 0;
//}

// 2.指针数组

//int main()
//{
	//指针数组本质是数组，数组中存放的是指针（指针）
	//int* arr[4];//存放整形指针的数组
//不好的代码
	//int a = 10;
	//int b = 20;
	//int c = 30;
	//int* arr[3] = { &a,&b,&c };
	//int i = 0;
	//for (i = 0; i < 3; i++)
	//{
	//	printf("%d ", *(arr[i]));
	//} 
	
//	int a[5] = { 1,2,3,4,5 };
//	int b[5] = { 2,3,4,5,6 };
//	int c[5] = { 3,4,5,6,7 };
//
//	int* arr[3] = { a,b,c };
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 5; j++)
//		{
//			//printf("%d ", *(arr[i] + j));
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//3：数组指针
// 
//是一种指针
//整形指针 ---是指向整形的指针
//字符指针 ---是指向字符的指针
// int* p;
 
//int main()
//{
//	int a = 0;
//	//整形指针
//   	int* p = &a;
//
//	char ch = 'w';
//	//字符指针
//	char* pc = &ch;
//
//	int arr[10] = { 1,2,3,4,5 };
//	//数组指针 -- 取出的是数组的地址
//	int(*parr)[10] = &arr;
//	//parr就是一个数组指针 --- 其中存放的是数组的地址
//
//	// arr;数组名是首元素的地址 --- arr[0]的地址
//	//&arr;是数组的地址
//
//	double* d[5];
//	double* (*pd)[5] = &d;//pd 就是一个数组指针
//
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 0 };
//
//	int* p1 = arr;
//	int(*p2)[10] = &arr;
//	printf("%p\n", p1);
//	printf("%p\n", p1+1);
//
//	printf("%p\n", p2);
//	printf("%p\n", p2+1);
//
//
//	//printf("%p\n", arr);
//	//printf("%p\n", &arr);
//
//	return 0;
//}

//数组名是数组首元素的地址 
//但是有两个例外
//1:sizeof(数组名) -- 数组名表示整个数组，计算的是整个数组的大小，单位是字节
//2:&数组名 -  数组名表示的是整个数组，取出的是整个数组的地址

//void print(int arr[3][5], int r, int c)
//{
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < r; i++)
//	{
//		for (j = 0; j < c; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//}
//
//void print2(int(*p)[5], int r, int c)
//{
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < r; i++)
//	{
//		for (j = 0; j < c; j++)
//		{
//			printf("%d ", *(*(p + i) + j));
//		}
//		printf("\n");
//	}
//
//}
//int main()
//{
//	int arr[3][5] = { {1,2,3,4,5},{2,3,4,5,6},{3,4,5,6,7 } };
//
//	//print(arr, 3, 5);
//
//	print2(arr, 3, 5);//arr数组名表示数组首元素的地址
//	//二维数组的数组名表示首元素的地址，二维数组的首元素是：第一行
//
//	return 0;
//}


 