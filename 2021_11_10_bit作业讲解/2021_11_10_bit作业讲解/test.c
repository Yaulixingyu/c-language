#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

//编写一个函数 reverse_string(char * string)（递归实现）
//实现：将参数字符串中的字符反向排列，不是逆序打印。
//要求：不能使用C函数库中的字符串操作函数。
//比如 :
//char arr[] = "abcdef";
//逆序之后数组的内容变成：fedcba

//int my_strlen(char* s)
//{
//	int count = 0;
//	while (*s != '\0')
//	{
//		count++;
//		s++;
//	}
//	return count;
//}
//
//
////[ a b c d e f g \0 ]
//
////递归版本
//// 思想  g + reverse_string("bcdef") + a
//void reverse_string(char* arr)
//{
//	int len = my_strlen(arr);
//	char tmp = *arr;
//	*arr = *(arr + len - 1);
//	*(arr + len - 1) = '\0';
//	if(my_strlen(arr+1) > 1) // 跳出递归条件
//	reverse_string(arr+1);
//
//	*(arr + len - 1) = tmp;
//}
// 
//
//// 参数为指针的形式
////[ a b c d e f g \0 ]
////void reverse_string(char* str)
////{
////	char* left = str; 
////	char* right = str + my_strlen(str)-1;
////	while (left < right) {
////		char tmp = *left;
////		*left = *right;
////		*right = tmp;
////		left++;
////		right--;
////	}
////}
//
//
//
////参数是数组的形式
////void reverse_string(char arr[])
////{
////	int left = 0;
////	int right = my_strlen(arr) - 1;
////	// 交换
////	while (left < right) 
////	{
////		char tmp = arr[left];
////		arr[left] = arr[right];
////		arr[right] =tmp;
////
////		left++; 
////		right-- ;
////	}
////}
//
//int main()
//{
//	char arr[] = "abcdefg";
//	reverse_string(arr);//fedcba
//	printf("%s\n",arr);
//
//	return 0;
//
//}

//编写一个函数实现n的k次方，使用递归实现。

double Pow(int m,int k) {
	if (k > 0)
	{
	    return m * Pow(m, k - 1);
	}
	else if (k ==0)
	{
		return 1;
	}
	else if(k<0)
	{
		return  1.0 / Pow(m, -k);
	}
}
int main() {
	int m = 0;
	int k = 0;
	scanf("%d %d", &m, &k);
	double sum = Pow(m, k);
	printf("%lf", sum);
	return 0;
}