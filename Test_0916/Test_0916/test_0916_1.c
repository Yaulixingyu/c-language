#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//********指针********
//内存： 一个内存单元是一个字节  然后分配地址
// 一个内存单元 byte
//指针：
//int main() {
//
//	int a = 10;// a 在内存中要分配空间 - 4个字节
//	printf("%p\n",&a );//%p专门用来打印地址
//    int * pa = &a;//pa是用来存放地址的，在C语言中叫是指针变量
//	// * 说明 pa 是指针变量
//	//int 说明 pa 指向的对象是int类型的
// 
//	char ch = 'w';
//	char * pc = &ch;
//
//	return 0;
//
//}

//int main() {
//
//	int* pa = &pa;
//
//	return 0 ;
//}


// 指针 就是 地址
//int main() {
//
//	int a = 10;
//
//	int* pa = &a;
//
//	*pa = 20;//解引用操作  * pa  就是通过pa 里边的地址，找到a
//	         // 执行结果：20
//	printf("%d\n",a);
//	return 0 ;
//}

//int main() {
//
//	//// 指针的大小是相同的  
//	//// 指针是用来存放地址的！！
//	//// 指针需要多大空间，取决于地址的存储需要多大空间！
//
//	//// 32位机器  32 bit - 4byte
//	//// 64位机器  64 bit - 8byte
//	printf("%d\n", sizeof(char*));
//	printf("%d\n", sizeof(short*));
//	printf("%d\n", sizeof(int*));
//	printf("%d\n", sizeof(long*));
//	printf("%d\n", sizeof(long long*));
//	printf("%d\n", sizeof(float*));
//	printf("%d\n", sizeof(double*));
//	return 0;
//}