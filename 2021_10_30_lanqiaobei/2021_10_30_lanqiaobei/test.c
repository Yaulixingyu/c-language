#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main() {
	int m = 0;
	scanf("%d",&m);
	while (m != 0) {
		int n = m % 10;
		printf("%d", n);
		m = m / 10;
	}

	return 0;
}