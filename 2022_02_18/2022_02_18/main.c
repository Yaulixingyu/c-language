#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//文件操作

//文件：磁盘上的文件
//

//文件指针：每一个使用的文件都会开辟文件信息区
//文件信息区：描述记录当前文件的信息
//结构体 FILE

//文件的打开 -- fopen
#include <errno.h>
#include <string.h>
//int main()
//{
//	//打开文件
//	FILE* pf = fopen("test.txt", "w");//文件名要写绝对路径
//	if (pf == NULL)
//	{
//		//printf("打开文件失败\n");
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	//读（写）文件
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}


//写文件
//int main()
//{
//	FILE* pf = fopen("data.txt", "w");//文件名要写绝对路径
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	//写文件
//	//fputc('a', pf);
//	//fputc('b', pf);
//	//fputc('c', pf);
//	char ch = 0;
//	for (ch = 'a'; ch <= 'z'; ch++)
//	{
//		fputc(ch, pf);
//		//fputc(ch, stdout);输出到显示台
//		//流  标准输出流
//	}
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//写一行
//int main()
//{
//	FILE* pf = fopen("data.txt", "w");//文件名要写绝对路径
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	//写文件
//	fputs("hello world\n", pf);
//	fputs("hehe\n", pf);
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}


//读文件
//int main()
//{
//	FILE* pf = fopen("data.txt", "r");//文件名要写绝对路径
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	//读文件
//
//	//一个一个读
//	//int ch = fgetc(pf);
//	//printf("%c\n", ch);// 'a'
//
//	//ch = fgetc(pf);
//	//printf("%c\n", ch);
//
//	//ch = fgetc(pf);
//	//printf("%c\n", ch);
//
//	//ch = fgetc(pf);
//	//printf("%c\n", ch);
//
//	//读完
//	int ch = 0;
//	//如果读取失败的话返回EOF,EOF是-1，因此要是用int
//	while ((ch = fgetc(pf)) != EOF)
//	{
//		printf("%c ", ch);
//	}
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//读一行

//int main()
//{
//	FILE* pf = fopen("data.txt", "r");//文件名要写绝对路径
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	//读一行文件
//	char buf[1000] = { 0 };
//	fgets(buf, 1000, pf);
//	//1000表示每次读的最大字符数
//	printf("%s", buf);
//
//	fgets(buf, 1000, pf);
//	printf("%s", buf);
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}


//练习
//实现一个代码 将data.txt拷贝一份 生成 data2.txt
//int main()
//{
//	FILE* pr = fopen("data.txt", "r");
//	if (pr == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	FILE* pw = fopen("data2.txt", "w");
//	if (pw == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		fclose(pr);
//		pr = NULL;
//		return 0;
//	}
//	//拷贝文件
//	int ch = 0;
//	while ((ch = fgetc(pr))!= EOF)
//	{
//		fputc(ch, pw);
//	}
//	//关闭文件
//	fclose(pr);
//	pr = NULL;
//	fclose(pw);
//	pw = NULL;
//
//	return 0;
//}


//对于结构化的数据
 
//fprinf - 输出文件信息

struct Stu
{
	char name[20];
	int age;
	double d;
};
//int main()
//{
//	struct Stu s = { "张三",20,95.5 };
//	FILE* pf = fopen("data.txt", "w");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//
//	//写格式化的数据
//	fprintf(pf, "%s %d %lf", s.name, s.age, s.d);
//
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}



//fscanf - 从文件读取信息

//struct Stu
//{
//	char name[20];
//	int age;
//	double d;
//};
//int main()
//{
//	struct Stu s = { 0};
//	FILE* pf = fopen("data.txt", "r");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//
//	//写格式化的数据
//	fscanf(pf, "%s %d %lf", s.name, &(s.age), &(s.d));
//
//	printf("%s %d %lf", s.name, s.age, s.d);
//
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}


//fwrite - 以二进制的形式写
//int main()
//{
//	struct Stu s[2] = { {"张三",20,95.5},{"李四",16,66.5}};
//	FILE* pf = fopen("data.txt", "wb");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	//按照二进制的方式写文件
//	fwrite(s,sizeof(struct Stu),2,pf);
//	
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//二进制的读
//int main()
//{
//	struct Stu s[2] = { 0};
//	FILE* pf = fopen("data.txt", "rb");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	//按照二进制的方式写文件
//	fread(s,sizeof(struct Stu),2,pf);
//	printf("%s %d %lf\n", s[0].name, s[0].age, s[0].d);
//	printf("%s %d %lf\n ", s[1].name, s[1].age, s[1].d);
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//scanf 从标准输入流(stdin)上进行格式化输入的函数
//printf向标准输出流(stdout)上进行格式化的输出函数

//fscanf可以从标准输入流/指定的文件流 上读取格式化的数据
//fprintf把数据按照格式化的方式输出到标准输出流(stdout)/指定的文件流

//sscanf 可以把一个字符串中提取(转化)出格式化数据
//sprintf 可以把

//int main()
//{
//	struct Stu s = { "张三",20,95.5 };
//	struct Stu tmp = { 0 };
//	char buf[100] = { 0 };
//	sprintf(buf,"%s %d %lf", s.name, s.age, s.d);
//
//	printf("%s\n", buf);
//	sscanf(buf, "%s %d %lf", tmp.name, &(tmp.age), &(tmp.d));
//	printf("%s %d %lf", s.name, s.age, s.d);
//	return 0;
//}


//fseek
#include <string.h>
#include <errno.h>
//int main()
//{
//	FILE* pf = fopen("test.txt", "r");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return;
//	}
//	//读文件
//	int ch = fgetc(pf);
//	printf("%c\n", ch);
//
//	ch = fgetc(pf);
//	printf("%c\n", ch);
//	//想读到f
//	//定位文件指针-->挪到想读的地方
//	//3是当前指针的位置
//	//SEEK_CUR 当前位置
//	//SEEK_END 最后位置
//	//SEEK_SET 起始位置
//	//ch = fseek(pf,-1,SEEK_END);
//	//ch = fseek(pf, 5, SEEK_SET);
//	ch = fseek(pf, 3, SEEK_CUR);
//	ch = fgetc(pf);
//	printf("%c\n", ch);
//
//	//关闭
//	fclose(pf);
//	pf = NULL;
//	return 0;
// }

//写
//int main()
//{
//	FILE* pf = fopen("test.txt", "w");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return;
//	}
//	//写文件
//	int ch = 0;
//	for (ch = 'a'; ch <= 'z'; ch++)
//	{
//		fputc(ch, pf);
//	}
//	//定位文件指针-->挪到想读的地方
//    fseek(pf, -1, SEEK_END);
//	fputc('#',pf);
//	//关闭
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//ftell 计算偏移量
//int main()
//{
//	FILE* pf = fopen("test.txt", "r");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return;
//	}
//	//读文件
//	int ch = fgetc(pf);
//	printf("%c\n", ch);
//
//	ch = fgetc(pf);
//	printf("%c\n", ch);
//
//	int ret = ftell(pf);
//	printf("%d\n", ret);
//	//关闭
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//fread
//int main()
//{
//	FILE* pf = fopen("test.txt", "r");
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return;
//	}
//	//读文件
//	int ch = fgetc(pf);
//	printf("%c\n", ch);
//
//	ch = fgetc(pf);
//	printf("%c\n", ch);
//
//	int ret = ftell(pf);
//	printf("%d\n", ret); // 2
//	
//	rewind(pf);
//	//fseek(pf,0,SEEK_SET)
//	ret = ftell(pf);
//	printf("%d\n", ret);
//
//	//关闭
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}

//文本文件和二进制文件

//数据在内存中以二进制的形式存储，如果不加转换的输出到外存，
//就是二进制文件，

//如果要求在外存上以ASCII码的形式存储，则需要在存储前转换，
//以ASCII字符的形式存储的文件就是文本文件



//文件读取结束的判定
 
//不能用feof来判断文件是否读取结束
 

//文件缓冲区
