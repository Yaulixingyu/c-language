#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//杨氏矩阵
//
//有一个数字矩阵，矩阵的每行从左到右是递增的，矩阵从上到下是递增的，请编写程序在这样的矩阵中查找某个数字是否存在。
//
//要求：时间复杂度小于O(N);
// 
//
// 1 2 3
// 2 3 4
// 3 4 5
//
// 1 2 3 
// 4 5 6
// 7 8 9
//

void find_int_arr(int arr[3][3], int r, int c,int k)
{
	int x = 0;
	int y = c - 1;
	while (y >= 0 && x <= r-1)
	{
		if (arr[x][y] < k) { x++; }
		else if (arr[x][y] > k) { y--; }
		else {
			printf("找到了,下标是:x = %d y = %d\n", x, y);
			return;
		}
	}
	printf("找不到\n");
}
//int main()
//{
//	int arr[3][3] = { 1,2,3,4,5,6,7,8,9 };
//
//	int k = 0;
//	scanf("%d", &k);
//	find_int_arr(arr,3,3,k);
//	return 0;
//}

#include <string.h>

int is_left_move(char* arr1, char* arr2)
{
	int len1 = strlen(arr1);
	int len2 = strlen(arr2);
	if (len1 != len2)
		return 0;
	strncat(arr1, arr1, len1);
	if (strstr(arr1, arr2))
		return 1;
	else
		return 0;
}
//int main()
//{
//	char arr1[20] = "AABCD";
//	char arr2[] = "BCDAA";
//	int ret = is_left_move(arr1, arr2);
//	printf("%d\n", ret);
//	return 0;
//}

