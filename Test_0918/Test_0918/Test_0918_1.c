#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//***********结构体********
//结构体：使得C语言有能力用来描述复杂类型
//        可以让C语言创建新的类型出来
//创建学生类型
//struct Stu {
//	char name[20];//成员变量
//	int age;
//	double score;
//};
//
//// 创建书类型
//struct Book {
//	char name [20];
//	float price;
//	char id[30];
//};
//  int main() {
//	  struct Stu s = { "张三",20,85.5 };//结构体的创建和初始化
//      printf("1:%s %d %lf\n",s.name,s.age,s.score);//结构体变量.成员变量
//	  struct Stu *ps = &s;
//	  printf("2:%s %d %lf\n", (*ps).name, (*ps).age, (*ps).score);
//	  printf("3:%s %d %lf\n", ps->name, ps->age, ps->score);
//	               // 结构体指针->结构名
//	return 0;
//}

//int main() {
//	int a = 0;
//	int b = 0;
//	printf("请输入两个整数：\n");
//	scanf("%d %d", &a, &b);
//	if (a > b)
//		printf("更大值为：%d\n", a);
//	else
//		printf("更大值为：%d\n", b);
//
//	return 0;
//}

int main() {
	printf("%d\n", sizeof(int*));
	printf("%d\n", sizeof(short*));
	printf("%d\n", sizeof(double*));


	return 0;
}