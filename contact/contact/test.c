#define _CRT_SECURE_NO_WARNINGS
#include "contact.h"
void menu()
{
	printf("*****   1.add    *******\n");
	printf("*****   2.del    *******\n");
	printf("*****   3.search *******\n");
	printf("*****   4.modify *******\n");
	printf("*****   5.sort   *******\n");
	printf("*****   6.show   *******\n");
	printf("*****   0.exit   *******\n");
}

int main()
{
	int input = 0;
	Contact con;//通讯录
	//初始化通讯录
	InitContact(&con);
	do
	{
		menu();
		printf("请选择：>");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			AddContact(&con);
			break;
		case 2:
			DeleteContact(&con);
			break;
		case 3:
			break;
		case 4:
			break;
		case 5:
			break;
		case 6:
			ShowContact(&con);
			break;
		case 0:
			break;
		default:
			printf("输入错误，请重新输入\n");
		}
	}while (input);
	return 0;
}