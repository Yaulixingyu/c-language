#pragma once
#include <stdio.h>
#include <string.h>
#include <assert.h>

#define MAX 1000
#define NAME_MAX 20
#define SEX_MAX 5
#define ADDR_MAX 30
#define TELE_MAX 12

typedef struct PeoInfo
{
	char name[NAME_MAX];
	int age;
	char sex[SEX_MAX];
	char addr[ADDR_MAX];
	char tele[TELE_MAX];
}PeoInfo;

typedef struct Contact
{
	PeoInfo data;//存放数据
	int sz;
	int capacity;//记录当前通讯录的最大容量
}Contact;

void InitContact(Contact* pc);

//增加联系人道通讯录
void AddContact(Contact* pc);

//打印通讯录中的信息
void ShowContact(const Contact* pc);

//删除联系人
void DeleteContact(Contact* pc);
