#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//int main()
//{
//	int a, b, c;
//	a = 5;
//	c = ++a;  
//	b = ++c, 
//		c++,  
//		++a, 
//		a++;
//	b += a++ + c;
//	printf("a = %d b = %d c = %d\n:", a, b, c);
//	return 0;
//}

//编程实现：两个int（32位）整数m和n的二进制表达中，有多少个位(bit)不同？ 

//输入例子:
//
//1999 2299
//
//输出例子 : 7

//int main()
//{
//	int m = 0;
//	int n = 0;
//	scanf("%d %d", &m, &n);
//	int c = m^n;
//	int count = 0;
//	while (c != 0)
//	{
//		count++;
//		c = c & (c - 1);
//	}
//	printf("%d",count);
//
//	return 0;
//}


//获取一个整数二进制序列中所有的偶数位和奇数位，分别打印出二进制序列
//int main()
//{
//	int m = 0;
//	scanf("%d", &m);
//
//
//
//	return 0;
//}

//写一个函数返回参数二进制中 1 的个数。
//
//比如： 15    0000 1111    4 个 1

//int test(int n,int count)
//{
//	for (int i = 0; i < 32; i++)
//	{
//		if (((n >> i) & 1) == 1)
//		{
//			count++;
//		}
//	}
//	return count;
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int count = 0;
//	count = test(n, count);
//	printf("%d", count);
//	return 0;
//}

//不允许创建临时变量，交换两个整数的内容
int main()
{
	int a = 0, b = 0;
	scanf("%d %d", &a, &b);
	printf("交换前：a=%d,b=%d\n", a, b);
	a = a ^ b;
	b = a ^ b;
	a = a ^ b;
	printf("交换后：a=%d,b=%d\n", a, b);
	return 0;
}