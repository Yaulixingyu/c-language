#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//命名约定
//函数和宏的语法相似
//宏的名字全大写 函数名字不要全大写

//#define MAX(x,y) ((x)>(y)?(x):(y))
//
//int Max(int x, int y)
//{
//	return x > y ? x : y;
//}
//
////#undef 
////取消宏定义
//int main()
//{
//#undef MAX
//	int m = MAX(2, 3);
//	printf("%d\n", m);
//	return 0;
//}


//命令行定义
//
//某些指令可以不在代码中指示，可以在编译的过程中指示
// int arr[SZ] ;


//条件编译
//在编译一个程序的时候我们如果要将一条语句编译或者放弃是很方便的
//1.
//int main()
//{
//	int  i = 0;
//	for (i = 0; i < 10; i++)
//	{
//#if 1
//		printf("%d\n", i);
//#endif	
//		//业务处理
//	}
//	return 0;
//}
//
// 2.多分支
//#define M 100
//
//int main()
//{
//#if M<100
//	printf("less\n");
//#elif M==100
//	printf("==\n");
//#else
//	printf("more\n");
//#endif
//
//
//	return 0;
//}


//3、判断是否被定义
//#define M 0
//int main()
//{
////正面
//#if defined(M)
//	printf("hehe\n");
//#endif
//#ifdef M
//	printf("haha\n");
//#endif
////反面
//#if !defined(M)
//	printf("hehe\n");
//#endif
//	//相同功能
//#ifndef M
//	printf("haha\n");
//#endif
//	return 0;
//}

//嵌套指令

//#include "test.h"
//#include <stdio.h>
//文件的包含
//防止头文件被多次包含
//#pragma once
//或者
//#ifndef _TEST_H_
//#define _TEST_H_
//
//#endif
		
//"  " 和 <	 >
//如果是本地文件也就是第几创建的 用"  "
//如果是库文件 用 <  >

//两者查找策略
//"  " :现在源文件所在目录下查找，再在库中找
//<  > :直接去标准路径下错误,如果找不到就编辑错误



//模拟实现atoi
#include <assert.h>
#include <stdlib.h>
//
//1.空指针
//2.空字符串
//3.+ -
//4.非数字字符
//5.超大数字
//

enum State
{
	INVALID,//非法
	VALID   //合法
};

enum State status = INVALID;
int my_atoi(const char* str)
{
	assert(str);
	//空字符串的问题
	if (*str == '\0')
		return 0;

	//跳过空白字符
	while (isspace(&str))
	{
		str++;
	}

	//+ -
	int flag = 1;
	if (*str == '+')
	{
		str++;
		flag = 1;
	}
	else if(*str == '-')
	{
		str++;
		flag = -1;
	}

	//过滤非法字符
	long long n = 0;
	while (isdigit(*str))
	{
		n = n * 10 +flag*(*str - '\0');
		//越界值
		if (n > INT_MAX || n < INT_MIN)
		{
			return 0;
		}
		str++;
	}

	if (*str == '\0')
	{
		//合法返回
		status = VALID;
		return (int)n;
	}
	return (int)n;
}

int main()
{
	int ret = my_atoi("1234");
	if (status == VALID)
		printf("%d\n", ret);
	else
		printf("非法返回");
	return 0;
}




//查看文件的字符数
//int main()
//{
//	long num = 0;
//	FILE* fp = NULL;
//	if ((fp = fopen("fname.dat", "r")) == NULL)
//	{
//		printf("Can’t open the file! ");
//		exit(0);
//	}
//	while (fgetc(fp) != EOF)
//	{
//		num++;
//	}
//	printf("num=%d\n", num);
//	fclose(fp);
//	return 0;
//}

//#define INT_PTR int*
//typedef int* int_ptr;
//INT_PTR a, b;
//int_ptr c, d;

//#define SWAP(x,y) ((x&0xaaaaaaaa)>>1)+((x&0x55555555)<<1)
//int main()
//{
//	int a = 10;
//	printf("%d\n", SWAP(a));
//	return 0;
//}
#include <stddef.h>
struct S
{
	char c;
	int a;
	double d;
};
#define OFFSETOF(s_name,m_name)  (int)&(((s_name*)0)->m_name)
//int main()
//{
//
//	printf("%d\n", OFFSETOF(struct S, c));
//	printf("%d\n", OFFSETOF(struct S, a));
//	printf("%d\n", OFFSETOF(struct S, d));
//	return 0;
//}