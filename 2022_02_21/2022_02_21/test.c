#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//int main()
//{
//	printf("%d", 1 ^ (-1)); //异或
//	return 0; 
//}

//int findMaxConsecutiveOnes(int* nums, int numsSize)
//{
//    int i = 0;
//    int count = 0;
//    int Maxcount = 0;
//    for (i = 0; i < numsSize; i++)
//    {
//        if (nums[i] == 1)
//        {
//            count++;
//        }
//        if (Maxcount < count)
//        {
//            Maxcount = count;
//        }
//        if (nums[i] == 0)
//        {
//            count = 0;
//        }
//    }
//    return Maxcount;
//}

//#include <stdio.h>
//int main()
//{
//    int n = 0;
//    //多组输入
//    while ((scanf("%d", &n)) != EOF)
//    {
//        int i = 0;
//        int j = 0;
//        int count = 0;
//        for (i = 1; i < n; i++)
//        {
//            int sum = 0;
//            //寻找因数法  -- 暴力查找法
//            for (j = 1; j < i; j++)
//            {
//                if (i % j == 0)
//                {
//                    sum += j;
//                }
//            }
//            if (sum == i)
//            {
//                count++;
//            }
//        }
//        printf("%d\n", count);
//    }
//    return 0;
//}

 
//单次倒排
//https://www.nowcoder.com/practice/81544a4989df4109b33c2d65037c5836?tpId=37&&tqId=38366&rp=1&ru=/ta/huawei&qru=/ta/huawei/question-ranking
#include <stdio.h>
#include <stdlib.h>

//int main() {
//    char str[100][22];
//    int i = 0;
//    int x;
//    while (1) {
//        x = scanf("%[a-z|A-Z]", str[i]);
//        if (getchar() == '\n') break;
//        if (x) i++;
//    }
//    for (int j = i; j >= 0; j--) {
//        printf("%s ", str[j]);
//    }
//    return 0;
//}

//int main()
//{
//	int n = 0;
//	int x = 2014;
//	while (x + 1)
//	{
//		n++;
//		x = x | (x + 1);
//	}
//	printf("%d", n);
//	return 0;
//}



//https://www.nowcoder.com/practice/20ef0972485e41019e39543e8e895b7f?tpId=188&&tqId=38589&rp=1&ru=/activity/oj&qru=/ta/job-code-high-week/question-ranking
/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 *
 * @param numbers int整型一维数组
 * @param numbersLen int numbers数组长度
 * @param target int整型
 * @return int整型一维数组
 * @return int* returnSize 返回数组行数
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
//int* twoSum(int* numbers, int numbersLen, int target, int* returnSize) {
//    // write code here
//    int* ptr = (int*)calloc(2, sizeof(int));
//    for (int i = 0; i < numbersLen; i++)
//    {
//        if (numbers[i] > target)
//            continue;
//        for (int j = i + 1; j < numbersLen; j++)
//        {
//            int sum = numbers[i] + numbers[j];
//            if (sum == target)
//            {
//                ptr[0] = i + 1;
//                ptr[1] = j + 1;
//                *returnSize = 2;
//                return ptr;
//            }
//        }
//    }
//    *returnSize = 0;
//    return 0;
//}


//异或寻找单身数
#include<stdio.h>
int main()
{
    int n = 0;
    int num = 0;
    int ret = 0;
    scanf("%d", &n);
    for (int i = 0; i < n; ++i)
    {
        scanf("%d", &num);
        ret ^= num;
    }
    printf("%d", ret);
}


// https://www.nowcoder.com/practice/fcf87540c4f347bcb4cf720b5b350c76?tpId=188&&tqId=38666&rp=1&ru=/activity/oj&qru=/ta/job-code-high-week/question-ranking
// 二分法
/**
 * 代码中的类名、方法名、参数名已经指定，请勿修改，直接返回方法规定的值即可
 *
 *
 * @param nums int整型一维数组
 * @param numsLen int nums数组长度
 * @return int整型
 *
 * C语言声明定义全局变量请加上static，防止重复定义
 */
int findPeakElement(int* nums, int numsLen)
{
    int left = 0;
    int right = numsLen - 1;
    while (left < right)
    {
        int mid = ((right - left) >> 1) + left; //防止直接相加发生溢出
        if (nums[mid] < nums[mid + 1])
            left = mid + 1;
        else
            right = mid;
    }
    return left;
}
