#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
//#define MAXN 10
//
//int even(int n);
//int OddSum(int List[], int N);
//
//int main()
//{
//    int List[MAXN], N, i;
//
//    scanf("%d", &N);
//    printf("Sum of ( ");
//    for (i = 0; i < N; i++) {
//        scanf("%d", &List[i]);
//        if (even(List[i]) == 0)
//            printf("%d ", List[i]);
//    }
//    printf(") = %d\n", OddSum(List, N));
//
//    return 0;
//}
//
//int even(int n) {
//    if (n % 2 == 0) {
//        return 1;
//    }
//    return 0;
//}

//int OddSum(int List[], int N) {
//    int i, sum = 0;
//    for (i = 0; i < N; i++) {
//        if (even(List[i]) == 0)
//            sum += List[i];
//    }
//    return sum;
//}

// 函数 计算坐标两点间的距离
//double dist(double x1, double y1, double x2, double y2);
//int main()
//{
//    double x1, y1, x2, y2;
//
//    scanf("%lf %lf %lf %lf", &x1, &y1, &x2, &y2);
//    printf("dist = %.2f\n", dist(x1, y1, x2, y2));
//
//    return 0;
//}
//double dist(double x1, double y1, double x2, double y2)
//{
//    int x, y;
//    x = pow(x1 - x2, 2);
//    y = pow(y1 - y2, 2);
//    return sqrt(x + y);//开平方根sqrt，不要忘了
//}

// 利用函数计算素数个数并求和

//int prime(int m);
//int main(int argc, char const* argv[])
//{
//	int m, n, i;
//	int sum = 0, count = 0;
//	printf("请输入两个正整数:");
//	while (1)
//	{
//		scanf("%d%d", &m, &n);
//		if (m >= 1 && m <= 500 && n >= 1 && n <= 500 && m <= n)
//			break;
//		printf("请重新输入:");
//	}
//
//	for (i = m; i <= n; i++)
//	{
//		if (prime(i))
//		{
//			count++;
//			sum += i;
//		}
//	}
//
//	printf("%d和%d之间有%d个素数,它们的和为%d\n", m, n, count, sum);
//
//	return 0;
//}
//
//int prime(int m)
//{
//	int i;
//	int ret = 1; 
//
//	if (m == 1)
//		ret = 0;
//	for (i = 2; i <= sqrt(m); i++)
//	{
//		if (m % i == 0)
//		{
//			ret = 0;
//			break;
//		}
//
//	}
//
//	return ret;
//}

// 使用函数输出水仙花数
//#include <stdio.h>
//
//int narcissistic(int number);
//void PrintN(int m, int n);
//
//int main()
//{
//    int m, n;
//
//    scanf("%d %d", &m, &n);
//    if (narcissistic(m)) printf("%d is a narcissistic number\n", m);
//    PrintN(m, n);
//    if (narcissistic(n)) printf("%d is a narcissistic number\n", n);
//
//    return 0;
//}
//int narcissistic(int number) {
//	int exm, sum = 0, cnt = 0;
//	exm = number;
//	while (exm) {
//		cnt++;
//		exm /= 10;
//	}
//	exm = number;
//	int i;
//	while (exm) {
//		i = exm % 10;
//		sum += pow(i, cnt);
//		exm /= 10;
//	}
//	if (sum == number) {
//		return 1;
//	}
//	else {
//		return 0;
//	}
//}
//
//void PrintN(int m, int n) {
//	for (int i = m + 1; i < n; i++) {
//		if (narcissistic(i)) {
//			printf("%d\n", i);
//		}
//	}
//}

// 使用函数求余弦函数的近似值
double funcos(double e, double x);

int main()
{
	double e, x;

	scanf("%lf %lf", &e, &x);
	printf("cos(%.2f) = %.6f\n", x, funcos(e, x));

	return 0;
}

double funcos(double e, double x)
{
	int i, flag = 1, j = 2;
	int s1 = 2, f = 1;
	double cos = 1;

	if (1 < e)                          //判断第一项是不是最后一项
		j = 0;
	while (flag == 1)                //这个while循环找出最后一项，将最后一项的指数赋值给j
	{
		double x1 = x, y1 = 1;
		double last;

		for (int k = 1; k < j; k++)      //计算第j项的分子和分母
		{
			x1 = x1 * x;
			y1 = y1 * (k + 1);
		}

		last = x1 / y1;

		if (last < e)                 //判断是不是最后一项
		{
			flag = 0;
			continue;
		}
		else j += 2;               //如果不是最后一项j+2
	}

	if (j == 0)                       //如果第一项是最后一项，则返回1.0
		return 1.0;

	while (s1 <= j)                    //计算cos(x)的值
	{
		double x1 = x, y1 = 1;
		double last;

		for (i = 1; i < s1; i++)       //计算第s1项的值，将其赋值给last
		{
			x1 = x1 * x;
			y1 = y1 * (i + 1);
			if (f == 1)            //偶数项为负，正数项为正
				last = -x1 / y1;
			else last = x1 / y1;
			f = -f;
		}
		s1 += 2;
		cos += last;
	}
	return cos;
}