#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>


//// 反向打印一个数字
//int main() {
//	int n = 0;
//	scanf("%d", &n);
//	while (n!=0) {
//		printf("%d", n % 10);
//		n = n / 10;
//	}
//	return 0;
//}

//int main() {
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	//输入
//	scanf("%d %d %d", &a, &b, &c);
//	//计算
//	int tmp = 0;
//	// 先把 a 和 b  的 较大值放在 a 中  
//	if (a < b) {
//		tmp = a;
//		a = b;
//		b = tmp;
//	}
//	if (a < c) {
//		tmp = a;
//		a = c;
//		c = tmp;
//	}
//	if (b < c) {
//		tmp = b;
//		b = c;
//		c = tmp;
//	}
//	//打印
//	printf("%d %d %d", a, b, c);
//	return 0;
//}
// 模拟用户登录


//  strcmp - string compare
// 返回0，说明两个字符串相等
//int main() {
//	int i = 0;
//	char password[20] = "";
//	 假设密码是"123456"
//	for (i = 0; i < 3; i++) {
//		printf("请输入密码：>");
//		scanf("%s", password);//password是数组名，本来就是地址
//		if (strcmp(password,"123456")==0) { // 比较两个字符串是否相等，不能使用==，应该使用strcmp这个函数
//			printf("登录成功\n");
//			break;
//		}
//		else {
//			printf("密码错误\n");
//		}
//	}
//	if (i == 3) {
//		printf("3次密码均错误，退出程序\n");
//	}
//
//	return 0;
//
//}

// 猜数字游戏

// 电脑随机生成 1 - 100的数字
// 猜数字
// c语言中 生成的随机数的方法是rand函数
// 范围 0-RAND_MAX（0-32767）
void menu() {
	printf("*******************************\n");
	printf("*********** 1. play   *********\n");
	printf("*********** 0. exit   *********\n");
	printf("*******************************\n");
}
void game() {
	int guess = 0;
	//猜数字游戏的过程
	// 生成随机数
	int r = rand()%100+1;
	//printf("%d\n",r);
	//猜数字
	while (1) {
		printf("猜数字：>");
		scanf("%d", &guess);
		if (guess < r) {
			printf("猜小了\n");
		}
		else if (guess > r) {
			printf("猜大了\n");
		}
		else if (guess == r) {
			printf("猜对了\n");
			break;
		}
	}
}
int main() {
	int input = 0;
	srand((unsigned int)time(NULL));// 时间戳  -- 设置随机数的生成器
	do {
		
		// 打印菜单
		menu();
		printf("请选择：>");
		scanf("%d",&input);
		switch (input) {
		case 1:
			game();
			break;
		case 0:
			printf("退出游戏\n"); 
			break;
		default:
			printf("选择错误\n");
			break;
		}
	} while(input);


	return 0;
}