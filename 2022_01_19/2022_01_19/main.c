#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	return 0;
//}

//strlen
#include  <string.h>
#include  <assert.h>
int My_strlen(const char* str)
{
	assert(str);
	int count = 0;
	while (*str != '\0')
	{
		count++;
		str++;
	}
	return count;
}
//int main()
//{
//	int len =My_strlen("abcdef");
//	printf("%d\n", len);
//	return 0;
//}

//strlen返回的是size_t,是无符号数
//int main()
//{
//	if (strlen("abc") - strlen("abcdef") > 0)
//	{
//		printf(">");
//	}
//	else
//	{
//		printf("<=");
//	}
//	return 0;
//}


//strcmp

//拷贝的原字符串必须以'\0'结束,否则
//strcmp不会考虑目标空间是不是足够大
// 目标空间可变 不能是const

char* my_strcpy(char* dest, const char* src)
{
	char* ret = dest;
	assert(dest && src);
	while (*dest++ = *src++)
	{
		;
	}
	return ret;
}

//int main()
//{
//	char arr1[] = { 'a','b','c','d','e','\0'};
//	//char arr1[] = "abcdef";
//	char arr2[20] = { 0 };
//	
//	//strcpy(arr2, arr1);
//	my_strcpy(arr2, arr1);
//	printf("%s\n", arr2);
//	return 0;
//}

//strcat  -- 字符串连接
char* my_strcat(char* dest, const char* src)
{
	char* ret = dest;
	assert(dest && src);
	//1：找到目标空间中的'\0'
	while (*dest)
	{
		dest++;
	}
	//2：追加内容到目标空间
	while (*dest++ = *src++)
	{
		;
	}
	return ret;

}

//int main()
//{
//	char arr1[30] = "hello";
//	char arr2[20] = "world";
//	//strcat(arr1, arr2);
//	my_strcat(arr1, arr2);
//	printf("%s\n", arr1);
//	return 0;
//}



//strcmp  - 字符串比较
// 比较的是对应位置上的字符大小
// 大于 return 1;
// 等于 return 0；
// 小于 return -1;
int my_strcmp(const char* str1, const char* str2)
{
	assert(str1 && str2);
	while (*str1 == *str2)
	{
		if (*str1 == '\0') return 0;
		str1++;
		str2++;
	}
	if (*str1 > *str2) return 1;
	else return -1;

}
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[] = "bbq";
//	int ret = my_strcmp(arr1, arr2);
//	printf("%d\n", ret);
//
//	return 0;
//}



//strncpy
//strncat
//strcmp

//int main()
//{
//	char arr1[] = "xxxxxxxxxxx";
//	char arr2[] = "hello world";
//	strncpy(arr1, arr2, 5);
//	printf("%s\n", arr1);
//
//	return 0;
//}

char* my_strstr(const char* str, const char* substr)
{
	const char* s1 = str;
	const char* s2 = substr;
	char* cur = str;

	assert(str && substr);
	if (*substr == '\0')
	{
		return (char*)str;
	}

	while (*cur)
	{
		s1 = cur;
		s2 = substr;
		while (*s1 && *s2 && *s1 == *s2)
		{
			s1++;
			s2++;
		}
		if (*s2 == '\0')
			return (char*)cur;
		cur++;
	}
	return NULL;
}

//KMP算法
int main()
{
	char arr1[] = "abbbcdef";
	char arr2[] = "bcd";
	
	char* ret = my_strstr(arr1, arr2);

	if (NULL == ret)
		printf("没找到\n");
	else
		printf("%s\n", ret);

	return 0;

}
