#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

/*编写一个函数 reverse_string(char* string)（递归实现）
实现：将参数字符串中的字符反向排列，不是逆序打印。
要求：不能使用C函数库中的字符串操作函数。*/


/*写一个递归函数DigitSum(n)，输入一个非负整数，返回组成它的数字之和
例如，调用DigitSum(1729)，则应该返回1+7+2+9，它的和是19
输入：1729，输出：19*/
int DigitSum(int n) {
	if(n!=0)
	n = n % 10;
	return n+DigitSum(n / 10);
}

int main() {
	int num = 0;
	scanf("%d",&num);
	DigitSum(num);
	printf("%u",num);
	return 0;
}


/*编写一个函数实现n的k次方，使用递归实现。*/




/*递归和非递归分别实现求第n个斐波那契数
例如：
输入：5  输出：5
输入：10， 输出：55
输入：2， 输出：1*/


