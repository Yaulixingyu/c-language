#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
#include <errno.h>
//文件操作

//实现一个代码 将data.txt拷贝一份 生成 data2.txt
//int main()
//{
//	FILE* pr = fopen("data.txt", "r");
//	if (pr == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	FILE* pw = fopen("data2.txt", "w");
//	if (pw == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		fclose(pr);
//		pr = NULL;
//		return 0;
//	}
//	//拷贝文件
//	int ch = 0;
//	while ((ch = fgetc(pr))!= EOF)
//	{
//		fputc(ch, pw);
//	}
//	//关闭文件
//	fclose(pr);
//	pr = NULL;
//	fclose(pw);
//	pw = NULL;
//
//	return 0;
//}
 


//读文件
int main()
{
	FILE* pf = fopen("data.txt", "r");//文件名要写绝对路径
	if (pf == NULL)
	{
		printf("%s\n", strerror(errno));
		return 0;
	}
	//读文件

	//一个一个读
	//int ch = fgetc(pf);
	//printf("%c\n", ch);// 'a'

	//ch = fgetc(pf);
	//printf("%c\n", ch);

	//ch = fgetc(pf);
	//printf("%c\n", ch);

	//ch = fgetc(pf);
	//printf("%c\n", ch);

	//读完
	int ch = 0;
	//如果读取失败的话返回EOF,EOF是-1，因此要是用int
	while ((ch = fgetc(pf)) != EOF)
	{
		printf("%c ", ch);
	}
	//关闭文件
	fclose(pf);
	pf = NULL;
	return 0;
}

//读一行
//int main()
//{
//	FILE* pf = fopen("data.txt", "r");//文件名要写绝对路径
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	//读一行文件
//	char buf[1000] = { 0 };
//	fgets(buf, 1000, pf);
//	//1000表示每次读的最大字符数
//	printf("%s", buf);
//
//	fgets(buf, 1000, pf);
//	printf("%s", buf);
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}



//写文件
//写一行
//int main()
//{
//	FILE* pf = fopen("data.txt", "w");//文件名要写绝对路径
//	if (pf == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	//写文件
//	fputs("hello world\n", pf);
//	fputs("hehe\n", pf);
//
//	//关闭文件
//	fclose(pf);
//	pf = NULL;
//	return 0;
//}


//异或
//int main()
//{
//	int a = 1;
//	int b = 2;
//	int c = 1 ^ 2;
//	printf("%d\n", c);
//
//	return 0;
//}


//圆周率的计算 -- 格雷戈里公式
//double funpi(double E)
//{
//    double pi = 0, deno = 1, sign = 1;//pi为派的值 deno是分母(denominator)的缩写 sign是当前项的正负值 
//    while (1)
//    {
//        pi += sign / deno;//每项相加 
//        sign *= (-1.0);//隔项变号 
//        deno += 2;//分母隔项自加2 
//        if (1 / (deno - 2) < E)//对于精度绝对值小于e的表述 
//        {
//            break;//如果精度小于e 则跳出循环 
//        }
//    }
//    return pi;//返回pi值 
//}
//int main()
//{
//    double e;//给出精度e
//    scanf("%lf", &e);//输入e的值 
//    printf("pi = %.6f", 4 * funpi(e));//按照题目要求打印 打印的值为4倍的funpi(e)函数 
//    return 0;
//}


//兔子问题
//已知一对兔子每一个月可以生一对小兔子，而一对兔子出生后.第三个月开始生小兔子, 假如没有发生死亡，则每个月有多少兔子？
//递归法
//int rsum(month)
//{
//	if (month <= 2)
//	{
//		return 2;
//	}
//	else
//		return rsum(month - 1) + rsum(month - 2);
//}
//
//int main()
//{
//	int month = 0;
//	scanf("%d", &month);
//	int sum = rsum(month);
//	printf("兔子数有%d\n", sum);
//
//	return 0;
//}


//递推法
//分析完：斐波那契数列 
int main()
{
	int a = 2;
	int b = 2;
	int c = 2;
	int month = 0;
	scanf("%d", &month);
	while ((month-2)>0)
	{
		c = a + b;
		a = b;
		b = c;
		month--;
	}
	printf("month月后有%d只兔子\n", c);
	return 0;
}


//排序问题
//选择排序 :
//第1趟，在待排序记录r[1]~r[n]中选出最小的记录，将它与r[1]交换
//第2趟，在待排序记录r[2]~r[n]中选出最小的记录，将它与r[2]交换
//以此类推，第i趟在待排序记录r[i]~r[n]中选出最小的记录，将它与r[i]交换，使有序序列不断增长直到全部排序完毕。
//int main()
//{
//	int arr[10] = { 9,8,7,6,5,4,3,2,1,0 };
//	//选择排序
//	for (int i = 0; i < 9; i++)
//	{
//		int j = 0;
//		for (j = i+1; j < 10; j++)
//		{
//			if (arr[i] > arr[j])
//			{
//				//交换
//				int temp = 0;
//				temp = arr[i];
//				arr[i] = arr[j];
//				arr[j] = temp;
//			}
//		}
//	}
//	//打印
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

 
//冒泡排序
//int main()
//{
//	int arr[10] = { 9,8,7,6,5,4,3,2,1,0 };
//	for (int i = 0; i < 10; i++)
//	{
//		for (int j = i + 1; j < 10; j++)
//		{
//			if (arr[i] > arr[j])
//			{
//				//交换
//				int temp = 0;
//				temp = arr[i];
//				arr[i] = arr[j];
//				arr[j] = temp;
//			}
//		}
//	}
//
//	for (int i = 0; i < 10; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}



//百钱买百鸡
//  1鸡翁 -- 5 
//  1鸡母 -- 3
//  3鸡雏 -- 1
//int main()
//{
//	int dad = 0;
//	int mom = 0;
//	int child = 0;
//	for (dad = 0; dad <= 100; dad++)
//	{
//		for (mom = 0; mom <= 100; mom++)
//		{
//			for (child = 100 - dad - mom; child <= 100; child++)
//			{
//				if ((dad * 15 + mom * 9 + child  == 300) && (mom + dad + child == 100))
//				{
//					printf("dad = %d,mom = %d,child = %d\n", dad, mom, child);
//					break;
//					
//				}
//			}
//		}
//	}
//
//	return 0;
//}

//物不知数
//int main()
//{
//	int i = 1;
//	while (i)
//	{
//		if (i % 3 == 2 && i % 5 == 3 && i % 7 == 2)
//		{
//			printf("%d", i);
//			break;
//		}
//		i++;
//	}
//
//	return 0;
//}


//成绩转换
//int main()
//{
//	int score = 0;
//	scanf("%d", &score);
//	if (score >= 90 && score <= 100)
//	{
//		printf("A\n");
//	}
//	else if (score >= 80 && score <= 89)
//	{
//		printf("B\n");
//	}
//	else if (score >= 70 && score <= 79)
//	{
//		printf("C\n");
//	}
//	else if (score >= 60 && score <= 69)
//	{
//		printf("D\n");
//	}
//	else if (score >= 0 && score <= 59)
//	{
//		printf("E\n");
//	}
//	return 0;
//}


//闰年的判断
//是4的倍数且不是100的倍数  或者 是400的倍数
//int main()
//{
//	int year = 0;
//	scanf("%d", &year);
//	if ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0))
//	{
//		printf("%d是闰年\n", year);
//	}
//	else
//	{
//		printf("%d不是闰年\n", year);
//	}
//
//	return 0;
//}


//海伦三角形 秦九韶
//int main()
//{
//	int a = 0;
//	int b = 0;
//	int c = 0;
//	//输入 a b c 三边
//	scanf("%d %d %d", &a, &b, &c);
//	//判断是否构成三角形 --->利用两边之和大于第三边
//	if (a + b > c && a + c > b && b + c > a)
//	{
//		double s = (a + b + c) / 2.0;s
//		double area = sqrt(s * (s - a) * (s - b) * (s - c));
//		printf("%lf", area);
//	}
//	else
//	{
//		printf("三边不能构成三角形\n");
//	}
//
//	return 0;
//}
