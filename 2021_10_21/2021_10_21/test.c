#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//初始C语言（四）

//关键字是不能自己创建的，不能是变量名
//   
// static --静态的
// C语言中可以修饰：
//  1：局部变量
//  2: 全局变量
//  3: 函数


// 1： 修饰局部变量
//void 表示函数不需要函数返回任何值
//void test() {
//	int a = 1;
//	a++;
//	printf("%d\n", a);
//}
//int main1(){
//	int i = 0;
//	while (i < 10) {
//		test();      //执行结果 10个2 
//		i++;
//	}
//	return 0;
//}


//void test() {
//	static int a = 1; 
//	// a本来是局部变量  static 修饰局部变量的时候，其实改变了变量的存储类型（栈区存储->静态区）
//	// 所以使得这个静态的局部变量出了自己的作用域也不会销毁 其实相当于改变了这个局部变量的生命周期
//	// 不销毁 作用域是test函数  生命周期变成函数的生命周期
//	// 内存是一块比较大的存储空间，在使用内存时会划分不同的功能区域
//	// 在学习编程语言的时候： 栈区:局部变量
//	//                    堆区：动态内存分配(malloc,free,calloc,realloc)
//	//                   静态区:全局变量 静态变量
//	a++;
//	printf("%d\n", a);
//}
//int main() {
//	int i = 0;
//	while (i < 10) {
//		test();      //执行结果 2 3 4 5 6 7 8 9 10 11
//		i++;
//	}
//	return 0;
//}

// 2：修饰全局变量
//一个全局变量在整个工程的其他文件内能被使用。是因为全局变量具有外部链接属性
//当一个全局变量被static修饰的时候，这个变量的外部链接属性就变成了内部连接属性
//使得这个全局变量只能在自己所在的源文件内部使用，其他文件不能再使用
//static 修饰全局变量，让全局变量的外部链接属性变成了内部链接属性，作用域变小了  生命周期不变
//extern int g_val;//声明外部符号
//int main() {
//	printf("%d\n",g_val);
//	return 0;
//}

//3.修饰函数
// static修饰函数的时候，函数本来也是具有外部链接属性的，被static修饰的时候，就变成了内部链接属性
// 这个函数只能在自己所在的源文件内部使用，不能再其他文件内部使用
// 给我们感觉是改变了作用域
//extern int Add(int x, int y);
//int main() {
//	int a = 10;
//	int b = 20;
//	int ret = Add(a, b);
//	printf("%d\n", ret);
//	return 0;
//}


// #define 定义常量和宏

// #define 定义标识符常量
//#define M 100
//#define STR "hehe"
//int main() {
//	printf("%d\n", M);
//	printf("%s\n", STR);
//	return 0;
//}

// #define 定义宏
//#define ADD(X,Y) ((X)+(Y))
//int main() {
//	int a = 10;
//	int b = 20;
//	int ret = ADD(a, b);
//	printf("%d\n", ret);
//
//	return 0;
//}

//*******指针（指针眼里什么都是地址）*********
//int main() {
//	// & 取地址操作符
//	// %p --是以地址的形式打印
//	int a = 10;//a要在内存中开辟空间
//	printf("%p\n",&a);
//	int *p = &a;//p就是一个指针变量
//	//*p  --- *  解引用操作符好
//	// int是说明，
//	printf("%p\n",&*p);
//	char ch = 'w';
//	char* pc = ch;
//	printf("%p",&*pc);
//	return 0;
//}
//// 地址
//// 地址是放在指针变量中
//
//int main() {
//    int seconds = 0;
//    scanf("%d", &seconds);
//    int h = seconds / (60 * 60);
//    int m = seconds / 60 - h * 60;
//    int s = (seconds % 3600) % 60;
//    printf("%d %d %d", h, m, s);
//
//
//    return 0;
//}
//
//#include <stdio.h>
//int main() {
//    int g1 = 0, g2 = 0, g3 = 0, g4 = 0, g5 = 0;
//    scanf("%d %d %d %d %d", &g1, &g2, &g3, &g4, &g5);
//    double average = (g1 + g2 + g3 + g4 + g5) / 5.0;
//    printf("%.1lf", average);
//
//    return 0;
//}
//#include <stdio.h>
//int main() {
//    printf("Name  Age  Gender\n");
//    printf("---------------------\n");
//    printf("Jack  18   man\n");
//
//    printf("Name    Age    Gender\n");
//    printf("---------------------\n");
//    printf("Jack    18     man");
//
// 
//}
//#include <stdio.h>
//int main() {
//    int g1 = 0, g2 = 0, g3 = 0, g4 = 0, g5 = 0;
//    scanf("%d %d %d %d %d", &g1, &g2, &g3, &g4, &g5);
//    double average = (g1 + g2 + g3 + g4 + g5) / 5.0;
//    printf("%.1lf", average);
//
//    return 0;
//}
//#include <stdio.h>
//int main() {
//    int x = 0;
//    scanf("%d", &x);
//    int a1 = x / 1000;
//    int b1 = x / 100 - a1 * 10;
//    int c1 = x / 10 - a1 * 100 - b1 * 10;
//    int d1 = x % 10;
//    printf("%d%d%d%d", d1, c1, b1, a1);
//
//    return 0;
//}
