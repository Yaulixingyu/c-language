#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <assert.h>

//void test()
//{
//	printf("hehe\n");
//}
//
//int main()
//{
//	int arr[10] = { 0 };
//	int i = 0;
//	test();//逐语句 进入函数内部 按 F11
//	//F10 逐过程  
//	for (i = 0; i < 10; i++)
//	{
//		arr[i] = i + 1;
//		printf("%d ", arr[i]);
//
//	}
//	
//	return 0;
//}

//利用函数调用堆栈 查看函数调用的顺序
  
 
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	//计算n的阶乘
//	int i = 0;
//	int j = 0;
//	int sum = 0;
//
//	for (j = 1; j <= n; j++)
//	{
//		int ret = 1;
//		for (i = 1; i <= j; i++)
//		{
//			ret *= i;
//		}
//		sum += ret;
//	}
//	printf("%d", sum);
//
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	int arr[10] = { 0 };
//	for (i = 0; i <= 12; i++)
//	{
//		arr[i] = 0;
//		printf("hehe\n"); // 死循环打印hehe
//		//
//		//  i 和 arr 是局部变量
//		//  局部变量是放在栈区上的
//		//  栈区上内存的使用习惯是：先使用高地址的空间，再使用低地址的空间
//		//  数组随着下标的增长，地址由低到高变化
//	}
//	return 0;
//}

//void my_strcpy(char* dest, char* src)
//{
//	assert(dest != NULL);//断言
//	assert(src != NULL);//断言
//
//
//	while (*src != '\0')
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//	*dest = *src;//处理\0
//}
//char*  my_strcpy(char* dest,const char* src)
//{
//	assert(dest && src);//断言
//	char* ret = dest;
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
//
//int main()
//{
//	//strcpy -string copy - 字符串拷贝
//	char arr1[20] = { 0 };
//	char arr2[] = "hello";
//
//	//strcpy(arr1, arr2);// "hello"中的'\0'也会被拷过去
//	//my_strcpy(arr1, arr2);
//
//	//printf("%s\n", arr1);
//
//
//	printf("%s\n", my_strcpy(arr1, arr2));
//
//	return 0;
//}

// const 修饰变量
// const 修饰指针

//int main()
//{
//	const int n = 10;
//	//n = 20;
//	
//	//const 修饰指针
//	//const 放在*的左边
//	//const 修饰的指针指向的内容，表示指针指向内容不能通过指针来改变
//	//但是指针变量本身是可以改变的 
//	// 
//	//const 放在*的右边
//	//const 修饰的是指针变量本身，指针变量的内容不能被修改
//	int m = 100;
//
//	int* const p = &n;
//	//p = &m;
//	*p = 20;
//
//
//	//*p = 20;
//
//	printf("%d", n);
//	return 0;
//}

//#include<stdio.h>
//#include<stdlib.h>
////调整数组使奇数全部都位于偶数前面
//void Depart_Arry(int arr[], int size)
//{
//	int left = 0;
//	int right = size - 1;
//	while (left < right)
//	{
//		//从前到后找偶数
//		while (left < right && arr[left] % 2 != 0)
//		{
//			left++;
//		}
//		//从后到前找奇数
//		while (left < right && arr[right] % 2 == 0)
//		{
//			right--;
//		}
//		//交换
//		if (left < right)
//		{
//			int tmp = arr[left];
//			arr[left] = arr[right];
//			arr[right] = tmp;
//		}
//	}
//}
//
////打印数组
//void Print(int arr[], int size)
//{
//	for (int i = 0; i < size; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//}
//
//int main()
//{
//
//	int arr[] = { 2, 3, 4, 6, 5, 8, 7, 10, 9, 1 };
//	int size = sizeof(arr) / sizeof(arr[0]);
//	Depart_Arry(arr, size);
//	Print(arr, size);
//	system("pause");
//	return 0;
//}


//int main()
//{
//	unsigned char a = 200;
//	unsigned char b = 100;
//	unsigned char c = 0;
//	c = a + b;
//	printf("%d %d", a + b, c);
//	return 0;
//}

int main()
{
	char a[1000] = { 0 };
	int i = 0;
	for (i = 0; i < 1000; i++)
	{
		a[i] = -1 - i;
	}
	printf("%d", strlen(a));
	return 0;
}