#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

// ********初识C语言末尾+分支和循环（1）********

// & --> 取地址操作福 
// * --> 解引用操作符/
//int main() {
//	////内存单元  1 byte --> 1 字节
//	////内存单元的编号  ---> 地址
//	int a = 0;  //int --> 4 byte
//	//// 拿出 a 的地址  使用 & ---取地址操作符   
//	////&a; // 取出的是所占4个字节空间的第一个字节的地址(地址小的那一个字节)
//
//	//&a;
//
//	//printf("%p", &a);
//	////scanf("%d", &a);
//	////printf("%d\n", a);
//	int* p = &a; // p 是用来存放地址的，所以我们把p成为指针变量
//	//  * ---->解引用操作符
//	//  *p  通过p中的值，找到p所指向的对象，*p就是a
//	*p = 20;
//	printf("%d\n", sizeof(*p));
//	printf("%d\n", a);
//	// * 表示 p 是一个指针变量
//	// int 表示p指向的对象的类型是int
//	printf("%p\n", p);
//	printf("%p", &a);
//
//	double d;
//	double * pd = &d;
//
//
//	return 0;
//}

// 指针变量的大小
//int main() {
//	printf("%d\n", sizeof(char*));
//	printf("%d\n", sizeof(short*));
//	printf("%d\n", sizeof(int*));
//	printf("%d\n", sizeof(long*));
//	printf("%d\n", sizeof(float*));
//	printf("%d\n", sizeof(double*));
//	//--->指针变量有多大？ 
//	//--->地址的存放需要多大空间？
//	//--->地址是如何产生的？地址是什么样的数据？
//	// 32 位 机器 ----> 32跟地址线/数据线
//
//	// 在32 位环境下，指针的大小是4个字节
//	// 在64 位环境下，指针的大小是8个字节
//
//	return 0;
//}


//**********结构体*************** 

//描述一个复杂对象的时候

// 描述一个学生
//struct Stu {
//	// 结构体成员
//	char name[20];
//	int age;
//	char sex[10];
//
//};
//
//int main() {
//
//	//创建结构体变量，并且初始化
//	struct Stu zhangsan = { "张三",20,"男" };
//	struct Stu lisi = { "李四",23,"保密" };
//
//	//  -> 这个操作符
//	 struct Stu *pl = &lisi;
//	 printf("%s %d %s \n", (*pl).name, (*pl).age, (*pl).sex);
//	 printf("%s %d %s\n", pl->name, pl->age, pl->sex);
//
//
//
//	// 打印结构体的数据
//	printf("%s %d %s\n", zhangsan.name, zhangsan.age, zhangsan.sex);
//	printf("%s %d %s\n", lisi.name, lisi.age, lisi.sex);
//
//
//	return 0;
//}

// . 
// 结构体成员访问的操作符
// 结构体变量 . 结构体成员


// ****分支和循环语句****

//  ***分支语句（选择语句）***
//int main() {
//	int age = 0;
//	scanf("%d",&age);
//
//	//if (age < 0) {
//	//	printf("年龄不合法\n");
//	//}
//	//else if (age < 18) {
//	//	printf("少年\n");
//	//}
//	//else if(age >= 18 && age < 40) {
//	//	printf("青年\n");
//
//	//}
//	//else if (age >= 40 && age < 60) {
//	//	printf("壮年\n");
//
//	//}
//	//else if (age >= 60 && age < 90) {
//	//	printf("老年\n");
//
//	//}
//	//else if (age > 90 ) {
//	//	printf("老寿星\n");
//
//	//
//
//		//int age = 0;
//		//scanf("%d", age);
//		//if（18 <= age < 40）{
//		//printf("")
//		//}
//
//	//if (age >= 18) {
//	//	printf("成年\n");
//	//}
//	//else {
//	//	printf("未成年");
//	//}
//
//	/*if (age >= 18) {
//		printf("成年\n");
//	}*/
//
//	return 0;
//
//}

//int main() {
//	int a = 0;
//	int b = 2;
//	if (a == 1) {
//		if (b == 2) {
//			printf("hehe\n");
//		}
//		else {
//			printf("haha\n");
//		}
//	}
//	return 0;
//}
// else 是和离他最近的if进行匹配的

//int main() {
//	int a = 10;
//	if (a = 5) {
//		printf("hehe\n"); // 依然会打印
//	}
//	if (5 = a) {// 就会报错，常量不可修改 
//		printf("hehe\n");
//	}
//	return 0;
//}
//// 常量（数字）放在左边
//
//int main() {
//	int a = 10;
//	if (a == 5) {
//		printf("hehe\n");
//	}
//
//	return 0;
//}

//int main() {
//	int a = 0;
//	scanf("%d", &a);
//	if (a % 2 == 1) {  // 判断
//		printf("奇数\n");
//	}
//	return 0;
//}

// 打印1 - 100 所有的奇数
//int main() {
//	int a = 0;
//	while (a <= 100) {
//		if (a % 2 == 1) {
//			printf("%d  ",a);
//		}
//		a++;
//	}
//	return 0;
//}

// *** switch 语句 ***
//int main() {
//	int  day = 0;
//	scanf("%d", &day);
//	switch (day)// 整形表达式
//	{ 
//	case 1:   // case
//		printf("星期一\n"); 
//		break;//break终止 跳出
//	case 2:
//		printf("星期二\n"); 
//		break;
//	case 3:
//		printf("星期三\n"); 
//		break;
//	case 4:
//		printf("星期四\n"); 
//		break;
//	case 5:
//		printf("星期五\n");
//		break;
//	case 6:
//		printf("星期六\n"); 
//		break;
//	case 7:
//		printf("星期天\n"); 
//		break;
//	default:
//		printf("输入错误，请输入1-7的数字\n");
//		break;
//	}	
//	//default字句
//
//	return 0;
//} 

// ***循环语句***
//  while 循环
//int main() {
//	while (1) {
//		printf("hehe\n");
//	}
//	return 0;
//}

// 打印1-10
int main() {
	int  i = 1;
	while (i <= 10) {
		if (5 == i)
			/*break;*/
		continue;// continue 会跳过后面的语句

		printf("%d ", i);
		i++;
	}
	return 0;
}
