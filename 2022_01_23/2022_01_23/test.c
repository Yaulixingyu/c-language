#define  _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

struct A
{
	//4 byte
	int _a : 2; //2bit --剩下30
	int _b : 5; //25
	int _c : 10;//15
	//4 byte - 32bit
	int _d : 30;//
};

//int main()
//{
//	//位段
//	printf("%d", sizeof(struct A));
//
//	
//	return 0;
//}

//枚举

enum Day
{
	//枚举的可能取值
	Mon,
	Tues,
	Wed,
	Thir,
	Fri,
	Sta,
	Sun
};

typedef enum Sex
{
	MALE = 4,//0
	FEMALE = 8,//1
	SECRET
};

//int main()
//{
//	//enum Day d = Sun;
//	printf("%d\n", MALE);
//	printf("%d\n", FEMALE);
//	printf("%d\n", SECRET);
//	return 0;
//}

//test.c --> 编译 --> 链接 --> test.exe

//union Un
//{
//	char c;//1
//	int i;//4
//};
//int main()
//{
//	union Un u;
//	printf("%d\n", sizeof(u));
//	printf("%p\n", &u);
//	printf("%p\n", sizeof(u));
//	printf("%p\n", sizeof(u));
//	return 0;
//}

union Un1
{
	char c[5];//1,8 -1 
	int i; // 4 4,8 -4


};

union Un2
{
	short c1[7];  // 2 14 
	int j;      // 4   

};
//int main()
//{
//	printf("%d\n", sizeof(union Un1));
//	printf("%d\n", sizeof(union Un2));
//	return 0;
//}


//
// 动态内存分配
//
// 在堆区上
//
// malloc
// calloc
// realloc
// free   -- 内存释放
//

// malloc   --  内存申请


#include <errno.h>
#include <string.h>
#include <stdlib.h>
//int main()
//{
//	//开辟10个整形的空间
//	//int arr[10];
//	int* p =(int*)malloc(40);
//	if (NULL == p)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	//使用
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = 0;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", p[i]);
//	}	
//	//释放
//	free(p);
//	//当释放后，p就是野指针了
//	p = NULL;
//
//	return 0;
//}



//calloc
//可以初始化 
//int main()
//{
//	int* p = (int*)calloc(10, sizeof(int));
//	if (NULL == p)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	//使用
//	int i = 0;
//	//for (i = 0; i < 10; i++)
//	//{
//	//	*(p + i) = 0;
//	//}
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", p[i]);
//	}	
//	//释放
//	free(p);
//	//当释放后，p就是野指针了
//	p = NULL;
//
//
//	return 0;
//}

//realloc
//1:可以开辟空间
//2:也可以调整空间
//
//int main()
//{
//	int* p = (int*)calloc(10, sizeof(int));
//	if (NULL == p)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	//使用
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", p[i]);
//	}	
//
//	//需要增加容量 - 80
//	int*ptr = (int *) realloc(p, 80);
//	if (NULL != ptr)
//	{
//		p = ptr;
//	}
//
//	//释放
//	free(p);
//	//当释放后，p就是野指针了
//	p = NULL;
//
//
//	return 0;
//}
