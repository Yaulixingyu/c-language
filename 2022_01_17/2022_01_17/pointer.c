#include <stdio.h>
#include <stdlib.h>



//指针数组 - 存放指针的数组

//int main()
//{
//	//指针数组
//	int* arr[5];//arr数组内放了5个整型指针
//	char* arr2[6];//arr2数组内放了6个字符型指针
//	char** arr3[3];
//
//
//	//数组指针 -- 指向数组的指针
//	int arr[10];
//	int* p = arr;
//	int (*p2)[10] = &arr;//取出的是数组的地址，既然是数组的地址，就应该放到数组中指针变量中
//	//p2就是一个数组指针
//
//	//p存放的是第一个元素的地址
//	//p+1跳过一个int 
//	//*p 访问一个int数据 4个字节
//
//	//p2存放的是arr数组的地址
//	//p2+1跳过整个数组
//	//*p2访问到的是arr数组，*p2等价于arr了，那么*p2就是数组名了
//	//数组名有相当于首元素的地址，所以*p2本质上是arr数组第一个元素的地址
//
//	//  p  == *p2 
//
//	return 0;
//}


// 函数指针
// 函数其实也是有地址的，函数名(或&函数名)就是函数的地址
//int my_test(const char* s)
//{
//	printf("测试通过函数指针来调用函数");
//	return 0;
//}
//int main()
//{
//	int (*pf)(const char* s) = my_test;
//	(*pf)("hehe");
//	return 0;
//}



//函数指针数组 --存放函数指针的数组 -- 指针数组

//int Add(int x, int y)
//{
//	return x + y;
//}
//int main()
//{
//	int (*pa)(int，int) = Add;//函数指针
//	int (*pfa[4])(int, int);//函数指针数组
//	return 0;
//}


//指向函数指针数组的指针

//int Add(int x, int y)
//{
//	return x + y;
//}
//int main()
//{
//	int (*pa)(int，int) = Add;//函数指针
//	int (*pfA[4])(int, int);//函数指针数组
//	int (*(*ppfA)[4])(int,int) = &pfA;
//	//ppfA是一个指针，该指针指向了一个存放函数指针的数组
//	return 0;
//}

//回调函数
//回调函数就是一个通过函数指针调用的函数
//如果把函数的指针作为参数传递给另一个函数
//当这个指针被用来调用其所指向的函数时，我们就说这是回调函数
// 
//

//void bubble_sort(int arr[], int sz)
//{
//	//趟数
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		//每一趟冒泡排序的过程
//		//确定一趟排序中比较的对数学
//		int j = 0;
//		for (j = 0; j <sz-1-i ; j++)
//		{
//			if (arr[j] > arr[j + 1])
//			{
//				int  tmp = arr[j];
//				arr[j] = arr[j + 1];
//				arr[j + 1] = tmp;
//			}
//		}
//	}
//	
//}

void print_arr(int arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}
int cmp_int(const void* e1, const void* e2)
{
	return *(int*)e1 -*(int*)e2;
	
}
//测试qsort 排序整形数组


struct Stu
{
	char name[20];
	int age;
	float score;
};
//测试qsort排序结构体数据
int cmp_stu_by_score(const void* e1,const void* e2)
{ 
	if ((((struct Stu*)e1)->score) > (((struct Stu*)e2)->score))
		return 1;
	else if ((((struct Stu*)e1)->score) > (((struct Stu*)e2)->score))
		return -1;
	else
	{
		return 0;
	}
}

void print_stu(struct Stu arr[], int sz)
{
	int i = 0;
	for (i = 0; i < sz; i++)
	{
		printf("%s %d %.2f\n", arr[i].name, arr[i].age, arr[i].score);

	}
	printf("\n");

}
void Swap(char* buf1, char* buf2, int width)
{
	int i = 0;
	for (i = 0; i < width; i++)
	{
		char tmp = *buf1;
		*buf1 = *buf2;
		*buf2 = tmp;
		buf1++;
		buf2++;
	}
}


void bubble_sort(void* base, int sz, int width, int(*cmp)(const void* e1, const void* e2))

{
	//趟数
	int i = 0;
	for (i = 0; i < sz - 1; i++)
	{
		//每一趟冒泡排序的过程
		//确定一趟排序中比较的对数学
		int j = 0;
		for (j = 0; j < sz - 1 - i; j++)
		{
			/*if (arr[j] > arr[j + 1])*/
			if (cmp((char*)base + j * width, (char*)base + (j + 1) * width) > 0)
			{
				//两个元素的交换
				Swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
			}
		}
	}

}
void test4()
{
	struct Stu arr[] = { {"Jennie",20,87.5f},{"Lisa",20,99.0f},{"Jisoo",22,100.0f},{"Rose",21,93.5f}};

	//按照成绩排序
	int sz = sizeof(arr) / sizeof(arr[0]);
	bubble_sort(arr, sz, sizeof(arr[0]), cmp_stu_by_score);
	print_stu(arr, sz);
}



void test3()
{
	int arr[] = { 1,4,2,6,5,3,7,9,0,8 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	bubble_sort(arr, sz,sizeof(arr[0]),cmp_int);
	print_arr(arr, sz);
}

void test1()
{
	int arr[] = { 1,4,2,6,5,3,7,9,0,8 };
	int sz = sizeof(arr) / sizeof(arr[0]);
	//bubble_sort(arr, sz);
	qsort(arr, sz, sizeof(arr[0]), cmp_int);

	print_arr(arr, sz);
}

int main()
{
	test4();
	return 0;
}

//qsort -- 库函数



//int main()
//{
//	int a = 10;
//	//int* p = &a;
//	//char* p = &a;
//	void* p = &a;
//	//
//	//void* 是一种无具体类型的指针
//	//void* 的指针变量可以存放任意类型的地址
//	//
//	//void* 不能直接进行解引用操作
//	//void* 也不能直接进行 + - 整数
//	//
//	return 0;
//}

//void Swap(char* buf1, char* buf2, int width)
//{
//	int i = 0;
//	for (i = 0; i < width; i++)
//	{
//		char tmp = *buf1;
//		*buf1 = *buf2;
//		*buf2 = tmp;
//		buf1++;
//		buf2++;
//	}
//}
//
//void bubble_sort(void* base, int sz, int width, int(*cmp)(const void*e1, const void*e2))
//{
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			//if (arr[j] > arr[j + 1])
//			if(cmp((char*)base + j * width, (char*)base + (j + 1) * width)>0)
//			{
//				//两个元素的交换
//				Swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
//			}
//		}
//	}
//}

////模拟实现qsort
//void print_arr(int arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//}
//
//void bubble_sort(void* base, int sz, int width, int(*cmp)(const void* e1, const void* e2))
//
//{
//	//趟数
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		//每一趟冒泡排序的过程
//		//确定一趟排序中比较的对数学
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			/*if (arr[j] > arr[j + 1])*/
//			if (cmp((char*)base + j * width, (char*)base + (j + 1) * width) > 0)
//			{
//				//两个元素的交换
//				Swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
//			}
//		}
//	}
//
//}
//
//
//void test3()
//{
//	int arr[] = { 1,4,2,6,5,3,7,9,0,8 };
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr, sz, sizeof(arr[0]), cmp_int);
//	print_arr(arr, sz);
//}
//
//int main()
//{
//	test3();
//	return 0;
//}



//练习使用库函数，qsort排序各种类型的数据
//struct Stu
//{
//	char name[20];
//	int age;
//	float score;
//};
////测试qsort排序结构体数据
//void print_stu(struct Stu arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%s %d %.2f\n", arr[i].name, arr[i].age, arr[i].score);
//
//	}
//	printf("\n");
//
//}
//
//
//void bubble_sort(void* base, int sz, int width, int(*cmp)(const void* e1, const void* e2))
//
//{
//	//趟数
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		//每一趟冒泡排序的过程
//		//确定一趟排序中比较的对数学
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			/*if (arr[j] > arr[j + 1])*/
//			if (cmp((char*)base + j * width, (char*)base + (j + 1) * width) > 0)
//			{
//				//两个元素的交换
//				Swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
//			}
//		}
//	}
//
//}
//void test4()
//{
//	struct Stu arr[] = { {"Jennie",20,87.5f},{"Lisa",20,99.0f},{"Jisoo",22,100.0f},{"Rose",21,93.5f} };
//
//	//按照成绩排序
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr, sz, sizeof(arr[0]), cmp_stu_by_score);
//	print_stu(arr, sz);
//}
//
//int main()
//{
//	test4();
//	return 0;
//}