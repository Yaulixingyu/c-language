#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
//坐标实现构成三角形
int main() {
    double a = 0;
    a = 25 * 3.14;
    printf("%lf",a);

    return 0;
}
int main9() {
    double x1, x2, x3, y1, y2, y3, a, b, c, s,L, area;
    printf("请分别输入3个点的坐标的x值，y值: \n");
    scanf("%lf %lf %lf %lf %lf %lf", &x1, &y1, &x2, &y2, &x3, &y3);
    a = sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));//BC两点之间的距离a
    b = sqrt((x3 - x2) * (x3 - x2) + (y3 - y2) * (y3 - y2));//AC两点之间的距离b
    c = sqrt((x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1));//AB两点之间的距离c
    s = (a + b + c) / 2.0;
    if (a + b <= c || a + c <= b || b + c <= a) {      //判断是否可以构成三角形
        printf("Impossible");  // 不能构成三角形
    }
    else {                    //能构成三角形
        printf("能构成三角形!\n");
        L = a + b + c;     //实现周长L
        area = sqrt(s * (s - a) * (s - b) * (s - c));//实现面积area
        printf("其中三角形的周长是: %.2lf\n", L);
        printf("其中三角形的面积是: %.2lf\n", area);
    }
    return 0;
}

//统计学生成绩：ABCDE等级
int main8() {
    int n, x, i, a = 0, b = 0, c = 0, d = 0, e = 0;
	printf("输入一个学生人数n：\n");
	scanf("%d", &n);
	for (i = 0; i < n; i++) {
		printf("请分别输入学生的成绩x：\n");
		scanf("%d", &x);
		if (x >= 90) {
			a++;
		}else if (x >= 80 && x < 90) {
			b++;
		}else if(x >= 70 && x < 80) {
			c++;
		}
        else if (x >= 60 && x < 70) {
            d++;
        }
        else if (x < 60) {
            e++;
        }
	}
	printf("获得A人数：%d\n", a);
	printf("获得B人数：%d\n", b);
    printf("获得C人数：%d\n", c);
    printf("获得D人数：%d\n", d);
    printf("获得E人数：%d\n", e);
	return 0;
}



//比较大小，从小到大输出3个整数
int main7() {
    int a, b, c, t;
    printf("请输入3个整数：\n");
    scanf("%d %d %d", &a, &b, &c);
    if (a > b) {
        t = a;
        a = b;
        b = t;
    }
    if (a > c) {
        t = a;
        a = c;
        c = t;
    }
    if (b > c) {
        t = b;
        b = c;
        c = t;
    }
    printf("%d %d %d\n", a, b, c);
    return 0;
}

int main6() {
    printf("请输入4个整数\n");
    int a, b, c, d;
    float average;
    scanf("%d %d %d %d",&a,&b,&c,&d);
    average =(a + b + c + d)/ 4.0;
    printf("average = %.1f", average);
    return 0;
}

int main5() {
    int n = 0 ;
    float sum = 0.0;
    while (1) {
        printf("请输入n值：\n");
        scanf("%d\n", &n);
        int m = 1;
        while (m <= n) {
            sum = sum + sqrt(m);
            m++;
        }
        printf("%.2f", sum);
    }
        return 0;
    
}



int main4() {
    int m, n;
    float sum = 0.0;
    printf("请输入两个整数：\n");
    scanf("%d %d", &m, &n);
    for (m; m <= n; m++) {
        sum = sum + m*m + 1.0 / m;
    }
    printf("sum =%f", sum);
    return 0;
}



//**********三角形的面积*************int main() {
int main3(){
    printf("分别输入三角形的三边长: ");
    int a = 0;
    int b = 0;
    int c = 0;
    scanf("%d %d %d",&a,&b,&c);
        // 判断是否可以构成三角形
        if (a+ b < c || a + c < b || b + c < a) {
           printf("你输入数据的不能构成三角形!!");
        }
        else {
            double area = 0, p = 0;
            //三角形的面积公式
            p = (a + b + c) / 2;
            area = sqrt(p * (p - a) * (p - b) * (p - c));
            printf("三角形的面积是:%lf", area);
        }
        return 0;
    }

int main2() {

	printf("heh ");

	return 0;
}

int main1() {
	int a ,b,c,d,sum;
	double average;
	printf("分别输入四个数: \n");
	scanf("%d %d %d %d",&a,&b,&c,&d);
	sum = a + b + c + d;
	average = sum / 4.0;
	printf("sum = %d\n", sum);
	printf("average= %.1lf",average);
	return 0 ;
}