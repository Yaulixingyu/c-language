#define  _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

/*描述
从键盘输入a, b, c的值，编程计算并输出一元二次方程ax2 + bx + c = 0的根，当a = 0时，输出“Not quadratic equation”，当a ≠ 0时，根据△ = b2 - 4*a*c的三种情况计算并输出方程的根。
输入描述：
多组输入，一行，包含三个浮点数a, b, c，以一个空格分隔，表示一元二次方程ax2 + bx + c = 0的系数。
输出描述：
针对每组输入，输出一行，输出一元二次方程ax2 + bx +c = 0的根的情况。

如果a = 0，输出“Not quadratic equation”；

如果a ≠  0，分三种情况：

△ = 0，则两个实根相等，输出形式为：x1=x2=...。

△  > 0，则两个实根不等，输出形式为：x1=...;x2=...，其中x1  <=  x2。

△  < 0，则有两个虚根，则输出：x1=实部-虚部i;x2=实部+虚部i，即x1的虚部系数小于等于x2的虚部系数，实部为0时不可省略。实部= -b / (2*a),虚部= sqrt(-△ ) / (2*a)

所有实数部分要求精确到小数点后2位，数字、符号之间没有空格。

示例1
输入：
2.0 7.0 1.0
输出：
x1=-3.35;x2=-0.15

示例2
输入：
0.0 3.0 3.0
输出：
Not quadratic equation

示例3
输入：
1 2 1
输出：
x1=x2=-1.00

示例4
输入：
2 2 5
输出：
x1=-0.50-1.50i;x2=-0.50+1.50i

示例5
输入：
1 0 1
输出：
x1=0.00-1.00i;x2=0.00+1.00i
*/
#include <math.h>
//int main()
//{
//	//假设a,b,c分别是一元二次方程组的3个参数
//	//x1,x2为两个根
//	double a = 0, b = 0, c = 0, x1 = 0, x2 = 0;
//	double y = 0;//判别式 
//	//多组输入
//	while (~(scanf("%lf %lf %lf", &a, &b, &c)))
//	{
//		double y = b * b - 4 * a * c;
//		if (0 == a)
//		{
//			printf("Not quadratic equation");
//		}
//		else
//		{
//			if (0 == y)
//			{
//				x1 = (-b) /(2*a);
//				if (x1 == 0)x1 = 0;
//				printf("x1=x2=%.2lf", x1);
//			}
//			else if (y > 0)
//			{
//				x1 = (((-b) - sqrt(y)) / (2 * a));
//				x2 = (((-b) + sqrt(y)) / (2 * a));
//				if (x1 > x2)
//				{
//					double tmp = 0;
//					tmp = x1;
//					x1 = x2;
//					x2 = tmp;
//				}
//				printf("x1=%.2lf;x2=%.2lf", x1, x2);
//			}
//			else if (y < 0)
//			{
//				//实部
//				double Real = -b / (2 * a);
//				//虚部  取了绝对值
//				double Ima = fabs(sqrt(-y) / (2 * a));
//				printf("x1=%.2lf-%.2lfi;x2=%.2lf+%.2lfi", Real, Ima, Real, Ima);
//			}
//		}
//		printf("\n");
//	}
//	return 0;
//}

//整数在内存中的存储练习

//练习1
//int main()
//{
//	char a = -1;
//	signed char b = -1;
//	unsigned char c = -1;
//	printf("a=%d,b=%d,c=%d", a, b, c);
//	return 0;
//}

//练习2
//int main()
//{
//	char a = -128;
//	printf("%u\n", a);
//	return 0;
//}

//练习3
//int main()
//{
//	char a = 128;
//	printf("%u\n", a);
//	return 0;
//}

//练习4
//int main()
//{
//	int i = -20;
//	unsigned int j = 10;
//	printf("%d\n", i + j);
//	return 0;
//}

////练习5
//int main()
//{
//	unsigned int i;
//	for (i = 9; i >= 0; i--)
//	{
//		printf("%u\n", i);
//	}
//	return 0;
//}

//练习6
#include <string.h>
//int main()
//{
//	char a[1000];
//	int i;
//	for (i = 0; i < 1000; i++)
//	{
//		a[i] = -1 - i;
//	}
//	printf("%d", strlen(a));
//	return 0;
//}

//练习7
//unsigned char i = 0;
//int main()
//{
//	for (i = 0; i <= 255; i++)
//	{
//		printf("Hello\n");
//	}
//	return 0;
//}