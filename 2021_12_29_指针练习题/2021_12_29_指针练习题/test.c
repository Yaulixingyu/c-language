#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//指针练习题 1
//使用指针打印数组内容
//
//内容:写一个函数打印arr数组的内容，不使用数组下标，使用指针。
//
//arr是一个整形一维数组。
//int main()
//{
//	//创建arr数组
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	//打印arr数组
//	int sz = sizeof(arr) / sizeof(arr[0]); 
//	int* p = arr;// p取到arr首元素地址
//	int i = 0;
//	        
//	       //正序打印
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//
//	       //逆序打印
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + sz -1- i));
//	}
//	return 0;
//}


//指针练习题2
//题目：字符串逆序
//内容与要求：使用指针，写一个函数，可以逆序一个字符串的内容。

//举例：
//输入: I Love You.
//输出: .uoY evoL I
// 
//输入: abc
//输出: cba
#include <string.h>

int My_strlen(char* s)
{
	//求长度 指针减指针的方法求长度
	char* start = s;
	while (*s != '\0')
	{
		s++;
	}
	return  s - start;
}
int main()
{
	int i = 0;
	//创建字符数组
	char arr[1000];
	//使用gets函数输入字符串
	gets(arr);
	//用指针求字符串长度
	int len = My_strlen(arr);
	//逆序打印字符串
	char* p = arr+len-1;//最后元素的地址
	for (i = 0; i < len; i++)
	{
		putchar(*(p - i));
	}
	return 0;
}

