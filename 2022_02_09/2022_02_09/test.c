#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//int main()
//{
//	printf("666");
//	return 0;
//}

// 关键字7

//switch 语句的基本理解
//case
int main()
{
	int day = 0;
	while (1)
	{
		scanf("%d", &day);

		//任何具有判定能力的语法结构，都必须具具备：判定+分支

		switch (day)//整型||表达式
		{
		case 1://case本身是用来判定的
			printf("星期1\n");
			break;//用来进行分支功能
		case 2:
			printf("星期2\n");
			break;
		case 3:
			printf("星期3\n");
			break;
		case 4:
			printf("星期4\n");
			break;
		case 5:
			printf("星期5\n");
			break;
		case 6:
			printf("星期6\n");
			break;
		case 7:
			printf("星期7\n");
			break;
			//必须带上default
		default:
			printf("输入错误\n"); 
		//即使不需要default,也保留
		/*default:
			break;*/
		}
	}
	return 0;
}