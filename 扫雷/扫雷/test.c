#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include "game.h"
void menu()// 游戏菜单
{
	printf("********************\n");
	printf("****  1. play  *****\n");
	printf("****  0. exit  *****\n");
	printf("********************\n");
}
void game()
{
	// 创建数组
	// 创建雷的数组(mine)   显示的数组(show)    两个数组一样规模 一样类型
	char mine[ROWS][COLS] = { 0 }; // 存放布置好的雷的信息
	char show[ROWS][COLS] = { 0 }; // 存放排查出的雷的信息

	// 初始化mine数组为全'0'
	InitBoard(mine,ROWS,COLS,'0');//初始化--->棋盘函数
	// 初始化show数组为全'*'
	InitBoard(show,ROWS,COLS,'*');//初始化--->棋盘函数

	//打印棋盘
	//DisplayBoard(mine, ROW, COL);
	//printf("\n");
	//DisplayBoard(show,ROW,COL);

	//布置雷
	SetMine(mine,ROW,COL);
	DisplayBoard(mine, ROW, COL);
	//DisplayBoard(show,ROW,COL);

	//排雷
	FindMine(mine,show,ROW,COL);
}
void test()
{
	int input = 0;
	srand((unsigned int )time(NULL));
	do
	{
		menu();// 游戏菜单
		printf("请选择>");
		scanf("%d", &input);
		switch (input)
		{
		case 1:
			// 扫雷游戏
			game();
			break;
		case 0:
			printf("退出游戏\n");
			break;
		default:
			printf("选择错误\n");
			break;
		}
	} while (input);

}


int main()
{
	test();

	return 0;

}