#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
double funpi(double e);//声明函数 
int main()
{
    double e;//给出精度e 
    scanf("%lf", &e);//输入e的值 
    printf("pi = %.6f", 4 * funpi(e));//按照题目要求打印 打印的值为4倍的funpi(e)函数 
    return 0;
}

double funpi(double E)//定义funpi()函数 
{
    double pi = 0, deno = 1, sign = 1;//pi为派的值 deno是分母(denominator)的缩写 sign是当前项的正负值 
   while(1)
    {
        pi += sign / deno;//每项相加 
        sign *= (-1.0);//隔项变号 
        deno += 2;//分母隔项自加2 
        if (1 / (deno - 2) < E)//对于精度绝对值小于e的表述 
        {
            break;//如果精度小于e 则跳出循环 
        }
    }
    return pi;//返回pi值 
}