#include <stdio.h>

// 第一个代码
//int main() { //int -> 整形
//	//打印 -> 打印函数:printf
//	//printf 是一个库函数：库函数的使用得包含一个头文件 stdio.h
//	// #include <stdio.h> ——>头文件的包含
//
//    printf("Hello World!\n ");
//
//	return 0;
//}
//void main() {
//	printf("hehe");
//}
//  main函数是程序的入口
// 程序是从main函数的第一行开始执行的
// main函数有且只有一个

//数据类型
// char  字符数据类型
// short 短整形
// int   整形
//long   长整型
//long long   更长的整形
//float   单精度浮点数
////double  双精度浮点数  (精度更高)
//int main() {
//	//描述一个人的年龄
//	int age;  // 表示age是一个整形变量
//	age = 20; // 使用int整形类型创建了一个变量叫age 并且赋值 20 
//	age = age + 1;
//
//	// 描述 pai = 3.1415.....
//	float pai = 3.14f;
//	double pai = 3.1415926;  //精度更高
//	return 0;
//}
//每一种类型的大小是多少
//int main() {

	//int age = 20;//age变量的创建其实是要在内存中开辟空间的
	// int 决定 age  的大小
	// 关键字 sizeof 
	//c语言中的sizeof是用来计算变量的或者使用类型创建的变量的大小，单位是字节
	//printf("hello world!\n");
	//printf("%d", 100);
	//%d 表示 打印一个10进制的整数
	//printf("%d\n", age);
	//计算机中是2进制的： 1/0
             //10进制的： 0-9
			 // 8进制的： 0-7
// 存放一个二进制位的时候： 存 1  需要 1个bit位 -> 8 bit = 1 byte(字节)
	// 8 bit = 1 byte 
	// 1 kb = 1024 byte
	// 1 mb = 1024 kb
	// 1 gb = 1024 mb
	// ......tb pb
//	printf("%d\n", sizeof(char));   // 1  单位是 字节 - byte
//	printf("%d\n", sizeof(short));  // 2  
//	printf("%d\n", sizeof(int));    // 4  2^32 状态
//	printf("%d\n", sizeof(long));   // 4
//	printf("%d\n", sizeof(long long)); // 8
//	printf("%d\n", sizeof(float));  // 4
// 	printf("%d\n", sizeof(double)); // 8
//	//合理的选择 对内存的利用率高
//
//
//	return 0;
//}

//int main(){
////	char ch = 'w';//字符使用单引号引起来
////	//'a' 'b'
//	
//
//
//// 变量  常量
////C语言中为了描述变化的值 -> 使用变量
////               不变的值 -> 使用常量
//// 类型 变量名
//	//int age = 0; 
//	////char c;
//	////变量在创建时，不初始化是一种不好的代码风格
//	////当一个 (局部) 变量不初始化的时候，他的值是随机的
//	//age = age + 1;
//	//printf("%d", age);
//	//char ch = 'w'; // 初始化
//	return 0;
//}
//局部变量和全局变量
// 一个大括号就是一个代码块
// 在代码块内部定义的变量就是局部变量
// 在代码块外部定义的变量就是全局变量

//int  a = 100;// 全局变量
//int main() {
//
//	int a = 10; //局部变量
//	printf("%d", a);// 执行结果：10  
//	//因为 当名字相同时候  局部变量优先
// 尽量做到不要名字冲突
//	return 0;
//}
