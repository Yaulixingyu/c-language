﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//初识结构体
//结构是一些值的集合，这些值成为成员变量，结构的每个成员可以是不同类型的变量
//
//生活中的对象都是复杂的
//结构体就是用来描述复杂对象的~

//结构的声明：
//声明一个学生的结构体类型
//
//struct S
//{
//	int a;
//	char c;
//	double d;
//};
//
//struct Stu
//{
//	struct S ss;
//	char name[20];
//	int age;
//	float score;
//}s1,s2;//s1,s2是2个结构体变量，是全局的
//int main()
//{
//	int a = 0;
//	int b = 0;
//
//	struct Stu s1 = {100,'w',3.14} ;
//	printf("%d %c %lf\n", s1.ss.a, s1.ss.c, s1.ss.d);
//	
//	//struct Stu s = { "zhangsan",20,95.5f };//定义一个结构体变量 ，局部的
//	//printf("%s %d %f\n", s.name, s.age, s.score);
//	return 0;
//}


//struct Stu
//{
//	char name[20];
//	int age;
//};
//void print(struct Stu* ps)
//{
//	printf("name = %s   age = %d\n", (*ps).name, (*ps).age);
//	//使用结构体指针访问指向对象的成员
//	printf("name = %s   age = %d\n", ps->name, ps->age);
//}
//int main()
//{
//	struct Stu s = { "zhangsan", 20 };
//	print(&s);//结构体地址传参
//	return 0;
//}


//struct S
//{
//	int arr[1000];
//	float f;
//	char ch[100];
//
//};
//void print(struct S tmp)
//{
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", tmp.arr[i]);
//	}
//	printf("\n");
//	printf("%f\n", tmp.f);
//	printf("%s\n", tmp.ch);
//}
//
//void print2(struct S* ps)
//{
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", ps->arr[i]);
//	}
//	printf("\n");
//	printf("%f\n", ps->f);
//	printf("%s\n", ps->ch);
//}
//int main()
//{
//	//初始化结构体
//	struct S s = { {1,2,3,4,5,6,7,8,9,10},5.5f,"hello" };
//	//调用函数打印结构体s
//	print(s);//自定义函数printf  结构体s传给函数
//
//	print2(&s);
//
//	return 0;
//}

int Add(int x, int y)
{
	int z = 0;
	z = x + y;
	return z;
}
int main()
{
	int a = 10;
	int b = 20;
	int c = Add(a, b);
	printf("%d\n", c);
	return 0;
}


