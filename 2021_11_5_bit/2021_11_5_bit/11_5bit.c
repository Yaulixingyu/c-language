#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//int main() {
//	int i = 0;
//	while (i<10) {
//		i++;
//	}
//// 判断比循环多一次
//	return 0;
//}

int main() {
	int arr[10] = { 0 };
	int max = arr[0];
	int i = 0;
	for (i = 0; i < 10; i++) {
		scanf("%d", &arr[i]);
	}
	for (i = 0; i < 10; i++) {
		if (arr[i] > max) {
			max = arr[i];
		}
	}
	printf("%d\n", max);
	return 0;
}