#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//int main() {
//	int i = 0;
//	while (i<10) {
//		i++;
//	}
//// 判断比循环多一次
//	return 0;
//}

//int main() {
//	int arr[10] = { 0 };
//	int max = arr[0];
//	int i = 0;
//	for (i = 0; i < 10; i++) {
//		scanf("%d", &arr[i]);
//	}
//	for (i = 0; i < 10; i++) {
//		if (arr[i] > max) {
//			max = arr[i];
//		}
//	}
//	printf("%d\n", max);
//	return 0;
//}


// 全局变量

//int a = 0;
//int b = 0;
//
//void test() {
//	int x = 10;
//	int y = 20;
//	a = x;
//	b = y;
//}
//int main(){
//
//	test();
//	printf("%d %d", a, b);
//	return 0;
//}

//  栈区  ：   局部变量  形式参数（）
//  堆区  :    动态内存分配
// 静态区 :    static  全局变量

//  *******数组********

// ***一维数组的创建和初始化***

// 数组是一组相同类型的集合

//int main() {
	////int a = 10;
	////int b = 20;
	////int c = 30;
//	//  假设要创建100个
//	//int n = 10;
//	//int arr2[n];// [] 内应该为常量
//	/// <summary>
//	/// c99标准之前不支持使用变量的，只能是常量！
//	/// c99中增加了边长数组的概念，允许数组的大小是变量
//	/// 而且要求编译器支持C99标准
//	/// VS 对C99的支持不够好
//	/// </summary>
//	/// <returns></returns>
//
//	//int arr[5];
//	//char ch[10];
// 
// 
////创建的同时给一些初始值叫初始化
//	//int arr[10] = { 1,2,3,4,5,6,7,8,9.10 };
////不完全初始化 剩余的元素默认初始化为0
//int main() {
////	int arr[10] = { 1,2,3 };
////	int arr2[10] = { 0 };
////	return 0;
////}
//	char arr1[] = { 'a','b','c' }; // 'a','b','c'
////	//arr1有3个元素，数组的大小是3个字节
//         printf("%d\n",sizeof(arr1));
//         printf("%d\n", strlen(arr1));//随机值
////
//	char arr2[] = { "abc" };  // 'a','b','c','\0'
////	//arr2有4个元素，数组的大小是4个字节
//	printf("%d\n",sizeof(arr2));
//	printf("%d\n", strlen(arr2));// 3 
////
////
////	//strlen 和 sizeof
////
////	//strlen： 是一个库函数，计算的是字符串的长度，只能针对字符串   关注的是字符串是否有 '\0'
////    //         计算的是'\0'之前的字符的个数
////
////	//sizeof: sizeof是一个操作符（运算符）
////	//        sizerof是用来计算变量所占内存空间的大小的，任何类型都是可以使用
////	//        只关注空间的大小，不在乎内存中是否存在'\0'
////
////	 
////
//	return 0;
//}

// 数组的下标
//int main() {
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	//              0 1 2 3 4 5 6 7 8  9  
//	// 下标是用来访问元素的
//	arr[5];
//	//printf("%d\n", arr[5]);//[] 下标访问操作符
//	int i = 0;
//	//  计算数组的元素个数
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	            //    40 / 4;   计算数组的元素个数
//	for (i = 0; i < sz; i++) {
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int arr[10] = { 0 };//数组的不完全初始化
//	   //计算数组的元素个数
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	//对数组内容赋值,数组是使用下标来访问的，下标从0开始。所以：
//	int i = 0;//做下标
//	for (i = 0; i < 10; i++)//这里写10，好不好？
//	{
//		arr[i] = i;
//	}
//	//输出数组的内容
//	for (i = 0; i < 10; ++i)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}



// 一维数组在内存中的储存

//int main() {
//
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int *p = &arr[0];
//
//
//	// 打印数组的每个元素的地址
//	int i = 0;
//	for (i = 0; i < 10; i++) {
//		printf("&arr[%d] = %p\n", i, &arr[i]);
//		//printf("%p\n", p + i);// p+i 是数组中arr[i]的地址
//		//printf("%p-------------- %p\n", p+i, &arr[i]);
//
//	/*	printf("%d ", *(p + i));*/
//		
//
//		 //int 是4个字节 
//		 //一维数组在内存中是连续存放的
//		 //数组随着下标的增长，地址是由低到高变化的
//	}
//	
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int arr[10] = { 0 };
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//
//	for (i = 0; i < sz; ++i)
//	{
//		printf("&arr[%d] = %p\n", i, &arr[i]);
//	}
//	return 0;
//}

//********二维数组的创建和初始化**************

//int main() {
////
//	//int arr[3][4];// 表示3行4列  12个int 元素
//	//行和列的编号也是从0开始
//	//int arr[3][4] = { 1,2,3,4,5,6,7,8,9,10,11,12 };//先放满一行再到下一行
//    //int arr[3][4] = { 1,2,3,4,5,6,7};//先放满一行再到下一行，不完全初始化 7后面都是0000
//	int arr[3][4] = { {1,2,3},{4,5,6},{7,8,9} };
//	//printf("%d\n",arr[2][3]);// 第二行第三列的元素
//	int i = 0;
//	for (i = 0; i < 3; i++) {
//		int j = 0;
//		for (j = 0; j < 4; j++) {
//			printf("%-2d ", arr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}
int main() {
	int arr[3][4] = { 1,2,3,4,5,6,7,8,9,10,11,12 };
	int i = 0;
	int j = 0;
	for (i = 0; i < 3; i++) {
		for (j = 0; j < 4; j++) {
			printf("&arr[%d][%d] = %p\n",i,j,&arr[i][j]);
		 
			//二维数组的存放也是连续存放的

		}
	}
	return 0;
}

//#include <stdio.h>
//int main()
//{
//	int arr[3][4] = { 0 };
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 4; j++)
//		{
//			arr[i][j] = i * 4 + j;
//		}
//	}
//	for (i = 0; i < 3; i++)
//	{
//		int j = 0;
//		for (j = 0; j < 4; j++)
//		{
//			printf("%d ", arr[i][j]);
//		}
//	}
//	return 0;
//}