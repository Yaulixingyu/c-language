#define  _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>


void Swap(int* pa, int* pb)
{
	int tmp = *pa;
	*pa = *pb;
	*pb = tmp;
}

//hoare法
int PartSort1(int* a, int left, int right)
{
	//让key是最左边的数字
	int keyi = left;
	while (left < right)
	{
		while (left < right && a[right] >= a[keyi])
			right--;
		while (left < right && a[left] <= a[keyi])
			left++;
		//到这里都停止了 交换a[left]和a[right]
		Swap(&a[left], &a[right]);
	}
	//让最终位置和key交换
	Swap(&a[keyi], &a[left]);
	return left;
}

//挖坑法
int PartSort2(int* a, int left, int right)
{
	int key = a[left];
	int pit = left;
	while (left < right)
	{
		while (left<right && a[right]>=key)
		{
			right--;
		}
		//right找到了 操作坑
		a[pit] = a[right];
		pit = right;

		while (left < right && a[left] <= key)
		{
			left++;
		}
		//left填坑，操作
		a[pit] = a[left];
		pit = left;
	}
	//说明相遇
	a[pit] = key;
	return pit;
}

//前后指针法
int PartSort3(int* a, int left, int right)
{
	int prev = left;
	int cur = left + 1;
	while (cur <= right)
	{
		if (a[cur] < a[left] && a[++prev] != a[cur])
			Swap(&a[prev], &a[cur]);
		//到这里说明无论交不交换，cur都要往后走
		cur++;
	}
	Swap(&a[left], &a[prev]);
	return prev;
}

void QuickSort(int* a, int begin, int end)
{
	if (begin >= end) return;
	//定义keyi
	int keyi = PartSort3(a, begin, end);
	//区间划分 [begin,keyi-1] keyi [keyi+1,end]
	QuickSort(a, begin, keyi - 1);
	QuickSort(a, keyi+1, end);
}

void TestQuickSort(int* a, int n)
{
	QuickSort(a, 0, n - 1);
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
}

//int main()
//{
//	int a[] = { 3,7,2,5,1,6,10,9,4,8 };
//	//{ 100,206,144,660,588,233,724,401,769,295,408,945,21,646,924,67,264,297,989,830,727,515,228,123,742,563,959,55,676,839,635,627,141,311,819,120,21,484,143, 301, 283, 394, 439, 151, 757, 727, 931, 424, 392, 452, 392, 428, 346, 405, 26, 687, 135, 909, 972, 246, 295, 645, 686, 948, 823, 59, 106, 456, 294, 805, 565, 673, 312, 726, 140, 622, 626, 569, 432, 303, 585, 110, 301, 834, 198, 394, 746, 463, 112, 943, 191, 12, 42, 552, 12, 552, 186, 610, 213, 381, 128 };
//	TestQuickSort(a, sizeof(a) / sizeof(int));
//	return 0;
//}


#include <string.h>
struct student
{
	int id;
	char name[20];
	double score;
}s1;

//int main()
//{
//	s1.id = 1;
//	strcpy(s1.name,"蔡徐坤");
//	s1.score = 90;
//	printf("%d %s %lf\n", s1.id,s1.name,s1.score);
//	strcpy(s1.name, "狗八");
//	printf("%d %s %lf\n", s1.id, s1.name, s1.score);
//
//	struct student s2 = { 1,"七喜",96 };
//	printf("%d %s %lf\n", s2.id, s2.name, s2.score);
//
//	return 0;
//}

//归并排序
void _MergeSort(int* a, int begin, int end, int* tmp)
{
	if (begin >= end) return;

	int mid = (begin + end) / 2;
	//此时递归区间划分
	//[begin,mid],mid,[mid+1,end]
	_MergeSort(a, begin, mid, tmp);//递归左区间
	_MergeSort(a, mid+1, end, tmp);//递归右区间

	//到这一步说明递归完成了 已经有了最小子区间
	int begin1 = begin, end1 = mid;
	int begin2 = mid + 1, end2 = end;
	int index = begin;//这个来记录tmp数组的下标
	while (begin1 <= end1 && begin2 <= end2)
	{
		//归并选小  思想简单 不难 
		if (a[begin1] < a[begin2])
		{
			tmp[index++] = a[begin1++];
		}
		else
		{
			tmp[index++] = a[begin2++];
		}
		//这里一定都是后置++,是先赋值再往后++
	}
	
	//到这里一定有一个区间取完了，让另一个区间直接链接下来即可
	while (begin1 <= end1)
	{
		tmp[index++] = a[begin1++];
	}
	while (begin2 <= end2)
	{
		tmp[index++] = a[begin2++];
	}

	//到这里tmp已经拿到了两个区间的升序，再memcpy回元素组
	memcpy(a + begin, tmp + begin, (end - begin + 1) * sizeof(int));
	//这里一定要控制好区间的通用性
}

void MergeSort(int* a, int n)
{
	//开辟临时数组
	int* tmp =(int*) malloc(sizeof(int)*n);
	assert(tmp);

	//子归并排序
	_MergeSort(a, 0, n - 1, tmp);

	free(tmp);
}


void TestMergeSort()
{
	int a[] = { 3,1,2,5,7,4,9,6,10,8 };
	MergeSort(a, sizeof(a) / sizeof(int));
	for (int i = 0; i < sizeof(a) / sizeof(int); ++i)
	{
		printf("%d ", a[i]);
	}
}


//非递归思想和递归类似

void MergeSortNonR(int* a, int n)
{
	//开辟临时空间tmp
	int* tmp = (int*)malloc(sizeof(int) * n);
	assert(tmp);

	int gap = 1;
	while (gap < n)
	{
		//间距gap是一个组,相邻的两个区间归并
		for (int i = 0; i < n; i += 2 * gap)
		{
			int begin1 = i, end1 = i + gap - 1;
			int begin2 = i + gap, end2 = i + 2 * gap - 1;
			//区间进行修正  只有begin1没有越界的风险
			if (end1 >= n)
				end1 = n - 1;
			//begin2越界 说明不存在区间2
			if (begin2 >= n)
			{
				begin2 = n;
				end2 = n - 1;
			}
			//begin2 OK,end2越界,让end2等于n-1即可
			if (begin2 < n && end2 >= n)
				end2 = n - 1;

			//到这里区间全部搞定 只需要合并就好,合并思路不变 不难
			int index = i;//index是新数组下标
			while (begin1 <= end1 && begin2 <= end2)
			{
				if (a[begin1] < a[begin2])
					tmp[index++] = a[begin1++];
				else
					tmp[index++] = a[begin2++];
			}
			while (begin1 <= end1)
				tmp[index++] = a[begin1++];

			while (begin2 <= end2)
				tmp[index++] = a[begin2++];		
		}
		//这个for循环结束，说明两个区间合并到tmp已经是有序的了,memcpy回原数组即可
		memcpy(a, tmp, sizeof(int) * n);

		gap *= 2;
	}

	//释放临时空间
	free(tmp);
}
void TestMergeSortNonR()
{
	int a[] = { 3,1,2,5,7,4,9,6,10,8 };
	MergeSortNonR(a, sizeof(a) / sizeof(int));
	for (int i = 0; i < sizeof(a) / sizeof(int); ++i)
	{
		printf("%d ", a[i]);
	}
}

int main()
{
	//递归实现
	TestMergeSort();
	printf("\n");
	//非递归实现
	TestMergeSortNonR();

	return 0;
}
