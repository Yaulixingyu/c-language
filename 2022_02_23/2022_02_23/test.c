#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//int main()
//{
//	//printf("%s\n", __FILE__);
//	//printf("%d\n", __LINE__);
//	//printf("%s\n",__DATE__);
//	//printf("%s\n", __TIME__);
//
//	int i = 0;
//	//记录日志
//	FILE* pf = fopen("log.txt", "w");
//	if (pf == NULL)
//	{
//		return 1;
//	}
//	for (i = 0; i < 10; i++)
//	{
//		fprintf(pf,"%s %s %s %d %d\n", __DATE__,__TIME__,__FILE__,__LINE__, i);
//	}
//
//	return 0;
//}

//VS2019 不遵循 AXNSI C
//int main()
//{
//	printf("%s\n", );
//	return 0;
//}

//***************************************************************
//#define 

//#define M 100
//
//int main()
//{
//	printf("%d\n", M);
//
//	return 0;
//}

//宏定义
//#define MAX(x,y) ((x)>(y)?(x):(y))
////带括号为了避免表达式与操作符发生优先级冲突 影响结果
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int m = MAX(a, b);
//	printf("%d\n", m);
//	return 0;
//}

//宏也是完成替换的
//	int m = MAX(a, b);
//  int m = a>b?a:b;



//##
//可以把位于它边的符号合成一个符号
//#define CAT(C,num) C##num
//
//int main()
//{
//	int Class104 = 10000;
//	printf("%d\n", CAT(Class, 104));
//	return 0; 
//}

//带副作用的宏参数
//
// ++a
//int main()
//{
//	//getchar();
//	//fgetc();
//	int a = 2;
//	int b = ++a;  //b = 3,a = 3
//	int b = a + 1;//b = 3,a = 2
//
//	return 0;
//}
 
//#define MAX(x,y)((x)>(y)?(x):(y))
//
//int main()
//{
//	int a = 3;
//	int b = 5;
//	//宏作为参数是不计算的 是替换进去的
//	int m = MAX(a++, b++);
//	//int m = ((a++)>(b++)?(a++):(b++)
//	printf("%d\n", m);
//	printf("a = %d b = %d", a, b);
//	return 0;
//}

//宏和函数对比
//宏通常被用来执行简单的计算

//#define MAX(x,y)((x)>(y)?(x):(y)
//
//int main()
//{
//	int a = 3;
//	int b = 5;
//	//宏
//	int m = MAX(a, b);
//
//	printf("%d\n", m);
//	return 0;
//}
//用于调用函数和从函数返回所用的代码可能比执行小型代码的时间更多

//简答逻辑下 ——>宏
//复杂逻辑下 ——>函数

//宏的缺点：
//1、宏体如果过大，预处理时代码篇幅过大
//2、宏没法调试
//3、宏由于类型无关，不够严谨
//4、宏可能带来运算优先级的问题，带来错误。

//#define MALLOC(num,type) (type*)malloc(num*sizeof(type));
//
//int main()
//{
//	int* p = (int*)malloc(10 * sizeof(int));
//
//	int* p2 = MALLOC(10, int);
//
//	return 0; 
//}

//函数是传值进去，计算后
//宏是传表达式，替换表达式