#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//********选择语句**********
//int main() {
//	int input = 0;
//	printf("你要好好学习吗？(1/0)\n");
//	//输入
//	scanf("%d", &input);
//	//判断
//	if (input == 1) {
//		printf("好offer\n");
//	}
//	else {
//		printf("回家卖红薯\n");
//	}
//
//	return 0;
//}

//int main() {
//
//	int a = 0;
//	if (a = 20) {
//		printf("hehe\n");
//	}
//
//	return 0;
//}

//********循环语句*********
//int main() {
//	int line = 0;
//	printf("练习敲代码\n");
//	while (line < 20000) {
//		printf("敲代码 --> %d行\n",line);
//		line = line + 1;
//	}
//	if (line >= 20000) {
//		printf("好offer\n");
//	}
//	else {
//		printf("火候还不够\n");
//	}
//	return 0;
//}
//后期介绍for循环

//********函数***********
//Add(int x, int y) {
//                      //Add --- 函数名   x,y ---形式参数
//	int z = x + y;
//	return z;
//}
//
//int main() {
//	int sum = 0;
//	int num1 = 0;
//	int num2 = 0;
//	scanf("%d %d", &num1, &num2);
//	sum = Add(num1, num2);
//	printf("sum = %d\n", sum);
//}


//数组：一组相同类型元素的集合
//int main() {
//	//arr 就是10个元素的整形数组
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	//数组下标       0,1,2,3,4,5,6,7,8, 9
//	//printf("%d\n", arr[3]);
//	int i = 0;//使用i产生下标
//	while (i <= 9) {
//		printf("%d ", arr[i]);
//		i = i + 1;
//	}
//	//存字符-----字符数组
// //   char ch[5] = { 'a','b','c','d','e' };
//	//char ch[6] = { "hello" };
//	//int arr[10] = {1}; 不完全初始化 把第一个元素初始化为1  剩下为0
//
//	return 0;
//}

//  *************操作符(运算符)***************
//C语言包括了很多操作符 ---->灵活

//算数操作符： + - * / %
//int main() {
//	//int a = 7 / 2;
//	//printf("%d\n", a);// 3
//	//double b = 7.0 / 2;
//	//printf("%lf\n", b);// 3.500000
//
//	////  % 取模(余)
//	//int n = 7 % 2;
//	//printf("%d\n",n);
//	
//	int n = 0;
//	scanf("%d",&n);
//	if (n % 2 == 1) {
//		printf("奇数\n");
//	}
//	else {
//		printf("偶数\n");
//	}
//	//   注意：   %  ---取模操作符只能作用于整数
//
//	return 0;
//}

//移位操作符
//其实是作用于一个数的二进制数
//int main() {
//	int a = 3;
//	int b = a << 1;
//	printf("%d\n", b);
//	//  00000000 00000000 00000000 00000011       --3
//	                   //a<<1
//	//  00000000 00000000 00000000 00000110       --6
//	return 0;
//}

//位操作符
// & 按位与  ^按位异或   |按位或
//int main() {
//	int a = 3;      
//	int b = 5;
//	int c = a & b;
//	printf("%d\n", c);//执行结果： 1
//	// 3(a) 的二进制序列 00000000 00000000 00000000 00000011   
//	// 5(b) 的二进制序列 00000000 00000000 00000000 00000101
//	   //       按位与， 对应的二进制有0，则为0，全1才为1
//	// &               00000000 00000000 00000000 00000001
//
//	int d = a | b;
//	printf("%d\n", d);//执行结果：  7
//	// 3(a) 的二进制序列 00000000 00000000 00000000 00000011   
//    // 5(b) 的二进制序列 00000000 00000000 00000000 00000101
//	//          按位或， 对应的二进制有1，则为1，全0才为0
//    // |               00000000 00000000 00000000 00000111
//
//	int e = a ^ b;
//	printf("%d\n", e);//执行结果：  6
//	// 3(a) 的二进制序列 00000000 00000000 00000000 00000011   
//    // 5(b) 的二进制序列 00000000 00000000 00000000 00000101
//	//          按位异或， 对应的二进制  相同为0，相异为1
//	// |               00000000 00000000 00000000 00000110
//	return 0;
//}

//赋值操作符
//
//int main() {
//	int a = 10;
//	a = a + 4;  //  =   赋值
//	a += 4;     //  +=  复合赋值符
// 
//	a = a - 4;  //  =   赋值
//	a -= 4;     //  -=  复合赋值符
//
//	return 0;
//}

//单目操作符
 //  a + b ; //有两个操作数  ----->双目操作符

//单目操作符    ------>只有一个操作数

//  !
//int main() {
//	int  a = 10;
//	//C语言中 0表示假  非0表示真
//	printf("%d\n", !a);
//	int b = 0;
//	printf("%d\n", !b);	//规定 为1 
//	return 0;
//}

//  -   相反号   很好理解

// sizeof 是一个操作符  用来计算变量或者类型所创建变量占据内存的大小----单位是：字节
//int main() {
//	int a = 10;
//	printf("%d\n", sizeof(a)); // 4
//	printf("%d\n", sizeof(int)); // 4
//
//	return 0;
//}

//   ~   按位取反 --（2进制位）  0 -> 1    1 -> 0
//int main() {
//	int a = -1;
//	// -1 是负整数
//	// 负整数的二进制有原码，反码，补码
//	// 内存中存储的是二进制的补码
//	//   -1 的原码：      10000000 00000000 00000000 00000001
//	//   -1 的反码：      11111111 11111111 11111111 11111110
//	//   -1 的补码：      11111111 11111111 11111111 11111111
//	//   b = ~a  :       00000000 00000000 00000000 00000000
//	int b = ~a;
//	printf("%d\n", b);//执行结果：0
//	return 0;
//}

//  ++a // 前置++  ： 先++  后使用
//  a++ // 后置++  ： 先使用 后++
//  --a // 前置--  ： 先--   后使用
//  a-- // 后置--  ： 先使用 后--
//int main() {
//	int a = 10;
//	int c = a++;
//	int b = ++a;
//	printf("a = %d b = %d c = %d",a, b,c);
//
//	return 0;
//}

//(类型)强制类型转换
//int main() {
//	/*int n =(int)3.14;*/
//	//int n = int (3.14)//error
//	//printf("%d\n", n);
//
//	return 0;
//}

//关系操作符： >    >=    <     <=     !=     ==
//逻辑操作符： 
// && 逻辑与----> 并且
// || 逻辑或----> 或者
//int main() {
//	int a = 3;
//	int b = 5;
//	if (a == 3 && b == 5) {
//		printf("hehe\n");
//	}
//
//	return 0;
//}

// 条件操作符
// exp1 ? exp2 : exp3
// 简化 if 语句
//int main() {
//	int a = 0;
//	int b = 0;
//	int max = 0;
//	scanf("%d %d", &a, &b);
//
//	//比较
//	//if(a > b){
//	//	max = a;
//	//}
//	//else {
//	//	max = b;
//	//}
//	// 三目操作符
//	max = (a > b ? a : b);
//	printf("%d\n", max);
//	return 0;
//}

//逗号表达式
//int main() {
//	int a = 3;
//	int b = 5;
//	int c = 10;
//	//                 0          4  
//	int d = ( a + 2, b = a - 3, c = b + 4); 
//	//逗号表达式会从左享有一次计算，整个逗号表达式的结果是最后一个表达式的结果
//	printf("%d\n", d);
//
//	return 0;
//}

//下标引用操作符  []
//int main() {
//	int arr[10] = { 1,2,3,4,5 };
//	printf("%d\n", arr[4]);  //   []  ---->下标引用操作符
//
//	return 0;
//}

// 函数调用操作符 
//Add(int x, int y) {
//	//                  Add --- 函数名   x,y ---形式参数
//	int z = x + y;
//	return z;
//}
//int main() {
//
//	int sum = Add(3, 5);  //  ( )  -----> 函数调用操作符  操作数 () , 3, 5
//	
//	printf("%d\n", sum);
//
//
//	return 0;
//}

// ********常见关键字********
//  auto  break  case  char const continue default do double else enum
//  extern float for goto if int long register return short signed sizeof
//  static struct  switch  typedef union unsigned void volatile while
 // auto ---  自动  --》 定义自动变量
//int main() {
//	auto int a;//所有局部变量都是 auto 一般省略
//	return 0;
//}

// break ---  终止  --》 在循环中使用 ， switch
// switch   case 
//const 常属性 - 修饰变量  void - 无，空的-使用的函数的返回类型，函数参数，修饰指针
//三个可以自定义的类型：enum - 枚举    struct - 结构体  union - 联合体    
// extern -声明外部符号    
// register - 寄存器  return -返回，一般应用到函数
// static - 静态的 
 
//typedef - 类型重定义
//typedef unsigned int u_int;
//int main() {
//	//unsigned int - 无符号整形
//	unsigned int num = 100;
//
//	u_int num2 = 100;
//	//  u_int 相当于给 unsigned 起了一个别名
//	return 0;
//}
////register - 寄存器关键字
//// 速度越快，造假越高，空间越小；寄存器 - 高速缓存 - 内存 - 硬盘 - 网盘
//int main() {
//	//a是寄存器变量
//	//register  是建议作用，建议编译器放入寄存器
//	register int a = 10;
//    // 寄存器变量不能取地址
//
//	return 0;
//}