#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//实验四  循环结构程序设计
//解决 n!(求一个数的阶乘)； 
//求 1！ + 2！ + 3！ + 4！...... + n!
	int main1() {
		int n = 0;
		printf("请输入n值：\n");
		scanf("%d", &n);
		int ret = 1;
		for (int i = 1; i <= n; i++) {  // for 循环实现
			ret *= i;
		}
		printf("%d", ret);
		return 0;
	}

	int main2() {
		int i = 1, n = 0, ret = 1;
		printf("请输入n值：\n");
		scanf("%d", &n);
		while (i <= n) {   //while 循环实现
			ret *= i;
			i++;
		}
		printf("%d", ret);

		return 0;
	}

	int main3() {
		int i = 1, n = 0, ret = 1;
		printf("请输入n值：\n");
		scanf("%d", &n);
		do                    // do while循环实现
		{
			ret *= i;
			i++;
		} while (i <= n);
		printf("%d", ret);
		return 0;
	}

	// 求 1！+2！+3！+4！......+n!
	int main() {
		int n = 0, i = 0, j = 0, sum = 0;
		printf("请输入n值：\n");
		scanf("%d", &n);
		for (j = 1; j <= n; j++) {
			int ret = 1;
			for (int i = 1; i <= j; i++) {  // for 循环实现单次循环
				ret *= i;
			}
			sum += ret;
		}
		printf("%d", sum);
		return 0;
	}