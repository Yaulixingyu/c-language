#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>;

//int main() {
//	int a, b; 
//	scanf("%d%d", &a, &b);
//	a = a + b;
//	b = a - b; 
//	a = a - b;
//	printf("a=%d#b=%d\n", a, b);
//
//	return 0;
//}

//***********while循环***********
//int main() {
//	int i = 1;
//	//在while循环中，break用于永久的终止循环
//	while (i<=10) {
//		if (i == 5)
//			//break;
//			//continue; //在while循环中，continue的作用是跳过本次循环continue后边的代码
//		              // 直接去判断部分，看是否进行下一次循环
//		printf("%d ",i);
//		i++;
//	}
//	return 0;
//}

//getchar 

//EOF - end of file - 文件结束标志

//int main() {
//
//	int ch = getchar();
//	printf("%c\n", ch);
//	printf("%d\n", ch);//字符对应的ASCII码值
//	putchar(ch);//输出一个字符
//
//	return 0;
//}

int main() {
	int ch = 0;

//ctrl+Z - getchar 读到EOF 就读取结束
	while ((ch = getchar()) != EOF)
	{
		putchar(ch);
	}

	return 0;
}



