#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

// 初始操作符
//int main() {
//	//*************操作符**************
//	//***算数操作符***  + - * / %
//	//***移位操作符***  >>右移操作符   <<左移操作符
//
//	/*float a = 9 / 2.0;
//	printf("%f\n",a);*/
//	 
//	int a = 9 % 2;// % 取余
//	printf("%d\n", a);
//     return  0;
//}


//移位操作符
//int main() {
//	int a = 2;
//	int b = a << 1;
//	printf("%d", b);
//
//	return 0;
//}


//位操作符  :   &按位与  |按位或   ^按位异或


//int main() {
	/*int a = 2;
	a = a + 5;
	a += 5;


	a = a - 3;
	a -= 3;

//	a = a % 3;
//	a %= 3;
//	printf("%d", a);*/
//
//	return 0;
//}

//int main() {
//// 0 表示假，非0就是真
//	int a = 0;
//	printf("%d", !a);//1 就是真
//
//	if (a) {
//		
//		//如果a为真做事
//
//	}
//
//	if (!a) {
//
//		//如果a为假做事
//
//	}
//
//	a = -5;
//	a = -a;
//	a = +a;
//	return 0;
//}

//int main() {
//
////sizeof是一个操作符
//	//计算类型或者变量的大小
//
//	int a = 10;
//	printf("%d\n", sizeof(int));
//	printf("%d\n", sizeof (a));
//
//	return 0;
//}

//int main(){
//	//10
//	int arr[10] = { 0 };
//	printf("%d\n", sizeof (arr));//一个int是4个字节 10个元素就是  40
//	                           //计算的是数组的总大小 单位是字节
//	printf("%d\n", sizeof arr[0]);//一个元素的大小  执行结果：4
//
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	printf("%d\n", sz);    //计算数组元素的个数  执行结果：10
//
//
//	return 0;
//}


//************单目操作符**********

//int main() {
//  //  ~  按位取反
//	int a = 0;
//	printf("%d", ~a);//  a:00000000000000000000000000000000
//	                 // ~a:11111111111111111111111111111111
//	//整数在内存中存储的是补码~
//	//对于正数来说原码 反码 补码 相同
//	 
//	// 对于负数来说
//	//一个整数的二进制表示有3种：原码 反码 补码
//	// -1的原码：10000000000000000000000000000001   
//	// -1的反码：11111111111111111111111111111110
//	//    反码 + 1 = 补码
//	// -1的补码：11111111111111111111111111111111 (取这个)
//	return 0;
//}

//int main() {
//
//	int a = 10;  
//	int b = ++a;//前置++先++ 后使用
//	int c = a++;//后置++  先使用 再++
//
//	printf("%d\n", b);
//	printf("%d\n", c);
//	printf("%d\n", a);//执行结果：11 11 12
//
//
//	return 0;
//}

//int main() {
//
//	int a = 1;
//	int b = (++a) + (++a) + (++a);
//	printf("%d\n", b);//执行结果：12
//
//	return 0;
//}

//int main() {
//	//强制类型转换
//	int a = (int)3.14;
//	printf("%d\n", a);
//
//	return 0;
//}

//***********逻辑操作符***********
// && 逻辑与 
// || 逻辑或
// 
//int main() {
//	int a = 0;
//	int b = 5;
//	int c = a && b;
//    printf("%d\n", c);//执行结果; 0
//	/*if (a && b) {
//
//
//	}*/
//
//
//	return 0;
//}
 
  
//int main() {
//	int a = 1;
//	int b = 0;
//	int c = a|| b;
//	printf("%d\n", c);//执行结果; 1 有一个为真则为真
//	/*if (a && b) {
//
//
//	}*/
//	return 0;
//}

//***********条件操作符(三目操作符)*************
//int main() {
////exp1 成立，exp2计算，整个表达式的结果是：exp2的结果
////exp1 不成立， exp3 计算，整个表达式的结果是：exp3的结果
//
//	int a = 0;
//	int b = 3;
//	int max = 0;
//	/*if (a > b)
//		max = a;
//	else
//		max = b;*/
//	max = a > b ? a : b;//执行结果：3
//	printf("%d\n", max);
//	return 0;
//}

// 逗号表达式：逗号隔开的一串表达式
//int main() {
//	
//	//(2, 4 + 5, 6);
//	int a = 0;
//	int b = 3;
//	int c = 5;
//             //a=5       c=1        b=3
//	int d = (a = b + 2, c = a - 4, b = c + 2);
//	//逗号表达式，是从左向右依次计算的
//	//整个表达式的结果是最后一个表达式的结果
//	printf("%d\n", d);//执行结果：3
//	return 0;
//}

//******下标引用操作符*******
//int main() {
//
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	printf("%d\n", arr[5]);//执行结果：6
//
//	return 0;
//}

//*****函数调用操作符******
//int main() {
//
//	//调用函数的时候，函数名后面()就是函数调用的操作符
//	printf("hehe\n" );//执行结果：hehe 
//	printf("%d\n", 100);//执行结果： 100
//	return 0;
//}   