﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
// std -->标准  i-->imput o-->output
// stdio -->标准输入输出头文件

//初始C语言(二)
//int b = 100;//全局变量
//int main() {
//	int a = 10;//局部变量
//
//
//	return 0;
//}

// 两个整数相加
//
//int main() {
//	int num1 = 0;
//	int num2 = 0;
//	int sum = 0;
//	//输入两个值
//	scanf("%d %d", &num1, &num2);//scanf是C语言提供的一个输入函数
//	// &  -----> 取地址s
//	//计算
//	sum = num1 + num2;
//	printf("%d\n", sum);
//	return 0;
//}
//把#define _CRT_SECURE_NO_WARNINGS 写在第一行
//报错中讲到的scanf_s的函数是VS编译器提供的，不是C语言自己
//的。在其他的编译器下就没scanf_s的函数，也不认识这个函数
//当你在代码中使用了scanf_s函数，你的代码在其他的编译器下就没法正确的编译，
//这就降低了代码的跨平台性
//所以建议写代码时尽量使用标准C提供的功能和函数

//****************变量的作用域*****************
//// 作用域
//int c = 100;
//void test() {
//	printf("c = %d\n", c);
//}
//int main() {
//	printf("c = %d\n", c);
//	test(); // 函数的调用
//	int a = 10;
//	{
//		int b = 20;
//		printf("b = %d\n", b);
//		printf("a = %d\n", a);
//	}
//
////	printf("%d", b);  //出了b的作用域
////	局部变量所在范围是局部范围
//
//	return 0;
//}
//int g = 100;
//int mian() {
//	{
//		g = 200;
//		printf("g = %d\n", g); 
//	}
//	return 0;
//}

//局部变量的作用域：
//变量所在的局部范围
//全局变量的作用域：
//整个工程

// ***************生命周期***********

//int  g = 100;//全局变量的生命周期：
//			 //生命周期main函数进入--->结束 
//			 //整个程序的生命周期
//int main(){
//	{  // 局部变量生命周期：
//	   //进入局部变量所在范围开始 ----> 出了局部变量的范围结束
//		int a = 100;
//		printf("%d\n", a);
//	}
//
//
//	return 0;
//}


//**********常量**************
//int main() {
//	1  ----->  字面常量
//	100;
//	3.14;
//	"abc";
//	'w';
//
//	// 2 ----->  const 修饰的常变量
//	// 虽然下面的num 是不能修改的但是本质上还是一个变量
//	//const int num = 10;
//	//printf("num = %d\n", num); //10
//	//num = 20;//不能被修改
//	//printf("num = %d\n", num); //20
//
//	//数组的概念
//	//int a = 10;
//	//int b = 20;// 0--99
//    //const int n = 100;
//	//int arr[n] = { 1 };//本质上还是个变量
//	//printf(arr);
//
//	//C99 的标准中，支持：变长数组：允许变量来制定数组大小
//	// int n = 100;
//	// int arr[n];
//	return 0;
//}

// 3 ------>  #define 定义的标识符常量
// 一般习惯大写
//#define MAX 100
//int main() {
//	int a = MAX;
//	printf("%d", a);
//	int arr[MAX];
//
//	return 0 ;
//}

// 4 ------>   枚举常量
// 枚举 --列举
// 生活中很多只可以一一列举
// 有些值不适合一一列举
// 性别：男，女，保密
//
//enum Sex {
//	//枚举这种类型的可能取值
//	MALE,
//	FEMALE,
//	SECRET
//};
//int mian() {
//
//	//enum Sex s = MALE;
//	printf("%d\n", MALE);
//	printf("%d\n", FEMALE);
//	printf("%d\n", SECRET);
//	// MALE = 10;//ERROR 常量
//	return 0; 
//}

// 字符串 转义字符 注释
//int main() {
//
//	'a';//字符
//	'#';
//
//	"abc";//字符串
//	"a";
//	""; //空字符串
//
//	// 字符串类型？ --->没有
//	return 0;
//}

//int main() {
//
//	// 可以把字符串放在字符数组中
//	//char ch = "abc";//ERROR
//	char arr1[] = "abc";//字符串的末尾隐藏了一个\0 
//	//   '\0'是字符串的结束标志
//	char arr2[] = { 'a', 'b', 'c' };
//	printf("%d\n", strlen(arr1));// a b c ---> 3
//	printf("%d\n", strlen(arr2));// a b c .....----->随机值
//	//string
//	//string length -- 求字符串长度
//	//int len = strlen("abc");
//	//printf("%d\n",len);
//	//printf("%s\n", arr1);
//	//printf("%s\n", arr2);
//	return 0;
//} 

// ***********转义字符***********
// 转变原来的意思
//
//int main() {
//
//	printf("hehe\nabc");
//	return 0;
//}

//int main() {
//
//	printf("D:\\tC,C++ code\n");
//	printf("he\nhe");
//	// \n ----换行
//
//	return 0;
//
//}

//int main() {
//	// ??)  -   三字母词 -->   ]
//	//printf("(hehe\?\?)");
//
//	printf("%c\n", 'a');
//	printf("%s\n", "\"");
//	return 0;
//}

//int main() {
//	printf("\a");//蜂鸣
//	return 0;
//}


//  \ddd  ddd--->三个八进制数字   
//  \xdd   dd--->两个十六进制数字
//int main() {
//	printf("%c\n", '\130');
//	// \130  ---->  8进制
//	//  130  ---->  1*8^2+3*8^1+0*8^0 = 88
//	//    88 的ASCII码值是X
//
//	printf("%c\n", '\X30');
//	// \x30  ---->  十六进制
//	//  30  ---->  3*16^1+0*16^0 = 48 
//	//    48 的ASCII码值是X
//  
//  printf("%d\n", strlen("c:\test\628\test.c"));
//  执行结果： 14
//	return 0;
//}

// C语言有两种注释方式
// C99之后引入的注释方式 ----> 也是C++的注释风格
/*  C99之前C语言的注释风格----->  C的注释风格*/

/*   缺陷 不支持嵌套  会识别最近的*/

//int main() {
//	//int a = 10;
//	//int b = 20;
//	printf("hehe");
//	return 0;
//}

//使用注释：
// 1： 代码复杂,最好加上注释 ，便于理解
// 2： 代码暂时不想要的时候 也可以注释掉

//#include <stdio.h>
//int main(){
//    int M = 0;
//    printf("输入：");
//    scanf("%d",&M);
//    if(M%5 == 0){
//        printf("YES");
//    }else{
//        printf("NO");
//    }
//    return 0;
//}