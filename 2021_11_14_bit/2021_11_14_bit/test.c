#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

// ***操作符(2)***
//  ++ --
//int main()
//{
//	int a = 10;
//	//int b = ++a; // 前置++ 先自增，后赋值使用
//	// a先自己++,然后加后的a赋值给b
//	//int b = --a; // 前置-- 先自减，后赋值使用        
//	// a先自己--,然后加后的a赋值给b
//	int b = a++;   // 后置++ 先赋值使用，后自增 
//	// a先把自己的值赋值给b，然后
//	int b = a--;   // 后置-- 先赋值使用，后自增 
//	//printf("%d %d", a, b);
//
//
//	//printf("%d ",(++a) + (a++));
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	int* pa = &a;
//	*pa = 20;//解引用操作符
//	printf("%d\n", a);
//	return 0;
//}

//int main()
//{
//	char arr[10] = { 0 };
//	int a =(int) 3.14;
//	// 3.14 -> double -> int
//	printf("%d\n", a); // 只会拿到3
//	printf("%d\n", sizeof(arr));
//	return 0;
//}
//
//int main()
//{
//	int i = 0, a = 0, b = 2, c = 3, d = 4;
//	//i = a++ && ++b && d++;
//	i = a++ || ++b || d++;
//	printf("a = %d\nb = %d\nc = %d\nd = %d\n", a, b, c, d);
//	return 0;
//
//}
// 条件操作符 (也叫三目操作符)
// 表达式1 ? 表达式2 : 表达式3
//  如果表达式1为真 就执行表达式2 否则执行表达式3

//int main()
//{
//	// 求两个数的最大值
//	int a = 0;
//	int b = 12;
//	int max = (a > b ? a : b);
//	printf("%d", max);
//	return 0;
//}

// 逗号表达式
// 从左向右依次执行，最终结果以最后一个式子为准
//int main()
//{
//	int a = 3;
//	int b = 5;
//	int c = 6;
//	int d = (a += 2, b = a - c, c = a + 2 - b);
//	printf("%d", d);// 13
//	return 0;
//}

// 下标引用操作符 [ ]
//int main()
//{	
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	printf("%d\n", arr[7]);
//	// arr[7] --> *(arr+7) --->  
//	return 0;
//}

// 访问一个结构成员

//struct Stu
//{
//	char name[20];
//	int age;
//	double score;
//};
//int main()
//{
//	struct Stu s = { "zhangsan",20,85.5 };
//	// .
//	printf("%s %d %.1lf\n", s.name, s.age, s.score);// 结构体变量.结构体成员
//
//	// ->
//	struct Stu* ps = &s;
//	//printf("%s %d %.1lf\n", (*ps).name, (*ps).age, (*ps).score);
//	
//	printf("%s %d %.1lf\n", ps->name, ps->age, ps->score);
//
//
//	return 0;
//}


// ***表达式求值***

// 隐士类型转换

// 整形提升
// 是按照变量的数据类型的符号位来提升的

//int main()
//{
//	char a = 5;// char 只有
//	// 00000000 00000000 00000000 00000101
//	// 00000101 ---只有8个bit位   ---截断
//	char b = 126;
//	// 00000000 00000000 00000000 01111110
//	// 01111110  --- 截断
//	char c = a + b;
//	// 00000000 00000000 00000000 00000101  -- a
//	// 00000000 00000000 00000000 01111110  -- b 
//	// 00000000 00000000 00000000 10000011
//	// 10000011  --> c
//
//	// 当a和b 相加的时候，a 和 b 都是char类型
//	//表达式计算的是就要发生整形提升
//
//	printf("%d\n", c);
//	// 10000011  -- > c
//	// 11111111 11111111 11111111 10000011 -- 补码
//	// 11111111 11111111 11111111 10000010 --
//	// 10000000 00000000 00000000 01111101 ->  -125
//	return 0;
//}
// 整形提升
//int main()
//{
//	char c = 1;
//    printf("%u\n", sizeof(c));   
//	printf("%u\n", sizeof(+c));
//	printf("%u\n", sizeof(-c));
//	return 0;
//}

//如果某个操作符的各个操作数属于不同的类型，
//那么除非其中一个操作数的转换为另一个操作数的类型
//否则操作就无法进行。下面的层次体系称为寻常算数转换
//int main()
//{
//	int a = 3; 
//	float f = 5.5;
//	float r = a + f;
//
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	int b = 20;
//	a + b;// 表达式有2个属性： 值属性  ，类型属性
//	// 30  ---> 值属性
//	// 
//	// int ---> 类型属性
//
//	return 0;
//}

//sizeof 内部表达式不参与计算
//int main()
//{
//	short s = 20;
//	int a = 5;
//	printf("%d\n", sizeof(s = a + 4));
//	printf("%d\n", s);
//
//	return 0;
//}

// 操作符的属性
int main()
{

	return 0;
}