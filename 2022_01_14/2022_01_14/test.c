#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//int x = 5, y = 7;
//
//void swap()
//{
//	int z;
//	z = x;
//	x = y;
//	y = z;
//}
//
//int main()
//{
//	int x = 3, y = 8;
//	swap();
//	printf("%d %d", x, y);
//
//	return 0;
//}
//int main()
//{
//	int arr[3 + 5] = { 0,1,3,5,7,9 };
//	char c1[] = { '1','2','3','4','5' };
//	char c2[] = { '\x10','\xa','\8' };
//	double x[5] = { 2.0,3.0,4.0,5.0,6.0 };
//
//	return 0;
//}

//static int a[100000];
//int* printNumbers(int n, int* returnSize) 
//{
//    int k = 1;
//    for (int i = 0; i < n; i++)
//        k *= 10;
//    int i;
//    for (i = 1; i < k; i++)
//    {
//        a[i - 1] = i;//a[i-1]下标是从0开始
//    }
//    *returnSize = --i;
//    return a;
//}
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);//输入n
//    int returnSize = 0;
//    printNumbers(n,&returnSize);
//   /* printf();*/
//	return 0;
//}

#include <stdlib.h>
#include <stdio.h>

int main()
{
    int month_days[12] = { 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 };
    int year, month, day;
    while (scanf("%d %d %d\n", &year, &month, &day) != EOF)
    {
        int days = 0;
        if (month >= 2)
            days = month_days[month - 2] + day;
        else
            days = day;
        if (month > 2 && ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0))
            days++;
        printf("%d\n", days);
    }
    return 0;
}

//int main()
//{
//	int b = 0;
//	char c = 1;
//	scanf("%d %s", &b, c);
//
//	printf("%d %s", b, c);
//
//	return 0;
//}
