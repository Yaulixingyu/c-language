﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
/// <summary>
/// 指针:是内存单元的编号，也就是指针
/// 
/// 
 ////内存单元 - 编号 - 地址 - 指针
 ////平时口语中的指针是指针变量，是存放内存变量的
 ////
 ////内存被划分为一个个小的内存单元  一个基本的内存单元的大小是一个字节
 ////地址： 内存单元的编号 0X00000000
 ////内存单元编号的产生： 32位机器-32跟地址线-物理的电线- 通电-1/0
 ////                  00000000 00000000 00000000 00000000 -- 4个字节
 ////                  00000000 00000000 00000000 00000001
 ////                  00000000 00000000 00000000 00000010
 ////                  ....     ...      ...      ...
 ////                  11111111  11111111 11111111 11111111
 ////                  2*10^32 bite   -- 4GB
 ////64位
/// </summary>
/// <returns></returns>
/// 
/// 
//int main()
//{
//	int a = 10;// 四个字节
//
//	int* pa = &a;//pa存的是地址 所以pa就是指针变量
//	printf("%d\n", sizeof(pa));
//	printf("%p", &a);
//	return 0;
//}

//int main()
//{
//	int a = 0x11223344;
//	int* pa = &a;
//	*pa = 0;
//	//char* pc = &a;
//	//*pc = 0;
//	//int*   --> 4
//	//char*   --> 1
//	//double*   --> 8
//
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	int* pa = &a;//整形  --4 
// 	char* pc = &a;//字符 --1
//	printf("%p\n", pa);
//	printf("%p\n", pa+1);
//
//	printf("%p\n", pc);
//	printf("%p\n", pc+1);
//
//	//指针类型决定了指针向前或者向后走一步走的距离 -- 单位是字节
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 0 };
//	int* p = arr;
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		//正着初始化
//		*(p+i) = i+1;
//	}
//	//正着打印
//	int* m = &arr[0];
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", *m);
//		m++;
//	}
//	printf("\n");
//	//倒着打印  9 8 7 ...
//	int* q = &arr[9];
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", *q);
//		q--;
//	}
//	return 0;
//}

//int main()
//{
//	int arr[10] = {0};
//	char* p = (char*)arr;//arr是int形数组 强制转换成char形
//	for (int i = 1; i < 10; i++)
//	{
//		*(p + i*4) = i ;
//	}
//	//正着打印
//	return 0;
//}

// 野指针
//#include <stdio.h>
//int main()
//{
//	int* p;//局部变量指针未初始化，默认为随机值
//	*p = 20;
//	return 0;
//}

//int main()
//{
	//int a; // 随机值
	//int* p; // 随机值   *p 随机地址
	//*p = 20;  // 随机地址 放一个20  就是野指针 没有明确指向

	//int arr[10] = { 0 };
	//int* p = arr;
	//for (int i = 0; i <= 10; i++)
	//{
	//	*p = i;
	//	p++;
	//}
//return 0;
//}

//指针指向的空间释放
//
//int* test()
//{
//	int a = 100;
//	return &a;
//}
//int main()
//{
//	int* p = test();
//	printf("%d", *p);
//	return 0;
//}
//
//int main()
//{
//	int a = 10;
//	int* pa = &a;// 知道存给谁
//
//	int* p = NULL;//初始化成空指针
//	if (p != NULL)
//	{
//
//	}
//
//	return 0;
//}

//指针运算

//指针加减整数
 

//初始化并赋值打印
//int main()
//{
//	int arr[10] = { 0 };
//	int* p = arr;
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (i = 0; i < sz; i++)
//	{
//		*(p + i) = i+1;
//	}
//	//打印
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	return 0;
//}

//指针减指针

//int main()
//{
//	/*int a[10] = { 0 };
//	
//	printf("%d\n", &a[9] - &a[0]);
//	printf("%d\n", &a[0] - &a[9]);*/
//
//	int a = 10;
//	char c = 'w';
//
//
//	return 0;
//}


//求字符串长度的函数 strlen
#include <string.h>

//int My_strlen(char* s)
//{
//	//int count = 0;
//	//while(*s != '\0')
//	//{
//	//	count++;
//	//	s++;
//	//}
//	//return count;
//
//	char* start = s;
//	while (*s != '\0')
//	{
//		s++;
//	}
//	return  s - start;
//}
//int main()
//{
//	char arr[] = "abc";
//	int len =My_strlen(arr);
//
//	printf("%d\n", len);
//	
//	return 0;
//}

//指针的关系运算

//int main()
//{
//	float a[5];
//	float* p;
//	//for (p = &a[5]; p >= &a[0];)
//	//{
//	//	*--p = 0;
//	//}
//
//	for (p = &a[4]; p >= &a[0]; p--)
//	{
//		*p = 0;
//	}
//
//	return 0;
//}

//指针和数组

//// 指针 -- 地址
//// 数组 -- 一组相同类型的数据
////int main()
////{
////	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
////	//arr首元素地址
////	int* p = arr;
////	int i = 0;
////	for (i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
////	{
////		printf("%p == %p \n", p + i,&arr[i]);
////	}
////
////
////	return 0;
////}
//#include <stdio.h>
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,0 };
//	int* p = arr; //指针存放数组首元素的地址
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	
//	for (int i = 0; i < sz; i++)
//	{
//		printf("&arr[%d] = %p   <====> p+%d = %p\n", i, &arr[i], i, p + i);
//	}
//	return 0;
//}

//二级指针

//int main()
//{
//	int a = 10;
//	int* pa = &a;
//	int** ppa = &pa; //ppa就是一个二级指针
//	**ppa = 20;
//	printf("%d\n",a);
//
//	return 0;
//}

//指针数组

// 
//int main()
//{
//	int arr[10];//整形数组
//	char ch[5];//字符数组
//
//	//指针数组 --- 存放指针的数组
//	int a = 10;
//	int b = 20;
//	int c = 30;
//	int* arr2[5] = {&a,&b,&c};//存放整形指针的数组
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		printf("%d ", *(arr2[i]));
//	}
//	return 0;
//}

//int main()
//{
//	int x, y;
//	scanf("%d", &x);
//	if (x >= 0)
//		if (x > 0)y = 1;
//		else y = 0;
//	else y = -1;
//	printf("%d", y);
//	return 0;
//}

//void fun(int* p)
//{
//	(*p)++;
//}
//main()
//{
//	int a = 5;
//	fun(&a);
//	printf("%d", a);
//}

//int main()
//{
//	double a = 2.5, b;
//	double *p = &a;
//	double *q = &b;
//
//	*&b = *p;
//
//	*q = *&a;
//	printf("%lf", b);
//
//	return 0;
//
//}
//int main()
//{
//	int s = 0;
//	s = 32;
//	s ^= 32;
//	printf("%d", s);
//}
int main()
{
	int a = 0, b = 0;
	a = (b++, 4);
	printf("%d %d", a, b);
}
