#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>


int main() {
	for (int i = 1; i <=10;i++ ) {
		if (i == 5) {
			continue;
		}
		printf("%d ", i);
	}
	return 0;
}
//int main() {
//    while (1) {
//        int n = 0;
//        printf("请输入n值： \n");
//        scanf("%d", &n);
//        double sum = 0;
//        int flg = 1;
//        for (int i = 1; i <= n; i++) {
//            sum = sum + 1.0 / i * flg;
//            flg = -flg;
//        }
//        printf("%lf", sum);
//    }
//	return 0;
//}

//************乘法口诀表**********
//int main() {
//	for (int i = 1; i <= 9; i++) {
//		for (int j = 1; j <= i; j++) {
//			printf("%d*%d=%d ", j, i, i * j);
//		}
//		printf("  \n");
//	}
//	return 0;
//}