﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//int main()
//{
////	    char        //字符数据类型
////	 	short       //短整型
////		int         //整形
////		long        //长整型
////		long long   //更长的整形
////		float       //单精度浮点数
////		double      //双精度浮点数
//	//sizeof
//	//printf("%d\n", sizeof(char));
//	//printf("%d\n", sizeof(short));
//	//printf("%d\n", sizeof(int));
//	//printf("%d\n", sizeof(long));
//	//printf("%d\n", sizeof(long long));
//	//printf("%d\n", sizeof(float));
//	//printf("%d\n", sizeof(double));
//
//	//float f1 = 3.14f;
//	//printf("%f", f1);
//
//	//double f1 = 10.0;
//	//
//	//double f2 = 2.0;
//	//double f3 = f1 / f2;
//	//if (f3 == 5.0)
//	//{
//	//	printf("相等");
//	//}
//	//else
//	//{
//	//	printf("不相等");
//	//}
//
//	double d1 = 5.0;
//	double d2 = d1 / 2;
//	int a1 = 5;
//	//double a2 = (double)a1 / 2;
//	//printf("%lf", a2);
//	//printf("%lf", d2);
//	//int a2 = d1 / 2.0;  //error
//	//printf("%d", a2);
//
//	return 0;
//}

//变量
//#include "test.h"
//int a = 10;//全局  整个都能访问
//int b = 10;
//int c = 20;
//int main()
//{
//	int a = 30; //局部变量   
//	int b = 20;
//	printf("%d\n", b);
//	printf("%d\n", a);
//	printf("%d\n", c);
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	scanf("%d", &a);
//	a = 30;
//	printf("%d", a);
//	return 0;
//}

//int main()
//{
//	int num1 = 0;
//	int num2 = 0;
//	scanf("%d %d", &num1, &num2);
//	int sum = num1 + num2;
//	printf("%d\n", sum);
//	return 0;
//}