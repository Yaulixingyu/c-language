#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
//int main()
//{
//	int* p = (int*)malloc(INT_MAX);
//
//	if (p == NULL)
//	{
//		return 0;
//	}
//
//	int i = 0;
//	
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = i;
//	}
//
//	return 0;
//}

//2.对动态开辟空间的越界访问
//int main()
//{
//	char* p = (char*)malloc(10 * sizeof(char));
//	if (p == NULL)
//	{
//		printf("%s\n", strerror(errno));
//	}
//	//使用
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = 'a' + i;
//	}
//
//	//释放
//	free(p);
//	p = NULL;
//
//	return 0;
//}

//3.对非动态开辟内存使用free释放
//int main()
//{
//	int* p = (int*)malloc(40);
//
//	//free(p);
//	//p = NULL;
//	
//	int a = 10;
//	int* p = &a;
//	
//	//写代码
//
//	free(p);
//	p = NULL;
//
//	
//	return 0;
//}

//4.使用free释放一块动态开辟内存的一部分
//int main()
//{
//	int* p = (int*)malloc(40);
//	if (p == NULL)
//	{
//		printf("%s\n", strerror(errno));
//		return 0;
//	}
//	//使用内存
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		*p = i + 1;
//		p++;
//	}
//	//释放
//	free(p);
//	p = NULL;
//	//不要重复释放
//
//	free(p);
//	return 0;
//}

//动态内存开辟忘记释放(内存泄漏)

void test()
{
	int* p =(int*) malloc(100);
	if (p == NULL)
	{
		return 0;
	}
	//使用

	//忘记释放，就会出现内存泄漏的问题
	free(p);
	p = NULL;

}

//int main()
//{
//
//
//	test();
//	return 0;
//}

//避免发生:谁申请的空间谁去释放
// malloc 和 free 成对使用

//void GetMemory(char** p)
//{
//	*p = (char*)malloc(100);
//}
//void Test(void)
//{
//	char* str = NULL;
//	str = GetMemory(str);
//	
//}


//柔型数组

//给柔性数组开辟空间必须是malloc开辟出来的

struct S1
{
	int n;
	int arr[0];//大小未指定
};

struct S2
{
	int n;
	int arr[];
};

//int main()
//{
//	//printf("%d\n", sizeof(struct S1)); // 4 
//	struct S2* ps = malloc(sizeof(struct S2)+40);
//
//	ps->n = 100;
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		ps->arr[i] = i; 
//	}
//	//增容
//	struct S2* ptr = realloc(ps, sizeof(struct S2) + 80);
//	if(ptr == NULL)
//	{
//		return 0;  
//	}
//
//	//释放
//	free(ps);
//	ps = NULL;
//	return 0;
//}

//1:开辟次数多，容易出错
//2:容易行程内存碎片
//3:访问速度慢                                                                                    
//struct S
//{
//	int n;
//	int* arr;
//};
//int main()
//{
//	struct S* ps = (struct S*)malloc(sizeof(struct S));
//	ps->n = 100;
//	ps->arr = (int*)malloc(40);
//
//	//使用
//	
//	//增容
//
//	free(ps -> arr);
//	ps->arr = NULL;
//	free(ps);
//	ps = NULL;
//	return 0;
//}