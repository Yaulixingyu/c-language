#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
int main() {
	int num, i, count = 0;
	printf("请输入一个整数:");
	scanf("%d", &num);
	if (num == 0) {
		printf("%d不是一个质数!", num);
	}
	for (i = 2; i <= (num/2) ; i++) {
		if (num % i == 0) {
			count++;
			break;
		}
	}
	if (num != 1 && count == 0) {
		printf("%d是一个质数!\n", num);
	}
	else {
		printf("%d不是一个质数!\n", num);
	}
	return 0;
} 