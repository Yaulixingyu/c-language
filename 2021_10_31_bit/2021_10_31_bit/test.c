#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
//****go to 语句*****
// 多层循环这种情况使用break是达不到目的的，他只能从最内层循环退出上一层的循环
// 这时候 goto语句适合这种场景
//for () {
//	for () {
//		for () {
//			if (disaster) {
//				goto error;
//			}
//		}
//	}
//}
//...
//error:
//if(disaster)
//// 处理错误情况

//int main() {
//
//again:
//	printf("hehe\n");
//	printf("haha\n");
//	goto again; // 标签一致就行，标签必须在同一个函数内
//
//	return 0;
//}
//例如： 写一个关机程序
// 只要程序启动了，就倒计时60秒关机，如果60秒之内，输入：我是猪，就取消关机，如果不输入，时间到就关机
//  
// shutdown   windows提供的关机命令
// shutdown -s -t 60秒之后关机
// shutdown -a 取消关机
//int main() {
//
//	char input[20] = { 0 };
//	system("shutdown -s -t 60");
//
//again:
//	printf("请注意，你的电脑在1分钟内关机，如果输入：我是猪，就取消关机\n");
//	scanf("%s", input);  //input本来就是地址 不用取地址
//	//判断
//	if (strcmp(input,"我是猪") == 0) {
//		// 取消关机
//		system("shutdown -a");
//	}
//	else {
//		goto again;
//	}
//
//	return 0;
// }
// 
// *******函数*******

// 库函数
//#include <stdio.h>
//#include <string.h>
//int main() {
//	char arr[] = "abc";
//	// size_t  -> unsigned int
//	size_t len = strlen(arr);
//	printf("%u\n", len);
//	// %d  - 有符号
//	// %u  - 无符号 （没有负数）
//	return 0;
//}
// 
// 
//#include <stdio.h>
//#include <string.h>
//int main() {
//	char arr1[20] = { 0 };//  目的地
//	char arr2[] = "hello" ;// 源数据
//	strcpy(arr1, arr2);
//	printf("%s\n",arr1);
//
//	return 0;
//}

//#include <stdio.h>
//#include <string.h>
//int main(){
//	char arr[] = "hello world";
//	memset(arr, 'x', 5);
//	printf("%s\n", arr);
//
//	return 0;
// }

//***自定义函数***
// 函数返回类型  函数名   参数
// ret_type fun_name(para1,*) 
// {                             //     
//   statement; // 语句项         //函数体
//               }               //

// 写一个函数，找出两个数的最大值
//返回类型   函数名   参数
//int get_max(int x, int y) {
//	int z = 0;
//	z = (x > y ? x : y);
//	return z;
//}
//
//int main() {
//	int a = 0;
//	int b = 0;
//	scanf("%d %d", &a, &b);
//	int m = get_max(a, b);// 函数调用  //变量 常量 函数 表达式
//	printf("%d\n", m);
//
//	return 0;
//}
//
// 无返回
//void menu() {
//
//	printf("******  1. play  ******\n");
//	printf("******  0. exit  ******\n");
//}
//int main() {
//	
//	menu();
//	return 0;
//}

// 用函数交换两个整形变量的内容

//void Swap1(int x, int y) {
//	int  temp = 0;
//	temp = x;
//	x    = y;
//	y    = temp;
//}
//  //失败的原因是：a 和 b 是实参 ， x 和 y 是形参
//            //  当函数调用的时候，实参传给形参，形参其实是实参的一份临时拷贝
//      //  所以对形参的修改，不会影响实参。
//// 正确写法    使用指针
//void Swap2(int* pa,int* pb) {
//	int t = 0;
//	    t = *pa;
//	  *pa = *pb;
//	  *pb = t;
//
//}
//
//int main() {
//	int a = 0, b = 0;
//	scanf("%d %d", &a, &b);
//
//	// 交换两个变量
//	printf("交换前a = %d, b = %d\n", a, b);
//	//Swap1(a,b);   // 传值调用
//	Swap2(&a,&b); // 传址调用
//	printf("交换后a = %d, b = %d\n", a, b);
//
//	return 0;
//}

// 函数的参数
// 1>实际参数
// 2>形式参数

//***函数的调用***
// 1> 传值调用
// 2> 传址调用

// 练习
// 写一个函数可以判断一个数字 是不是素数

 //is_prime()
 //是素数返回 1 ，不是素数返回 0
//int is_prime(int n) {
//
//	// 2 ~ n-1 的数字试除
//	int j = 0;
//	for (j = 2; j <= sqrt(n); j++) {
//		if (n % j == 0) {
//			return 0;
//		}
//		
//	}
//	return 1;
//
//}
//
//int main() {
//	int i = 0;
//	for (i = 100; i <= 200; i++) {
//
//		// 判断 i 是否为素数 - 如果是素数就打印
//		// i 取 100 - 200
//		if (is_prime(i) == 1) {
//			printf("%d ", i);
//		}
//	}
//
//	return 0;
//}




 //写一个函数判断一年是不是闰年
 //是闰年 返回1 不是闰年 返回0
//int is_leap_year(int y) {
//	//if (y % 100 != 0 && y % 4 == 0 || y % 400 == 0) {
//	//	return 1;
//	//}
//	//else {
//	//	return 0;
//	//}
//
//	return (y % 100 != 0 && y % 4 == 0 || y % 400 == 0);
//}
//int main() {
//
//	int y = 0;
//	for (y = 1000; y <= 2000; y++) {
//
//		// 判断 y 是不是闰年
//		if (is_leap_year(y) == 1) {
//			printf("%d ",y);
//		}
//	}
//
//	return 0;
//}


 //写一个函数 ，实现一个整形有序数组的二分查找
 //如果找到了返回 下标
 //如果找不到返回  -1 
//int binary_search(int arr[], int k,int sz) {
//	int left = 0;
//	int right = sz - 1;
//
//	while (left<=right) {
//		int mid = (left + right) / 2;
//		if (arr[mid] < k) {
//			left = mid + 1;
//		}
//		else if (arr[mid] > k)
//		{
//			right = mid - 1;
//
//		}
//		else if (left == right) {
//			return mid;
//		}
//	}
//	return -1;
//
//}
//int main() {
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int k = 7;
//	int sz = sizeof(arr) / sizeof arr[0];
//	   //      40        /  4
//	// 数组arr传给binary_search函数的时候，其实传递的arr数组首元素的地址
//	int ret = binary_search(arr,k,sz); // 在arr数组中找k
//	if (-1 == ret) {
//
//		printf("找不到\n");
//	}
//	else {
//		printf("找到了，下标是%d\n", ret);
//	}
//	return 0;
//}

//实现一个函数，打印乘法口诀表，口诀表的行数和列数自己指定

//如：输入9，输出9 * 9口诀表，输出12，输出12 * 12的乘法口诀表。
//void Multiplication_formula_table(int i, int j,int n) {
//	for (i = 1; i <= n; i++) {
//		for (j = 1; j <= i; j++) {
//			printf("%d*%d=%d ", j, i, i* j);
//		}
//		printf("\n");
//	}
//}
//int main() {
//	int n = 0;
//	scanf("%d",&n);
//	int i = 0;
//	int j = 0;
//	Multiplication_formula_table(i ,j,n);
//	return 0;
//}
