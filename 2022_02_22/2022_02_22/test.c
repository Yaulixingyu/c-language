#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//模拟实现qsort函数，排序结构体数据
//struct Stu
//{
//	char name[20];
//	int age;
//	float score;
//};
////测试qsort排序结构体数据
//void print_stu(struct Stu arr[], int sz)
//{
//	int i = 0;
//	for (i = 0; i < sz; i++)
//	{
//		printf("%s %d %.2f\n", arr[i].name, arr[i].age, arr[i].score);
//
//	}
//	printf("\n");
//}
//
////测试qsort排序结构体数据
//int cmp_stu_by_score(const void* e1, const void* e2)
//{
//	//从大到小排序
//	if ((((struct Stu*)e1)->score) < (((struct Stu*)e2)->score))
//		return 1;
//	else if ((((struct Stu*)e1)->score) < (((struct Stu*)e2)->score))
//		return -1;
//	else
//	{
//		return 0;
//	}
//}
//
//void Swap(char* buf1, char* buf2, int width)
//{
//	int i = 0;
//	for (i = 0; i < width; i++)
//	{
//		char tmp = *buf1;
//		*buf1 = *buf2;
//		*buf2 = tmp;
//		buf1++;
//		buf2++;
//	}
//}
//
//
//void bubble_sort(void* base, int sz, int width, int(*cmp)(const void* e1, const void* e2))
//{
//	//趟数
//	int i = 0;
//	for (i = 0; i < sz - 1; i++)
//	{
//		//每一趟冒泡排序的过程
//		//确定一趟排序中比较的对数学
//		int j = 0;
//		for (j = 0; j < sz - 1 - i; j++)
//		{
//			/*if (arr[j] > arr[j + 1])*/
//			if (cmp((char*)base + j * width, (char*)base + (j + 1) * width) > 0)
//			{
//				//两个元素的交换
//				Swap((char*)base + j * width, (char*)base + (j + 1) * width, width);
//			}
//		}
//	}
//
//}
//void test4()
//{
//	struct Stu arr[] = { {"Jennie",20,87.5f},{"Lisa",20,99.0f},{"Jisoo",22,100.0f},{"Rose",21,93.5f} };
//
//	//按照成绩排序
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	bubble_sort(arr, sz, sizeof(arr[0]), cmp_stu_by_score);
//	print_stu(arr, sz);
//}
//
//int main()
//{
//	test4();
//	return 0;
//}

//*****************************************************
//strlen 函数及其模拟实现

#include <string.h>
//1.计数器的方法
//2.递归的方法
//3.指针-指针
//4.调试
//

//int main()
//{
//	int len = strlen("abcdef");
//	printf("%d\n", len);
//	return 0;
//}
#include <assert.h>
//1.计数器的方法
//int my_strlen(const char* str)
//{
//	assert(str);
//	int count = 0;
//	while (*str != '\0')
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//
//int main()
//{
//	int len = my_strlen("abcdef");
//	printf("%d\n", len);
//	return 0;
//}



//*******************************************************
//strcpy  -- 拷贝字符串
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[20] = "XXXXXXXXXX";
//	strcpy(arr2, arr1);
//	printf("%s\n", arr2);
//	return 0;
//}

//模拟实现strcpy
//char* my_strcpy(char* dest,const char* src)
//{
//	char* ret = dest;
//	assert(dest && src);
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[20] = "XXXXXXXXXX";
//	my_strcpy(arr2, arr1);
//	printf("%s\n", arr2);
//	return 0;
//}



//*****************************************************
//strcat  
//int main()
//{
//	char arr1[30] = "hello";
//	char arr2[] = "world";
//	strcat(arr1, arr2);
//	printf("%s\n", arr1);
//	return 0;
//}

//模拟实现
//char* my_strcat(char* dest,const char* src)
//{
//	char ret = dest;
//	assert(dest && src);
//	//目标空间的\0
//	while (*dest != '\0')
//	{
//		dest++;
//	}
//	//追加内容到目标空间
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}
//
//int main()
//{
//	char arr1[30] = "hello";
//	char arr2[] = "world";
//	my_strcat(arr1, arr2);
//	printf("%s\n", arr1);
//	return 0;
//}


//***********************************************
//strcmp
//int main()
//{
//	//比较的是对应位置上的字符大小
//	char arr1[]= "abcdef";
//	char arr2[] = "abq";
//	//a和a相比 相等 往后走
//	//返回 >1 相等0  <-1
//	int ret = strcmp(arr1, arr2);
//	printf("%d\n", ret);
//	return 0;
//}

//模拟实现
//int my_strcmp(const char* str1, const char* str2)
//{
//	assert(str1 && str2);
//	while (*str1 == *str2)
//	{
//		if (*str1 == '\0')
//			return 0;
//		str1++;
//		str2++;
//	}
//	if (*str1 > *str2)
//		return 1;
//	else
//		return -1;
//}
//int main()
//{
//	//比较的是对应位置上的字符大小
//	char arr1[] = "abcdef";
//	char arr2[] = "abq";
//	int ret =my_strcmp(arr1, arr2);
//	printf("%d\n", ret);
//	return 0;
//}

//**************************************************************
//strstr
//int main()
//{
//	char arr1[] = "abcdef";
//	char arr2[] = "bcd";
//	char* ret = strstr(arr1, arr2);
//	if (NULL == ret)
//	{
//		printf("没找到\n");
//	}
//	else
//	{
//		printf("%s\n", ret);
//	}
//	return 0;
//}

//模拟实现
//char* my_strstr(const char* str,const char* substr)
//{
//	const char* s1 = str;
//	const char* s2 = substr;
//	char* cur = str;
//
//	assert(str && substr);
//	if (*substr == '\0')
//	{
//		return str;
//	}
//	while (*cur)
//	{
//		s1 = cur;
//		s2 = substr;
//		while (*s1 && *s2 &&*s1 == *s2)
//		{
//			s1++;
//			s2++;
//		}
//		if (*s2 == '\0')
//		{
//			return cur;
//		}
//		cur++;
//	}
//	return NULL;
//
//}
//int main()
//{
//	char arr1[] = "abbbcdef";
//	char arr2[] = "bbc";
//	char* ret = my_strstr(arr1, arr2);
//	if (NULL == ret)
//	{
//		printf("没找到\n");
//	}
//	else
//	{
//		printf("%s\n", ret);
//	}
//	return 0;
//}



//********************************************************
//memcpy
int main()
{
	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
	int arr2[5] = { 0 };
	memcpy(arr2, arr1, 20);
	int i = 0;
	for (i = 0; i < 5; i++)
	{
		printf("%d ", arr2[i]);
	}
	return 0;
}

//模拟实现
//void* my_memcpy(void* dest,const void* src, size_t num)
//{
//	void* ret = dest;
//	assert(dest && src);
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		dest =(char*)dest + 1;
//		src = (char*)src + 1;
//	}
//	return ret;
//}
//int main()
//{
//	int arr1[] = { 1,2,3,4,5,6,7,8,9,10 };
//	int arr2[5] = { 0 };
//	my_memcpy(arr2, arr1, 20);
//	int i = 0;
//	for (i = 0; i < 5; i++)
//	{
//		printf("%d\n", arr2[i]);
//	}
//	return 0;
//}


//***************************************************************
//memmove
//模拟实现
//void* my_memmove(void* dest, const void* src, size_t num)
//{
//	void* ret = dest;
//	assert(dest && src);
//	if (dest < src)
//	{
//		//前->后
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest + 1;
//			src = (char*)src + 1;
//		}
//	}
//	else
//	{
//		//后->前
//		while (num--)
//		{
//			*((char*)dest + num) = *((char*)src + num);
//		}
//	}
//	return ret;
//}
//int main()
//{
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	my_memmove(arr+2, arr, 20);
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d\n", arr[i]);
//	}
//	return 0;
//}