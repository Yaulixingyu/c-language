#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>

// C语言进阶 

// 数据的存储

//数据类型的介绍

//C语言中 用 0 表示假
//         !0 表示真
//int main()
//{
//	int flag = 0;
//	if (flag)
//	{
//		!flag 表示真
//	}
//	return 0;
//}

//C99中引入了布尔类型
#include <stdbool.h>
//int main()
//{
//	_Bool flag = true;//false表示假
//	if (flag)
//	{
//		printf("hehe");
//	}
//
//	return 0;
//}

//int main()
//{
//	int num = 10;
//	printf("%d\n", num);
//
//	num = -10;
//	printf("%d\n", num);
//
//	unsigned int num2 = 10;
//	printf("%u\n", num2);
//
//	num2 = -10;
//	printf("%u\n", num2);
//
//
//}

//int main()
//{
//	//可以通过改变类型和个数来改变数组类型
//	int a[10];
//
//	int a[5];
//	return 0;
//
//}


//没有返回值
//void test(void)
//{
//	void* p;//无具体类型的指针
//}
//int main()
//{
//	test();
//	return 0;
//}


//
//int main()
//{
//	int a = 0x11223344;
//
//	return 0;
//}

//设计一个程序判断编译器是大端存储还是小端存储
//int main()
//{
//	int a = 1;
//	char* p =(char*) &a;//int*
//	if (*p == 1)
//	{
//		printf("小端\n");
//	}
//	else
//	{
//		printf("大端\n");
//
//	}
//	return 0;
//}


//int check_sys()
//{
//	int a = 1;
//	char* p = (char*)&a;
//	/*if (1 == *p)
//		return 1;
//	else
//		return 0;*/
//	/*return *p;*/
//	return *(char*)&a;
//}
//
//int main()
//{
//	int ret = check_sys();//返回1是小端，返回0是大端
//	if (1 == ret)
//		printf("小端\n");
//	else
//		printf("大端\n");
//	return 0;
//}
////
/*

描述
不使用累计乘法的基础上，通过移位运算（<<）实现2的n次方的计算。
数据范围：
输入描述：一行输入整数n（0 <= n < 31）。
输出描述：输出对应的2的n次方的结果。
示例1
输入：
2
输出：
4

示例2
输入：
10
输出：
1024
*/

//常规算法
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int sum = pow(2, n);
//	printf("%d", sum);
//	return 0;
//}

//位运算
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	//移位
//	int sum = 1 << n;
//	//或者
//	int sum2 = 2 << (n - 1);
//	printf("%d", sum);
//
//	return 0;
//}


/*
描述
KiKi学习了循环，BoBo老师给他出了一系列打印图案的练习，该任务是打印用“*”组成的“空心”三角形图案。
输入描述：
多组输入，一个整数（3~20），表示输出的行数，也表示组成三角形边的“*”的数量。
输出描述：
针对每行输入，输出用“*”组成的“空心”三角形，每个“*”后面有一个空格。

示例1
输入：
4
输出：
*
* *
*   *
* * * *

示例2
输入：
5
输出：
*
* *
*   *
*     *
* * * * *
*/
//int main()
//{
//	int i = 0;
//	int n = 0;
//	int j = 1;
//	while (~scanf(" %d", &n))
//	{
//
//		for (i = 1; i <= n; i++)
//		{
//			//第 i 行
//
//			if (i <= 2)
//			{
//				for (j = 1; j <= i; j++)
//				{
//					printf("* ");
//				}
//				printf("\n");
//			}
//			else if (i <= n - 1)
//			{
//				j = 1;
//				while (j <= i)
//				{
//
//					if (j == 1 || j == i)
//					{
//						printf("* ");
//					}
//					if (2 <= j && j <= i - 1)
//					{
//						printf("  ");
//					}
//					j++;
//				}
//				printf("\n");
//			}
//			else
//			{
//				j = 1;
//				for (j = 1; j <= i; j++)
//				{
//					printf("* ");
//				}
//			}
//		}
//		printf("\n");
//	}
//	return 0;
//}
////
//int main()
//{
//	int n = 0;
//	//多组输入
//	while (~scanf(" %d", &n))
//	{
//		//控制行
//		for (int i = 0; i < n; i++)
//		{
//			//控制列
//			for (int j = 0; j < n; j++)
//			{
//				//控制打印
//				if (i == n - 1 || j == 0 || i == j)
//				{
//					printf("* ");
//				}
//				else
//				{
//					printf("  ");
//				}
//			}
//			printf("\n");
//		}
//	}
//
//	return 0;
//}