#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

//*******指针********
// 指针就是地址，口语中说的指针指的是指针变量

// 内存被划分为一个个小的内存单元 
// 一个基本的内存单元的大小是一个字节
// 内存单元的编号 --- 地址

// 32位 - 32根地址线--物理的电线--通电--1/0     --> 4GB
// 64位                                    

// 内存单元 - 编号 -地址 --指针

//int main()
//{
//	int a = 10;//在内存中开辟了一块空间
//	int* pa = &a; // pa是指针变量(是用来存放地址的)
//	printf("%d", sizeof(pa));
//	// 4 字节
//	// 拿到的地址是它这个变量所占空间第一个指针  ---连续存放
//	char  c = 'w';
//	//char* pa = &c;
//	 
//	//printf("%p", &a);
//	//00000000 00000000 00000000 00001010
//	//00 00 00 0a //倒着放进去
//	return 0;
//}


// 指针和指针类型
//int main()
//{
//	//char* pc;
//	//int* pa;
//	//double* pd;
//	//printf("%d\n", sizeof(pc));
//	//printf("%d\n", sizeof(pa));
//	//printf("%d\n", sizeof(pd));
//
//	int a = 0x11223344;
//	//int* pa = &a;
//	//*pa = 0;
//	char* pc = &a;
//	*pc = 0;
//	// 指针类型决定了在解引用时 一次能访问几个字节(指针的权限)
//	//int * -->4
//	//char* -->1
//	//double*-->8
//	return 0;
//}

// 指针类型决定了指针向前或者向后走一步，走的距离(单位是字节)
//int main()
//{
//	int a = 10;
//	int* pa = &a;
//	char* pc = &a;
//	printf("%p\n", pa);
//	printf("%p\n", pa+1);//加4个字节
//
//	printf("%p\n", pc);
//	printf("%p\n", pc+1);//加1个字节
//	return 0;
//}

//创建一个整形数组,10个元素
//1.初始化数组的内容是1-10
//2.打印数组

//int main()
//{
//	int arr[10] = { 0 };
//	int* p = arr;
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		*(p + i) = i+1;
//	}
//	//倒着打印
//	int* q = &arr[9];
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", *q);
//		q--;
//	}
//	return 0;
//}
//int main()
//{
//	int arr[10] = { 0 };
//	char* pa = (char*)arr;
//
//	return 0;
//}

// 野指针
//int main()
//{
//	//int a;
//	//printf("%d\n", a);
//	//int* p;
//	//*p = 20;
//
//
//	int arr[10] = { 0 };
//	int* p = arr;
//	int i = 0;
//	for (i = 0; i <= 10; i++)
//	{
//		*p = i;
//		p++;
//	}
//	return 0;
//}

//int* test()
//{
//	int a = 100;
//	return &a;
//}
//int main()
//{
//	int* p = test();
//	printf("%d", *p);
//	return 0;
//}

// 避免野指针
//1. 指针初始化
//int main()
//{
//	int a = 10;
//	int* pa = &a;
//
//	int* p = NULL; //不知道只想谁 给空指针
//    if(p!= NULL)
//    {
//        *p = 20;
//    }
//    if (pa != NULL)
//    {
//        *pa = 20;
//    }
//  	return 0;
//}

// 2.小心数组越界

//int main()
//{
//	int* p = NULL;
//	if (p != NULL)
//	{
//		*p = 100;
//	}
//	return 0;
//}


// ...........


// **************指针运算***********

// 指针加减整数

//int main()
//{
//	int arr[10] = { 0 };
//	int* p = arr;
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (i = 0; i < sz; i++)
//	{
//		*(p + i) = i;
//	}
//	// 顺序打印
//	//for (i = 0; i < sz; i++)
//	//{
//	//	printf("%d ", *(p + i));
//	//}
//	// 逆序打印
//	int* q = arr + sz - 1;
//	for (i = 0; i < sz; i++)
//	{
//		/*printf("%d ", *(q - i));*/
//
//
//		//或者
//		printf("%d ", *q);
//		q--;
//	}
//	return 0;
//}

// 指针 - 指针
//指针减去指针的前提是两个指针 只想同一块空间
//int main()
//{
//	//int a[10] = { 0 };
//	//printf("%d\n", &a[9] - &a[0]);
//	//printf("%d\n", &a[0] - &a[9]);
//
//	//int a = 10;
//	//char c = 'w';
//	//&a - &c;
//	return 0;
//}

// 求字符串长度的函数
//int my_strlen(char* s)
//{
//	int count = 0;
//    while(*s != '\0')
//	{
//		count++;
//		s++;
//	}
//	return count;
//}

//int my_strlen(char* s)
//{
//	char* star = s;
//	while (*s != '\0')
//	{
//
//	}
//}
//
//int main()
//{
//	char arr[] = "abc";
//	int len = my_strlen(arr);
//	printf("%d\n", len);
//	return 0;
//}

// 指针的关系运算


// 指针和数组


//指针 --地址
// 指针变量

//数组是一组相同类型的数据
//


// 数组和指针本身是两种食物 
// 可以通过指针 访问数组



//二级指针
//int main() 
//{
//	int a = 10;
//	int* pa = &a;
//	int** ppa = &pa;//ppa就是一个二级指针
//
//	int*** pppa = &pa;// pppa就是一个三级指针
//	return 0;
//}

// 指针数组
//int main()
//{
//	//int arr[10];//整形数组--存放整形的数组
//	//char ch[5];//字符数组 - 存放字符的数组
//	// 指针数组 --
//	int a = 10;
//	int b = 20;
//	int c = 30;
//	int* arr2[5] = { &a,&b,&c };
//	int i = 0;
//	for (i = 0; i < 3; i++)
//	{
//		printf("%d ", *(arr2[i]));
//	}
//
//	return 0;
//}


#include<stdio.h> 
int main()
{
	int n, i, m, j, k;
	scanf("%d", &n);
	m = (n + 1) / 2;
	for (i = 1; i <= n; i++)  //一行一行的循环打印
	{
		if (i <= m)           //分两种情况，上半部分和下半部分
		{
			for (j = m - i; j > 0; j--)
				printf(" ");
			for (k = i; k > 0; k--)
				printf("* ");
		}
		else
		{
			for (j = i - m; j > 0; j--)
				printf(" ");
			for (k = 2 * m - i; k > 0; k--)
				printf("* ");
		}
		printf("\n");        //注意换行
	}
}