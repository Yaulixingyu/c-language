#include <stdio.h>


int main()
{
	int i = 10;
	int a[10] = { 0 };
	//sizeof return values is Unsigned number.
	for (i = 10; i - sizeof(char) >= 0; i--)
	{
		a[i] = i;
	}
	return 0;
}