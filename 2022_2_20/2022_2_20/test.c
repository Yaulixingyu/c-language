#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
//int main()
//{
//	char str[] = "Geneius";
//	print(str);
//	return 0;
//}
//print(char* s)
//{
//	if (*s)
//	{
//		print(++s);
//		printf("%c", *s);
//	}
//}

#include <stdio.h>
#include <string.h>
//int main()
//{
//    char str[1000] = { 0 };
//    //多组输入
//    while ((scanf("%s", str)) != EOF)
//    {
//        int n = strlen(str);
//        int i = 0;
//        int j = 0;
//        //单组排序
//        for (i = 0; i <n; i++)
//        {
//            for (j = 0; j < i; j++)
//            {
//                if (str[j] > str[j + 1])
//                {
//                    //单次交换
//                    char tmp = 0;
//                    tmp = str[j];
//                    str[j] = str[j + 1];
//                    str[j + 1] = tmp;
//                }
//            }
//        }
//        printf("%s\n", str);
//    }
//    return 0;
//}

//#include <stdlib.h>
//int comp(const void* a, const void* b)
//{
//    return *(char*)a - *(char*)b;
//}
//int main()
//{
//    char str[1001];
//    while (scanf("%s", str) != -1) 
//    {
//        int n = strlen(str);
//        qsort(str, n, sizeof(char), comp);
//        printf("%s\n", str);
//    }
//    return 0;
//}

//题目：https://leetcode-cn.com/problems/find-pivot-index/
//int pivotIndex(int* nums, int numsSize)
//{
//    //思路：
//    //1:先求数组的和
//    //2:再求前i位的和*2加上num[i] 和sum比较 相等返回i
//    //3.如果2不满足  返回-1 说明没有这个下标
//    int sum = 0;
//    for (int i = 0; i < numsSize; i++)
//    {
//        sum += nums[i];  // 数组中所有数之和
//    }
//    int sumOfPart = 0;   // 遍历到第i个元素时，左侧元素之和为sumOfPart
//    for (int i = 0; i < numsSize; i++)
//    {
//        if (2 * sumOfPart + nums[i] == sum)
//        {
//            return i;
//        }
//        sumOfPart += nums[i];
//    }
//    return -1;
//}

//int main()
//{
//	char c[5] = { 'a','b','\0','c','\0' };
//	printf("%s\n", c);
//	return 0;
//}

//int main()
//{
//	char a[] = "x+y = 15.";
//	char a[10] = '5';
//	char a[20] = "abcdefg";
//	return 0;
//}

#include <stdio.h>
#include <string.h>
//int main()
//{
//    char n[501] = { '\0'};
//    scanf("%s", n);
//    int len = strlen(n);//字符n的长度
//    int k[128] = {0}; //计数器
//    int i = 0;
//    int num = 0;
//    for ( i = 0; i < len; i++)
//    {
//        k[n[i]]++;
//    }
//    for (i = 0; i < 128; i++)
//    {
//        if (k[i] !=0)
//        {
//            num++;
//        }
//    }
//    printf("%d", num);
//    return 0;
//}

//#include<stdio.h>
//int main()
//{
//    char arr[501] = { '\0' };//存放字符串
//    int count[128] = { 0 };//把字符串对应的ascii码作为下标
//    int sum = 0;//计算不同字符个数
//    scanf("%s", arr);
//    for (int i = 0; i < strlen(arr); ++i)
//    {
//        count[arr[i]]++;
//    }
//    //只要不为0 sum就加
//    for (int i = 0; i < 128; ++i)
//    {
//        if (count[i])
//            sum++;
//    }
//    printf("%d", sum);
//}
//
//int majorityElement(int* nums, int numsSize)
//{
//    int i = 0, j = 0;
//    for (i = 0; i < numsSize; i++)
//    {
//        int k = 0;//一个数字的出现次数
//        for (j = 0; j < numsSize; j++)
//        {
//            if (*(nums + i) == *(nums + j))
//            {
//                k++;
//            }
//        }
//     return *(nums + i);
//
//    }
//}
//  2 3 3 
//int majorityElement(int* nums, int numsSize) {
//    int r, c = 0;
//    for (int i = 0; i < numsSize; i++) {
//        if (c == 0) {
//            r = nums[i];  // r = 2 
//            c = 1;        // c = 1  
//        }
//        else if (nums[i] == r) {
//            c++;          // c = 2
//        }
//        else {
//            c--;           // c  = 1
//        }
//    }
//    return r;
//}

//int main()
//{
//	int i = 0;
//	if (i == 0)
//	{
//		i++;
//	}
//	else if (i > 0)
//	{
//		i++;
//	}
//	else
//	{
//		i--;
//	}
//	printf("%d", i);
//	return 0;
//}

//int Len(int n)
//{
//    int count = 0;
//    while (n)
//    {
//        n /= 10;
//        count++;
//    }
//    return count;
//}
//
//int* selfDividingNumbers(int left, int right, int* returnSize)
//{
//
//    //判断一个数是否是自除数
//    for (left; left <= right; left++)
//    {
//        int a = left;
//        int len = Len(left);
//        int k = 0;
//        while (left % 10)
//        {
//            if (left % (left % 10) == 0)
//                k++;
//            left = left / 10;
//        }
//        if (k == len)
//        {
//            for (int i = 0; i < (right - left); i++)
//                *(returnSize + i) = a;
//        }
//        return *returnSize;
//    }
//}

int func(int x)
{
	int count = 0;
	while (x)
	{
		count++;
		x = x & (x - 1);
	}
	return count;
}

int main()
{
	int x = func(-1);
	printf("%d", x);
	return 0; 
}