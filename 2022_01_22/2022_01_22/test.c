#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>


//内置类型：char short int long longlong 
// float double 
// 自定义类型：
// 结构体 枚举 联合 (数组)
//

// 结构体的声明
//描述复杂对象(类型) -----> 结构体类型

//结构体类型
//struct Stu//(结构体标签)
//{
//	//成员变量
//	char name[20];
//	char sex[5];
//	unsigned int age;//年龄
//	int height;//身高
//}s2,s3,s4;//变量列表(全局变量)
//
//struct Stu s5;//全局变量
//
//int main()
//{
//	struct Stu s1;//结构体变量（）
//	return 0;
//}

//struct
//{
//	char c;
//	int a;
//	double d;
//}sa;
//
//struct
//{
//	char c;
//	int a;
//	double d;
//}* ps;
//
//int main()
//{
//	ps = &sa;//类型不一样
//	//编译器认为等号两边是不同的结构体
//
//
//	return 0;
//}

//结构体的自引用

//error
//struct Node
//{
//	int data;
//	struct Node next;
//
//};

//正确
//struct Node
//{
//	int data;
//	struct Node* next;
//};


//error Node不存在
//typedef struct Node
//{
//	int data;
//	Node* next;
//}Node;
//
//int main()
//{
//	Node n = { 0 };
//	return 0;
//}

