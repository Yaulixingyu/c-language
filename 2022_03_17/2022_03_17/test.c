#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

#define Pi 3.1415926536
int main()
{
	double r = 0;
	scanf("%lf", &r);
	double area = Pi * r * r;//圆面积
	double surface_area = 4 * Pi * r * r;//球体表面积
	double volume = 4 * Pi * r * r * r / 3.0;//球体体积
	printf("%-20.10lf %-20.10lf %-20.10lf", area, surface_area, volume);
	return 0;
}

//int main()
//{
//	double principal = 0;//本金
//	double interest_rate = 0;//利率
//	scanf("%lf %lf", &principal, &interest_rate);
//	//sum = 本金+利息-利息所得税
//	double sum = principal + principal * interest_rate * 0.01 *(1-0.05);
//	printf("%.2lf", sum);
//	return 0;
//}