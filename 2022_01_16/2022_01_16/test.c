#include "test.h"


//声明并没有开辟空间 赋值或者初始化没有意义

static int g_val = 100;
//结论1：static修饰全局变量，该变量只在本文件内被访问,不能在其他文件被直接访问
// 


//static void show()
//{
//	printf("Hello Show()!\n");
//}
//
//void show_helper()
//{
//	show();
//}


//结论2：static修饰函数，该函数只能在本文件内被访问，不能被其他文件访问
//但是可以通过嵌套的方式调用，变相的封装的表现


//static项目维护，提供安全保护
//被static修饰，主要限制的是作用域

//void fun()
//{
//	int i = 0;
//	i++;
//	printf("i = %d\n", i);
//}
//
//int main()
//{
//	for (int i = 0; i < 10; i++)
//	{
//		fun();
//	}
//}

//i 是局部变量，具有临时性
//函数调用开辟空间并初始化
//函数结束释放空间


void fun()
{
	static int i = 0;
	i++;
	printf("i = %d\n", i);
}

int main()
{
	for (int i = 0; i < 10; i++)
	{
		fun();
	}
}
//结论3
// static修饰局部变量，更改该局部变量的生命周期
// 生命周期：将临时变量的生命周期 -- 全局变量的生命周期
// 作用域不变：作用域仍然是在本代码块内



