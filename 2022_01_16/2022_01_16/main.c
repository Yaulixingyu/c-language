#define _CRT_SECURE_NO_WARNINGS
#include "test.h"
//int i;
//void prt()
//{
//	for (i = 5; i < 8; i++)
//		printf("%c", '*');
//	printf("\t");
//} 
//int main()
//{
//	for (i = 5; i <= 8; i++)
//		prt();
//	return 0;
//}

//int main()
//{
//	int a = 3;
//	printf("%d\n", (a += a -= a * a));
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//    int n = 0;
//    while (~scanf("%d", &n))
//    {
//        int arr[2000] = { 0 };
//        for (int i = 0; i < n; i++)
//        {
//            scanf("%d", &arr[i]);
//        }
//        //筛选负数
//        int i = 0, count = 0, cou = 0, sum = 0;
//        float ave = 0.0f;
//        for (i = 0; i < n; i++)
//        {
//            if (arr[i] < 0)
//            {
//                count++;
//            }
//            if (arr[i] > 0)
//            {
//                sum += arr[i];
//                cou++;
//            }
//        }
//        ave = sum / (float)cou;
//        printf("%d %.1f\n", count, ave);
//    }
//    return 0;
//}

//二分法
//int left = 0;
//int right = arrSize - 1;
//while (left < right)
//{
//    int mid = (left + right) / 2;
//    if (arr[mid] < arr[right])
//        right = mid;
//    else if (arr[mid] > arr[right])
//        left = mid + 1;
//    else
//        right -= 1;
//}
//return arr[left];


//C语言深度解剖 --- 2
//static   sizeof
// static --最名不副实的关键字



// error: extern int g_val = 100;
//声明并没有开辟空间 赋值或者初始化没有意义

//结论：所有的变量声明的时候，不能设置初始值!

//为什么要有头文件？

//单纯的使用源文件，组织项目结构的时候，项目越大越复杂
//维护成本会变得越来越高
//.h：头文件，组织项目结构的时候，减少大型项目的维护成本问题
//头文件基本都是要被多个源文件包含，可能有一个问题，头文件被重复包含的问题？



// 为什么一定规模的项目，一定是多文件的？
// 多个文件之间，后续一定进行数据"交互"，如果不能跨文件，在"交互"成本比较高


//多文件：
//  全局变量可以快文件访问？     可以
//  函数可以跨文件访问吗？       可以

//在具体的单文件下只想在当前文件下，这就是static的作用

//int main()
//{
//	//printf("%d\n", g_val);
//	show_helper();
//	return 0;
//}

//sizeof

//关键字（操作符）求特定一个类型开辟空间数的大小，单位是字节

//int main()
//{
//
//	printf("%d\n", sizeof(char));
//	printf("%d\n", sizeof(short));
//	printf("%d\n", sizeof(int));
//	printf("%d\n", sizeof(float));
//	printf("%d\n", sizeof(double));
//	printf("%d\n", sizeof(long));
//	printf("%d\n", sizeof(long long));
//
//
//	return 0;
//}