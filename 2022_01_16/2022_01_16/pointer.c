
#include <stdio.h>

//字符指针
//指针数组：存放指针的数组就是指针数组
//int main()
//{
//	int* arr[10];//指针数组--存放的是int*
//	char* ch[5];//指针数组 -- 存放的是char*
//	return 0;
//}


//数组指针
//是一组指针。
//字符指针---指向字符的指针
//char ch = 'w';
//char* p = &ch;
//整形指针--指向整型的指针
//int a = 10;
//int* p = &a;
//数组指针--指向数组的指针
//int* P1[10];   
//int (*p2)[10]; p2就是一种数组指针

// arr和&arr的区别

//arr是数组首元素的地址
//即：arr=&arr[0];
//&arr是整个数组的地址

//int main()
//{
//	int arr[10] = { 0 };
//	printf("%p\n", arr);
//	printf("%p\n", arr+1);
//
//	printf("%p\n", &(arr[0]));
//	printf("%p\n", &(arr[0])+1);
//
//	printf("%p\n", &arr);
//	printf("%p\n", &arr+1);
//
//
//	return 0;
//}

//int main()
//{
//	char arr[5];
//	char(*pa)[5] = &arr;//pa是一个指向字符数组的指针
//
//	int* parr[6];
//	int* (*pp)[6] = &parr;
//	return 0;
//}

//数组指针有什么用

//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* p = arr;
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int(*p)[10] = &arr;
//	int i = 0;
//	for (i = 0; i < 10; i++)
//	{
//		printf("%d ", *((*p) + i));
//
//	}
//
//	return 0;
//}

void print(int a[3][5], int r, int c)
{
	int i = 0;
	int j = 0;
	for (i = 0; i < r; i++)
	{
		for (j = 0; j < c; j++)
		{
			printf("%d ", a[i][j]);
		}
		printf("\n");
	}
}

//int (*p)[5] 是一个数组指针
void print1(int(*p)[5], int r, int c)
{
	int i = 0;
	for (i = 0; i < r; i++)
	{
		int j = 0;
		for (j = 0; j < c; j++)
		{
			//*(p + i)相当于拿到了二维数组第i行，也相当于第i行的数组名，表示首元素的地址
			//其实也是第i行第一个元素的地址
			printf("%d ", *(*(p + i) + j));
			              //   p[i][j]
						  
			//p是第一行的地址
			//p+i是第i行的地址
			//*(p+i)是第i行首元素的地址
			//*(p+i)+j 是第i行第j列元素的地址
			//*(*(p+i)+j）是第i行第j列元素


		}
		printf("\n");
	}
}


//int main()
//{
//	int arr[3][5] = { {1,2,3,4,5},{2,3,4,5,6},{3,4,5,6,7} };
//	//print(arr, 3, 5);
//	//二位数组传参的时候，数组名表示首元素地址，表示第1行的地址
//	print1(arr,3,5);
//
//	return 0;
//}

//int main()
//{
//	int arr[10];
//	int i = 0;
//	arr[i];
//	int* p = arr;
//	*(p + i);
//	return 0;
//}

//int(*parr3[10])[5];//parr3和[] 结合，说明parr3是一个数组，数组是10个元素。
                     //数组的每个元素是什么类型？是一种数组指针，类型是int(*)[5]
                     //该类型的指针指向的数组有5个int类型的元素


//数组参数 指针参数

//二位数组传参

//void test(int* p)
//{
//
//
//}
//
//int main()
//{
//	int a = 10;
//	int* ptr = &a;
//	int arr[10] = { 0 };
//
//	test(&a);
//	test(ptr);
//	test(arr);
//	return 0;
//}


//void test(char** p)
//{
//
//}
//
//int main()
//{
//	char ch = 'w';
//	char* p = &ch;
//	char** pp = &p;
//	char* arr[5];//指针数组
//
//
//	test(&p);
//	test(pp);
//	test(arr);
//
//
//	return 0;
//}
// 

//int main()
//{
//	int arr[3][5];
//	int* p = arr;
//	return 0;
//}


//函数指针

// 指向函数的指针

//int Add(int x, int y)
//{
//	return x + y;
//}
//
//void test(char* str)
//{
//
//}
//
//int main()
//{
//
//    //void (*pt)(char*) = &test;
//	int(*pf)(int, int) = &Add;
//	int sum = (*pf)(2,3);
//
//	//int sum = Add(2, 3);
//
//	printf("%d\n", sum);
//	return 0;
//}

//int main()
//{
//	//1.把0强制类型转换成void(*)()类型的函数指针
//	//2.再去调用0地址处这个参数为无参，返回类型是void的函数
//	(*(void(*)())0)();//这是依次函数调用，调用0地址处的函数
//	
//	
//
//	(void (*signal(int, void(*)(int)))(int);
//	//signal是一个函数声明
//	//这个函数的参数有2个，第一个是int类型，第二个是函数指针，该指针指向的函数参数int,返回类型是void
//	//signal函数返回类型也是函数指针。该指针指向的函数int,返回类型是void
//	//简化
//	typedef void(*pfun_t)(int);
//	pfun_t signal(int, pfun_t);
//
//	void (*p)(int);             //p是函数指针变量名字
//	typedef void (*pf_t)(int);  //pt_f是类型名
//
//	return 0;
//}

//函数指针数组 -- 存放函数指针的数组，每个元素都是函数指针类型

//指针数组

int Add(int x, int y)
{
	return x+ y;
}

int Sub(int x, int y)
{
	return x - y;
}

int Mul(int x, int y)
{
	return x * y;
}

int Div(int x, int y)
{
	return x / y;
}
//int main()
//{
//	//int(*pf1)(int, int) = Add;
//	//int(*pf2)(int, int) = Sub;
//	//int(*pf3)(int, int) = Mul;
//	//int(*pf4)(int, int)= Div;
//	
//	int(*pfArr[4])(int, int) = {Add,Sub,Mul,Div}; //函数指针数组	也叫转移表
//	// 8 4 
//	//12
//	//4
//	//32
//	//2
//	int i = 0;
//	for (i = 0; i < 4; i++)
//	{
//		int ret =(*pfArr[i])(8, 4);
//		printf("%d\n", ret);
//	}
//
//
//
//	return 0;
//}

