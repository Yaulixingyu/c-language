#pragma once  //方法一
//解决：头文件被重复包含

// 1. 系统头文件
// 2. 所有的变量的声明
// 3. 所有的函数的声明
// 4. #define，类型typedef，struct
//

#include <stdio.h>

//变量的声明
extern int g_val;//变量必须带上extern

//函数的声明
extern void show();//函数声明建议带上extern!
extern void show_helper();
void fun();