#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
//int main()
//{
//	char s[] = "\\123456\123456\t";
//	printf("%d\n", strlen(s));
//	return 0;
//}
 
 
//int main()
//{
//	int n = 0;
//	while (~(scanf("%d", &n)) != 0)//多组输入一个整数n
//	{
//		int i = 0, k = 0;
//		//判断一个数
//		i = (int)pow(n, 3);//总和
//		k = i / n;   // 奇数的平均值
//		//输出
//		while (n) 
//		{
//			int m = k - (n / 2) - 1;
//			printf("%d",)
//		}
//	}
//	return 0; 
//}

#include <stdio.h>
//int main()
//{
//    int num;
//    while (scanf("%d", &num) != EOF)
//    {
//        int tri = num * num * num;
//        for (int i = 1; i < 65000; i += 2)
//        {
//            int sum = (i + (i + (num - 1) * 2)) * num / 2;
//            if (sum == tri)
//            {
//                for (int j = 0; j < num - 1; j++)
//                {
//                    printf("%d+", i + 2 * j);
//                }
//                printf("%d\n", i + 2 * (num - 1));
//            }
//        }
//    }
//    return 0;
//}
#include <stdio.h>
//int main()
//{
//	int n = 0;
//	while (~(scanf("%d", &n)))
//	{
//		int m = 2, sum = 2;
//		while (n - 1)
//		{
//			m += 3;//下一位
//			sum += m;
//			n--;
//		}
//		printf("%d\n", sum);
//	}
//	return 0;
//}

//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int arr[7] = { 0 };
//	int i = 0;
//	int j = 0;
//	for (i = 0; i < n; i++)
//	{
//		scanf("%d", &arr[i]);
//	}
//	int m = 0;
//	scanf("%d", &m);
//	for (i = 0; i < n; i++)
//	{
//		if (arr[i] != m)
//		{
//			arr[j++] = arr[i];
//		}
//	}
//		for (i = 0; i < j; i++)
//		{
//			printf("%d ", arr[i]);
//		}
//	return 0;
//}

//C语言深度解剖
//C语言关键字1
#include <stdio.h>

//文本代码 -->可执行程序(二进制文件)-->双击启动该程序
//任何程序在被运行之前都必须被加载到内存当中
//生成可执行程序并运行


//int main()
//{
//	printf("hello world!\n");
//	return 0;
//}


//定义：开辟空间  只有一次
//声明：告知     声明多次


//局部变量：包含在代码块中的变量。
//全局变量：在所有函数外定义的变量。

int a = 20;
int main()
{
	a = 10;
	printf("%d", a);
	return 0;
}


//最宽宏大量的关键字 -- auto 
//一般在代码块中定义的变量，局部变量，默认都是auto修饰的
//只在本代码块中使用 auto可省略

//auto int g_val = 100;
//int main()
//{
//	for (auto int i = 0; i < 10; i++)
//	{
//		printf("i = %d\n", i);
//		if (1)
//		{
//			auto int j = 1;
//			printf("before: %d\n", j);
//			j++;
//			printf("after: %d\n", j);
//		}
//	}
//	return 0;
//}

// register  -- 最快的关键字

//存储分级

////寄存器是下游存储设备的缓存

//本质是在硬件层面上，提高计算机的运算效率

//什么变量可以用register
//1.局部变量
//2.不会被写入
//3.经常被使用
//
//int main()
//{
//	register int val = 100;
//	//寄存器变量没有地址，不能取地址
//	val = 200; //可以被写入
//	printf("%d\n", val);
//	// 在gcc编译器下 也不允许取地址
//
//	return 0;
//}