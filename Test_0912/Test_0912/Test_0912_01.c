#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
//1.选择语句
//int main()
//{
//	int input = 0;
//	scanf("%d", &input);
//	if (input == 1)
//		printf("%d\n", input);
//	else
//		printf("error!\n");
//	
//	return 0;
//}
// ********************************************************
// 

//2.循环语句
//int main()
//{
//	int line = 0;
//	while (line < 30000)
//	{
//		printf("写代码：%d\n", line);
//		line++;
//	}
//	if (line == 30000)
//	{
//		printf("好offer\n");
//	}
//	return 0;
//}
 /**********************************************************/

//3.函数    //C语言中有两种类型函数：标准库函数  自定义函数
//int Add(int x, int y) {
//	int z = 0;
//	z = x + y;
//	return z;
//}
//int main(){
//	int num1 = 0;
//	int num2 = 0;
//	scanf("%d %d", &num1, &num2);
//	//int sum = num1 + num2;
//	//用函数解决
//	int sum = Add(num1, num2);
//	printf("%d\n", sum);
//	return 0;
//}
//*************************************************************

//4.数组的存储（一组相同类型的集合）
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	char ch[5] = { 'a','b','c' };//不完全初始化，剩余的默认为0；
//	int i = 0; 
//	int j = 0;
//	while (i<10)
//	{
//		printf("%d\n", arr[i]);
//		i++;
//	}
//	while (j < 5)
//	{
//		printf("%d\n", ch[j]);
//		j++;
//	}
//	return 0;
//
//}
