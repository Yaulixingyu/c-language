#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

//void add(int *p) {
//	*p = *p + 1;
//}

//int main() {
//	int num = 0;
//	add(&num);
//	printf("%d\n", num);
//	return 0;
//}

//int add(int n) {
//	n++;
//	return 0;
//}

//int add(int n) {
//	return ++n;
//}

//int add(int n) {
//
//    return n+1;
//}
//
//int main() {
//	int num = 0;
//	num = add(num);
//	printf("%d\n", num);
//	return 0;
//}


// 函数的返回类型
//  void --无返回
//  char  int float
//void test1()
//{
//	int n = 5;
//	printf("hehe\n");
//	if (n == 5)
//		return;// void 不能带具体值
//
//	printf("haha\n");
//}
//
//int test2()
//{
//	return 1;
//}
//
//int main() {
//	test1();
//
//	return 0;
//}

// 函数的嵌套调用和链式组合


// 函数可以嵌套调用 但是不能 嵌套定义 ：就是不能在一个函数里面再写一个函数
//void new_line() {
//
//	printf("hehe\n");
//}
//void three_line() {
//
//	int i = 0;
//	for (i = 1; i <= 3; i++) {
//		new_line();
//	}
//}
//int main() {
//
//	three_line();
//
//	return 0;
//}

//链式访问： 把一个函数的返回值作为另外一个函数的参数

//int main() {
//
//	//int len = strlen("abc");
//	//printf("%d\n", len);
//	//                 函数
//	printf("%d\n", strlen("abc"));
//	//      参数1       参数2
//
//	//链式访问： 把一个函数的返回值作为另外一个函数的参数
//
//	return 0;
//}


//int main() {
//
//	printf("%d", printf("%d", printf("%d", 43)));// 打印： 4321
// //          1             2          43 
//
//	return 0;
//}


// ****函数的声明和定义****
//int Add(int x, int y);
//
//int main() {
//	int a = 10;
//	int b = 20;
//	int ret = Add(a, b);
//	printf("%d\n", ret);
//
//	return 0;
//}
//
//int Add(int x, int y) {
//
//	int z = 0;
//	z = x + y;
//	return z;
//
//}

//// 函数声明
//int Add(int x, int y);

#include "add.h"

//int main() {
//	int a = 10;
//	int b = 20;
//	int ret = Add(a, b);//先声明后使用
//	printf("%d\n", ret);
//
//	return 0;
//}

// 函数定义
//int Add(int x, int y) {
//
//	int z = 0;
//	z = x + y;
//	return z;
//
//}

// ****函数递归****
//两个必要条件：1. 存在限制条件   2.每次递归调用之后越来越接近这个限制条件

// 递归：程序调用自身的编程技巧成为递归

//把大事化小
// 
// 史上最简单的递归

/*是递归，但是会死递归
int main() {

	printf("hehe\n");
	main();
	return 0;
}*/

// 接受一个整数，按照顺序打印每一位


// 每一次函数调用都会在内存的栈区申请一块空间
//void Print(unsigned int n) {
//
//	if (n > 9) { 
//
//		Print(n / 10);
//	}
//	printf("%d ", n % 10);
//}
//
//int main() {
//
//	unsigned int num = 0;
//	scanf("%u",&num);
//	Print(num);
//
//	return 0;
//}

//void test() {
//
//	printf("hehe\n");
//
//}
//
//int main() {
//	printf("heihei\n");
//	test();
//	printf("haha\n");
//
//	return 0;
//}


//  编写函数不允许创建临时变量，求字符串的长度

//int main() {
//
//	//求字符串的长度
//	char arr[] = "abc";
//	int len = strlen(arr);
//	printf("%d\n", len);
//
//	return 0;
//}
//
//int my_strlen(char* s) {
//
//	int count = 0; // 临时变量
//	//printf("%c\n", *s);
//	while(*s != '\0') {
//		
//		count++;
//		s++;
//	}
//	return count;
//}


//
//int my_strlen(char* s) {
//	if (*s == '\0') {
//		return 0;
//	}
//	else {
//		return 1 + my_strlen(s + 1);
//	}
//}
//int main() {
//
//    //求字符串的长度
//	char arr[] = "abc";
//	//arr是数组名，数组名是数组首元素的地址
//	int len = my_strlen(arr);//char*
//	printf("%d\n", len);
//	return 0;
//}

// 递归与迭代

// 求n的阶乘

//非递归算法
//
//int main() {
//	int m = 0;
//	int n = 0;
//	scanf("%d",&m);
//	int ret = 1;
//	for (n = 1; n <= m; n++) {
//
//		ret *= n;
//	}
//	printf("%d\n", ret);
//
//	return 0;
//}
 
 
// 递归算法
//int fac(n) {
//	
//	if (n <= 1) {
//		return 1;
//	}
//	else {
//		return n * fac(n - 1);
//	}
//}
//
//int main() {
//	int n = 0;
//	scanf("%d", &n);
//	int ret = fac(n);
//	printf("%d\n",ret);
//	return 0;
//}

//int DigitSum(int n) {
//	if (n != 0)
//		n = n % 10;
//	return n + DigitSum(n / 10);
//}
//
//int main() {
//	int num = 0;
//	scanf("%d", &num);
//	DigitSum(num);
//	printf("%d", num);
//	return 0;
//}

// 求第n个斐波那契数
//int fib(n) {
//	if (n <= 2) {
//		return 1;
//	}
//	else {
//		return fib(n - 1) + fib(n - 2);
//	}
//}
//
//int main() {
//	int n = 0;
//	scanf("%d", &n);
//	n = fib(n - 1) + fib(n - 2);
//	printf("%d", n);
//	return 0;
//}

// 高效：迭代
//int fib(n) {
//	int a = 1;
//	int b = 1;
//	int c = 1;
//
//	while (n>2) {
//		c = a + b;
//		a = b;
//		b = c;
//		n--;
//	}
//	return c;
//}
//int main() {
//	int n = 0;
//	scanf("%d", &n);
//	n = fib(n);
//	printf("%d", n);
//	return 0;
//}
